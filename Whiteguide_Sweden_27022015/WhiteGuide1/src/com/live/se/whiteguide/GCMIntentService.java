package com.live.se.whiteguide;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;
import com.se.whiteguide.dataengine.DataEngine;
import com.se.whiteguide.dataengine.Restaurant;
import com.se.whiteguide.helpers.UserEmailFetcher;
import com.se.whiteguide.pushnotifications.CommonUtilities;
import com.se.whiteguide.screens.TempActivity;
import com.live.se.whiteguide.R;

public class GCMIntentService extends GCMBaseIntentService {
	SharedPreferences settings;

	public GCMIntentService() {
		super(CommonUtilities.SENDER_ID);
	}

	/**
	 * Method called on device registered
	 **/
	@Override
	protected void onRegistered(Context context, final String registrationId) {
		CommonUtilities.displayMessage(context,
				"Your device registred with GCM");
		settings = getSharedPreferences(DataEngine.SETTINGS_NAME, MODE_PRIVATE);
		if (null != registrationId && !"".equalsIgnoreCase(registrationId)) {
			new Thread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					String nytest_status = settings.getString(
							DataEngine.SETTINGS_NYTEST, "false");
					String nhyter_status = settings.getString(
							DataEngine.SETTINGS_NHYTER, "true");
					boolean isnytestatEnabled=false,isnyheterEnabled=true;
					if(nytest_status.equalsIgnoreCase("true")){
						isnytestatEnabled=true;
					}
					if(nhyter_status.equalsIgnoreCase("false")){
						isnyheterEnabled=false;
					}
					
					
					postData(registrationId, isnytestatEnabled, isnyheterEnabled);
					Editor editor = settings.edit();
					editor.putString(DataEngine.SETTINGS_GCM, registrationId);
					editor.commit();
					Log.e("Hello", "IIIIdd-----------" + registrationId);
				}
			}).start();
		}

	}

	long timestamp;

	public void postData(String regId, boolean nytest_sts, boolean nhyt_sts) {
		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(DataEngine.BASE_URL
				+ DataEngine.GCM_URL);

		try {
			// Add your data
			timestamp = System.currentTimeMillis();
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("content-type",
					"application/json"));
			nameValuePairs.add(new BasicNameValuePair("email", UserEmailFetcher
					.getEmail(getApplicationContext())));
			nameValuePairs.add(new BasicNameValuePair("deviceId",
					UserEmailFetcher.getDeviceId(getApplicationContext())));
			nameValuePairs.add(new BasicNameValuePair("timeStamp", String
					.valueOf(timestamp)));
			nameValuePairs.add(new BasicNameValuePair("gcmId", regId.trim()));
			nameValuePairs.add(new BasicNameValuePair("deviceType", "Android"));
			nameValuePairs.add(new BasicNameValuePair("nyheter", Boolean.toString(nhyt_sts)));
			nameValuePairs.add(new BasicNameValuePair("nytestat",Boolean.toString(nytest_sts)));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			httpclient.execute(httppost);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
		} catch (IOException e) {
			// TODO Auto-generated catch block
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * Method called on device un registred
	 * */
	@Override
	protected void onUnregistered(Context context, String registrationId) {
		CommonUtilities.displayMessage(context, "Registered");
	}

	/**
	 * Method called on Receiving a new message
	 * */
	@Override
	protected void onMessage(Context context, Intent intent) {
		String message = intent.getExtras().getString("message");
		String newsid = "";
		String push_type = null;
		Restaurant restaurant = null;
		Log.e(TAG,"--pushtype---------"+intent.getExtras().getString("pushType", "false"));
		if (intent.getExtras().getString("pushType", "false")
				.equals("nytestat")) {
			push_type = "nytestat";
			restaurant = new Restaurant();
			newsid = intent.getExtras().getString("restaurantid");
			restaurant.setId(intent.getExtras().getString("restaurantid"));
			restaurant.setChanged(intent.getExtras().getString("changed"));
			restaurant.setRestaurantClass(intent.getExtras().getString("class"));
			restaurant.setTitle(intent.getExtras().getString("title"));
			restaurant.setPosition(intent.getExtras().getString("coordinates"));
			restaurant.setScore(intent.getExtras().getString("total"));
			//Log.e(TAG,"--restaurant obj---------"+intent.getExtras().getString("restaurantid")+"---"+intent.getExtras().getString("class")+"----"+intent.getExtras().getString("coordinates"));

		} else if (intent.getExtras().getString("pushType", "false")
				.equals("nyheter")) {
			push_type = "nyheter";
			newsid = intent.getExtras().getString("newsid");
		}
		generateNotification(context, message, newsid, restaurant,push_type);

		// "isNyheter"=> "false","title" => "19",
		// "coordinates"=>"POINT(59.3246206 18.0686084)","class"=>"3. Mycket god klass","total"=>"73"),
	}

	/**
	 * Method called on receiving a deleted message
	 * */
	@Override
	protected void onDeletedMessages(Context context, int total) {
		// generateNotification(context, "deleted");
	}

	/**
	 * Method called on Error
	 * */
	@Override
	public void onError(Context context, String errorId) {
	}

	@Override
	protected boolean onRecoverableError(Context context, String errorId) {
		// log message
		return super.onRecoverableError(context, errorId);
	}

	/**
	 * Issues a notification to inform the user that server has sent a message.
	 */
	private static void generateNotification(Context context, String message,
			String newsid, Restaurant restaurant,String push_type) {

		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				context).setSmallIcon(R.drawable.ic_launcher)
				.setContentTitle(message).setContentText("Whiteguide Sweden");
		// Random random=new Random();

		// Creates an explicit intent for an Activity in your app
		Intent resultIntent = new Intent(context, TempActivity.class);
		resultIntent.putExtra("PUSH_NOTIFICATION", true);
		resultIntent.putExtra("NEWSID", newsid);
		resultIntent.putExtra("RES", restaurant);
		resultIntent.putExtra("PUSHTYPE",push_type);
	//	resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
	//			| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		// The stack builder object will contain an artificial back stack for
		// the
		// started Activity.
		// This ensures that navigating backward from the Activity leads out of
		// your application to the Home screen.
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
		// Adds the back stack for the Intent (but not the Intent itself)
		stackBuilder.addParentStack(TempActivity.class);
		// Adds the Intent that starts the Activity to the top of the stack
		stackBuilder.addNextIntent(resultIntent);
		int random = new Random().nextInt(10000);
		PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
				random, PendingIntent.FLAG_UPDATE_CURRENT);
		mBuilder.setContentIntent(resultPendingIntent);
		mBuilder.setAutoCancel(true);
		NotificationManager mNotificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		// mId allows you to update the notification later on.

		mNotificationManager.notify(random, mBuilder.build());

		/*
		 * NotificationManager notificationManager = (NotificationManager)
		 * context .getSystemService(Context.NOTIFICATION_SERVICE); Notification
		 * notification = new Notification(icon, message, when);
		 * 
		 * String title = context.getString(R.string.app_name);
		 * 
		 * Intent notificationIntent = new Intent(context, TempActivity.class);
		 * notificationIntent.putExtra("PUSH_NOTIFICATION", true);
		 * notificationIntent.putExtra("NEWSID", newsid);
		 * 
		 * 
		 * // set intent so it does not start a new activity
		 * //notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP // |
		 * Intent.FLAG_ACTIVITY_SINGLE_TOP);
		 * Log.e("GCMSSSSS","--------push bundle--------"
		 * +newsid+"-------"+message);
		 * 
		 * 
		 * PendingIntent intent = PendingIntent.getActivity(context, 0,
		 * notificationIntent, 0); notification.setLatestEventInfo(context,
		 * title, message, intent); notification.flags |=
		 * Notification.FLAG_AUTO_CANCEL;
		 * 
		 * // Play default notification sound notification.defaults |=
		 * Notification.DEFAULT_SOUND;
		 * 
		 * // notification.sound = Uri.parse("android.resource://" + //
		 * context.getPackageName() + "your_sound_file_name.mp3");
		 * 
		 * // Vibrate if vibrate is enabled notification.defaults |=
		 * Notification.DEFAULT_VIBRATE; notificationManager.notify(0,
		 * notification);
		 */
	}

}
