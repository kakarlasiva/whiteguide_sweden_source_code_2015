package com.se.whiteguide.screens;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.facebook.samples.facebook.AsyncFacebookRunner;
import com.facebook.samples.facebook.AsyncFacebookRunner.RequestListener;
import com.facebook.samples.facebook.DialogError;
import com.facebook.samples.facebook.Facebook;
import com.facebook.samples.facebook.Facebook.DialogListener;
import com.facebook.samples.facebook.FacebookError;
import com.facebook.samples.facebook.SessionEvents;
import com.facebook.samples.facebook.SessionEvents.AuthListener;
import com.facebook.samples.facebook.SessionEvents.LogoutListener;
import com.facebook.samples.facebook.SessionStore;
import com.facebook.samples.facebook.Util;
import com.facebook.samples.fbconnect.FBConnectQueryProcessor;
import com.live.se.whiteguide.R;
import com.se.whiteguide.AppController;
import com.se.whiteguide.dataengine.DataEngine;
import com.se.whiteguide.parsers.DetailedPullParser;
import com.se.whiteguide.tabgroup.Home;
import com.se.whiteguide.twitter.Twitt_Sharing;

/**
 * Activity represents the detailed view of the news item the user 
 * had been selected with the sharing options.
 */
public class DetailedNewspush extends AnimActivity implements OnClickListener {
	// NewsListParser parser_obj;
	public int position;
	public static String DATE_FORMAT="dd MMM yyyy";
	TextView header_txt, title_txt, publish_txt, short_desc;
	NetworkImageView image;
	ImageView share_icon;
	ImageLoader imageLoader;
	//Detailedpullhelper helper_obj;
	File casted_image;
	private String string_msg = null;
	Facebook mFacebook;
	AsyncFacebookRunner mAsyncFacebookRunner;
	private String fbId;
	ProgressDialog progress;
	String shareToFbTwitter;
	DetailedPullParser parser_obj;
	String news_id,news_title;
	int pos = 1;
	DataLoader loader_obj;
	public static String Email_str="White Guide App �r hela Sveriges restaurangguide, med den f�r du ocks� tillg�ng till det senaste som h�nt i krogsverige, l�ngre <br/>reportage och v�ra senast testade restauranger.<br />Ladda ned din White Guide App fr�n App Store <a href=\"https://itunes.apple.com/us/app/apple-store/id878872712?mt=8\">H�R</a> <br />Ladda ned din White Guide App fr�n Google Play <a href=\"https://play.google.com/store/apps/details?id=com.live.se.whiteguide\">H�R</a>";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.detailednews_lay);
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			news_id=extras.getString("newsid");
			news_title=extras.getString("newstitle");
		}
		init();
	
		setListeners();
		loader_obj=new DataLoader();
		loader_obj.execute();
	}

	/**
	 *  Initializing all the layout components with their id's.
	 */
	private void init() {
		progress = new ProgressDialog(this);
		progress.setCancelable(true);
		progress.setMessage(getString(R.string.loading));
		imageLoader = AppController.getInstance().getImageLoader();
		parser_obj=new DetailedPullParser(this);
		header_txt = (TextView) findViewById(R.id.header_titletext);
		title_txt = (TextView) findViewById(R.id.title_txt);
		//title_txt.setTypeface(DataEngine.gettypeface(DetailedNewsList.this));
		short_desc = (TextView) findViewById(R.id.short_txt);
		//short_desc.setTypeface(DataEngine.gettypeface(DetailedNewsList.this));
		short_desc.setMovementMethod(LinkMovementMethod.getInstance());
		share_icon = (ImageView) findViewById(R.id.news_share);
		publish_txt = (TextView) findViewById(R.id.pub_txt);
		//publish_txt.setTypeface(DataEngine.gettypeface(DetailedNewsList.this));
		image = (NetworkImageView) findViewById(R.id.news_image);
	}

	/**
	 *  Adding click listeners to the respective images,buttons
	 */
	private void setListeners() {
		share_icon.setOnClickListener(this);
	}

	/**
	 *  Assigning the values to the respective layout components defined.
	 */
	private void dataSet() {
		if (parser_obj.fullnews_data.size()>0) {
			//helper_obj=parser_obj.fullnews_data.get(0);
			header_txt.setText(parser_obj.fullnews_data.get(0).getTitle());
			title_txt.setText(parser_obj.fullnews_data.get(0).getTitle());
			String short_description=parser_obj.fullnews_data.get(0).getFull_des();
			short_description = short_description.replace("\\<.*?\\>", "");
			short_desc.setText(Html.fromHtml(short_description));
			publish_txt.setText(getString(R.string.publish) + " "
					+ convertDate(parser_obj.fullnews_data.get(0).getPubdate(),DATE_FORMAT));
			shareToFbTwitter = parser_obj.fullnews_data.get(0).getItemurl();
			image.setDefaultImageResId(R.drawable.news_noimage);
			image.setImageUrl(parser_obj.fullnews_data.get(0).getImageurl(), imageLoader);
		}
	}
	
	class DataLoader extends AsyncTask<Void, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if (progress != null) {
				if (!progress.isShowing()) {
					progress.show();
				}
			}
			
		}

		@Override
		protected String doInBackground(Void... params) {
			String exception =null;
			try {
				String url=DataEngine.BASE_URL+DataEngine.DETAILED_PULLURL+news_id;
				parser_obj.getNewsList(url);
			} catch (Exception e) {
				exception=e.getMessage();
			}
			return exception;
		}

		@Override
		protected void onPostExecute(String result) {
			if (progress != null) {
				if (progress.isShowing()) {
					 progress.dismiss();
				}
			}
			if(parser_obj.fullnews_data.size()>0){
			dataSet();
			}
			super.onPostExecute(result);
		}
	}

	/**
	 * call method for back button
	 * @param v back button view
	 */
	public void call_back(View v) {
		Log.e("", "-------------------------------y----------");
		//DetailedNewspush.this.overridePendingTransition(R.anim.slide_out_left,R.anim.slide_in_right);
		Intent notificationIntent = new Intent(this,Home.class);
		// set intent so it does not start a new activity
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		notificationIntent.putExtras(getIntent().getExtras());
		notificationIntent.putExtra("PUSH_NOTIFICATION", false);
		startActivity(notificationIntent);
		finish();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.news_share:
			doShare();
			break;
		default:
			break;
		}

	}
	
	@Override
	public void onBackPressed() {
		Intent notificationIntent = new Intent(this,Home.class);
		// set intent so it does not start a new activity
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		notificationIntent.putExtras(getIntent().getExtras());
		notificationIntent.putExtra("PUSH_NOTIFICATION", false);
		startActivity(notificationIntent);
		finish();
	}

	/**
	 *  Shows you the dialog when the user clicks on the sharing image.
	 */
	public void doShare() {
		final Dialog view = new Dialog(this, android.R.style.Theme);
		view.requestWindowFeature(Window.FEATURE_NO_TITLE);
		view.setContentView(R.layout.mail_twitter_facebook);
		view.getWindow().setBackgroundDrawable(
		new ColorDrawable(android.graphics.Color.TRANSPARENT));
		// view.setCancelable(true);
		view.setCanceledOnTouchOutside(true);
		view.findViewById(R.id.shar_lay).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						view.dismiss();
					}
				});
		view.findViewById(R.id.twitter).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						doTweetShare();
						view.dismiss();
					}
				});
		view.findViewById(R.id.facebook).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						doFbShare();
						view.dismiss();

					}
				});
		
		/**
		 * Mail listener,sends mail
		 */
		view.findViewById(R.id.emailTxt).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						String tempData = "";
						try {
							if (parser_obj.fullnews_data.get(0).getFull_des().length() > 0) {
								tempData = android.text.Html.fromHtml(parser_obj.fullnews_data.get(0).getFull_des()).toString().substring(
										0, 250)+"..";
							}
						} catch (Exception e) {
							tempData = android.text.Html.fromHtml(parser_obj.fullnews_data.get(0).getFull_des()).toString();
						}
						string_msg ="<br />" + tempData + "<br><br>" + shareToFbTwitter + "<br><br>" +Email_str;
						Intent intent = new Intent(Intent.ACTION_SEND);
						intent.setType("text/html");
						intent.putExtra(Intent.EXTRA_SUBJECT,getString(R.string.email_header)+" "+parser_obj.fullnews_data.get(0).getTitle().replace("#", ""));
						intent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(string_msg));
						startActivity(Intent.createChooser(intent, ""));
						//startActivityForResult(Intent.createChooser(intent, ""), 9);
						view.dismiss();

					}
				});
		view.show();
	}
	
	

	public String convertDate(String dateInMilliseconds,
			String dateFormat) {
		Locale lithuanian = new Locale("sv", "SWE");
    	  SimpleDateFormat formatter = new SimpleDateFormat(dateFormat,lithuanian);
	     // Create a calendar object that will convert the date and time value in milliseconds to date. 
	      Calendar calendar = Calendar.getInstance();
	      calendar.setTimeInMillis(Long.parseLong(dateInMilliseconds)*1000);
	      return formatter.format(calendar.getTime());
	}

	/**
	 * twitter sharing
	 */
	public void doTweetShare() {
		Twitt_Sharing twitt = new Twitt_Sharing(DetailedNewspush.this,
				DataEngine.consumer_key, DataEngine.secret_key);
		string_msg = parser_obj.fullnews_data.get(0).getTitle().replace("#", "") + "\n"
				+ shareToFbTwitter;
		String_to_File();
		twitt.shareToTwitter(string_msg, casted_image);

	}

	public File String_to_File() {

		try {
			File rootSdDirectory = Environment.getExternalStorageDirectory();

			casted_image = new File(rootSdDirectory, "attachment.jpg");
			if (casted_image.exists()) {
				casted_image.delete();
			}
			casted_image.createNewFile();

			FileOutputStream fos = new FileOutputStream(casted_image);
			try {

				BitmapFactory.Options options = new BitmapFactory.Options();

				options.inSampleSize = 2;

				BufferedOutputStream bos = new BufferedOutputStream(fos);
				Bitmap mBitmap = BitmapFactory.decodeResource(getResources(),
						R.drawable.appicon);
				mBitmap.compress(CompressFormat.JPEG, 100, bos);

				bos.flush();

				bos.close();

			} catch (OutOfMemoryError ome) {
				return null;
			} catch (Exception e) {
				return null;
			}

			return casted_image;

		} catch (Exception e) {
		}
		return casted_image;

	}

	/**
	 * Facebook sharing
	 */
	public class FacebookAuthListener implements AuthListener {

		public void onAuthSucceed() {
			SessionStore.save(mFacebook, getApplicationContext());
		}

		public void onAuthFail(String error) {
		}
	}

	class FacebookLogoutListener implements LogoutListener {
		public void onLogoutBegin() {
		}

		public void onLogoutFinish() {
		}
	}

	private class UserDetailsRequestListener implements DialogListener,
			RequestListener {

		@Override
		public void onComplete(String response, Object state) {
			try {
				JSONObject json = Util.parseJson(response);
				String friedsresponse = new FBConnectQueryProcessor()
						.requestFriendsList(getApplicationContext(), mFacebook);
				JSONObject mainObj = new JSONObject(friedsresponse);
				fbId = json.getString("id");
				if (mainObj.has("error")) {
					return;
				} else {
					doFbShare();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (FacebookError e) {
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onIOException(IOException e, Object state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onFileNotFoundException(FileNotFoundException e,
				Object state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onMalformedURLException(MalformedURLException e,
				Object state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onFacebookError(FacebookError e, Object state) {

		}

		@Override
		public void onComplete(Bundle values) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onFacebookError(FacebookError e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onError(DialogError e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onCancel() {
			// TODO Auto-generated method stub

		}

	}

	class FacebookLoginDialogListener implements DialogListener {
		public void onComplete(Bundle values) {
			SessionEvents.onLoginSuccess();
			mAsyncFacebookRunner
					.request("me", new UserDetailsRequestListener());
			SessionStore.save(mFacebook, getApplicationContext());
			doFbShare();
		}

		public void onFacebookError(FacebookError error) {
			SessionEvents.onLoginError(error.getMessage());
		}

		public void onError(DialogError error) {
			SessionEvents.onLoginError(error.getMessage());
		}

		public void onCancel() {
			SessionEvents.onLoginError("Action Canceled");
		}
	}

	private void doFbShare() {
		mFacebook = new Facebook(DataEngine.FB_APPID);// new
		mAsyncFacebookRunner = new AsyncFacebookRunner(mFacebook);
		SessionStore.restore(mFacebook, getApplicationContext());
		SessionEvents.addAuthListener(new FacebookAuthListener());
		SessionEvents.addLogoutListener(new FacebookLogoutListener());
		if (mFacebook.isSessionValid()) {

			Bundle params = new Bundle();
			params.putString("message", parser_obj.fullnews_data.get(0).getTitle().replace("#", ""));
			params.putString("link", shareToFbTwitter);
			String tempData = "";
			try {
				if (parser_obj.fullnews_data.get(0).getFull_des().length() > 0) {
					tempData = android.text.Html.fromHtml(parser_obj.fullnews_data.get(0).getFull_des()).toString().substring(0, 250)
							+ "...";
				}
			} catch (Exception e) {
				tempData = android.text.Html.fromHtml(parser_obj.fullnews_data.get(0).getFull_des()).toString();
			}
			params.putString("description", tempData);
			params.putString("name", parser_obj.fullnews_data.get(0).getTitle().replace("#", ""));
			params.putString("picture", parser_obj.fullnews_data.get(0).getImageurl());
			params.putString("to", fbId);
			mFacebook.dialog(this, "feed", params, new DialogListener() {

				@Override
				public void onFacebookError(FacebookError ee) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onError(DialogError ee) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onComplete(Bundle values) {
					// TODO Auto-generated method stub
					if(!values.isEmpty())
					showFbDeliverySuccess();
				}

				@Override
				public void onCancel() {
					// TODO Auto-generated method stub
				}
			});

		} else {
			mFacebook.authorize(DetailedNewspush.this,
					Util.FACEBOOK_PERMISSIONS, Facebook.FORCE_DIALOG_AUTH,
					new FacebookLoginDialogListener());
		}

	}

	private void showFbDeliverySuccess() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(getString(R.string.facebook_succes));
		builder.setTitle(getString(R.string.facebook_alert));

		builder.setNegativeButton(getString(R.string.ok),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub

						dialog.dismiss();
						try {
							if (progress.isShowing())
								progress.dismiss();
						} catch (Exception e) {
							// TODO: handle exception
						}
					}
				});

		builder.show();

	}

}

