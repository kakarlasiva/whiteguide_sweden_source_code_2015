package com.se.whiteguide.screens;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.samples.facebook.AsyncFacebookRunner;
import com.facebook.samples.facebook.AsyncFacebookRunner.RequestListener;
import com.facebook.samples.facebook.DialogError;
import com.facebook.samples.facebook.Facebook;
import com.facebook.samples.facebook.Facebook.DialogListener;
import com.facebook.samples.facebook.FacebookError;
import com.facebook.samples.facebook.SessionEvents;
import com.facebook.samples.facebook.SessionEvents.AuthListener;
import com.facebook.samples.facebook.SessionEvents.LogoutListener;
import com.facebook.samples.facebook.SessionStore;
import com.facebook.samples.facebook.Util;
import com.facebook.samples.fbconnect.FBConnectQueryProcessor;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.live.se.whiteguide.R;
import com.se.whiteguide.NetworkChecker;
import com.se.whiteguide.dataengine.DataEngine;
import com.se.whiteguide.dataengine.FavouritesHandler;
import com.se.whiteguide.dataengine.InitialInformation;
import com.se.whiteguide.dataengine.PaymentInformation;
import com.se.whiteguide.dataengine.Restaurant;
import com.se.whiteguide.helpers.BasicInfoBean;
import com.se.whiteguide.helpers.CirclePageIndicator;
import com.se.whiteguide.log.MyDebug;
import com.se.whiteguide.tabgroup.Home;
import com.se.whiteguide.tabgroup.ListGroup;
import com.se.whiteguide.twitter.Twitt_Sharing;

/**
 * Displays entire details about restaurant/cafe which is clicked
 * 
 * @author Conevo
 */

public class DetailedRestaurantPushView extends AnimActivity {
	TextView description, imagecount, descriptionHeading, descriptionfulltext,
			openingtime, price, phone_id, mail_id, address_id;
	TextView detaildescription;
	GridView grid;
	DisplayMetrics metrics;
	private static String TAG = "DetailedRestaurantView";
	Bitmap bitmap;
	ImageView imageView, FavBtnClicked;
	Button gallery;
	ImageView twitter;
	GridView gallu;
	EdgeEffectViewPager viewPager;
	ImagePagerAdapter adapter;
	CirclePageIndicator mIndicator;
	FavouritesHandler treatHandler;
	ProgressDialog progress;
	private String string_msg = null;
	Vector<Restaurant> favourites;
	FavouritesHandler favHandler;
	File casted_image;
	Restaurant restaurant;
	private GoogleMap googleMap;
	double lat = 0.0, lan = 0.0;
	Facebook mFacebook;
	LinearLayout kr_layout;
	SharedPreferences settings;
	AsyncFacebookRunner mAsyncFacebookRunner;
	private String fbId;
	Map<String, Integer> symbol_value = new HashMap<String, Integer>();
	Map<String, Integer> symbol_name_value = new HashMap<String, Integer>();

	public Vector<String> gettigList = new Vector<String>();
	public static HashMap<String, Bitmap> finalBitmaps;
	// To update images in a new thread.
	UpdateImages updateImages;
	Handler handler;
	Vector<String> urlList;
	Vector<ImageView> imgList;
	ImageView clasificationImage;
	TextView totalPoint, mad_txt, service_txt;
	InitialInformation information;
	boolean isbought = false;
	String guideId;
	BasicInfoBean beanObj;
	PaymentInformation payInfo;
	Bitmap marker_bitmap, cof_bitmap, Nytestat_bitmap;
	int height, width;
	String tagline = "", address = "", phone = "", website = "", body = "",
			cheapstPrice = "", opentime = "", zipcode = "",
			expensive_entre = "", cheapest_main_dish = "",
			expensive_main_dish = "", cheapest_dessert = "",
			expensive_dessert = "", cheapest_menu = "", expensive_menu = "",
			city = "", email = "", shareToFbTwitter = "", food = "",
			service = "";
	LinearLayout score_layout;
	AdView adView;
	public static String Email_str = "Hela recensionen kan du l�sa i White Guide App.<br /> White Guide App �r hela Sveriges restaurangguide, med den f�r du ocks� tillg�ng till det senaste som h�nt i krogsverige, l�ngre <br/>reportage och v�ra senast testade restauranger.<br />Ladda ned din White Guide App fr�n App Store <a href=\"https://itunes.apple.com/us/app/apple-store/id878872712?mt=8\">H�R</a> <br />Ladda ned din White Guide App fr�n Google Play <a href=\"https://play.google.com/store/apps/details?id=com.live.se.whiteguide\">H�R</a>";

	URL url;
	HttpURLConnection connection;
	InputStream input;
	Bitmap srcBmp;
	BitmapFactory.Options o;

	/***
	 * Runnable to download the images...update the adapter with each image
	 * download completion.
	 */
	class UpdateImages extends Thread {
		@Override
		public void run() {
			try {
				for (String treat : urlList) {
					boolean isThere = false;
					try {
						if (null != finalBitmaps) {
							if (null != finalBitmaps.get(treat)) {
								isThere = true;
							}
						}
						if (!isThere) {
							url = new URL(treat);
							connection = (HttpURLConnection) url
									.openConnection();
							connection.setDoInput(true);
							connection.connect();
							input = connection.getInputStream();
							o = new BitmapFactory.Options();
							o.inJustDecodeBounds = false;
							o.inSampleSize = 2;
							srcBmp = BitmapFactory.decodeStream(input, null, o);
							finalBitmaps.put(treat, srcBmp);
						}
					} catch (Exception e) {
						finalBitmaps.put(treat, null);
					}
					handler.post(new Runnable() {

						@Override
						public void run() {
							MyDebug.log_info("Images",
									"The Final Number of Images here is: ",
									String.valueOf(finalBitmaps.size()));
							if (null != adapter) {
								adapter.notifyDataSetChanged();
							}
						}
					});
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		};
	}

	/**
	 * Facebook sharing
	 */
	public class FacebookAuthListener implements AuthListener {

		public void onAuthSucceed() {
			SessionStore.save(mFacebook, getApplicationContext());
		}

		public void onAuthFail(String error) {
		}
	}

	class FacebookLogoutListener implements LogoutListener {
		public void onLogoutBegin() {
		}

		public void onLogoutFinish() {
		}
	}

	private class UserDetailsRequestListener implements DialogListener,
			RequestListener {

		@Override
		public void onComplete(String response, Object state) {
			try {
				JSONObject json = Util.parseJson(response);
				String friedsresponse = new FBConnectQueryProcessor()
						.requestFriendsList(getApplicationContext(), mFacebook);
				JSONObject mainObj = new JSONObject(friedsresponse);
				fbId = json.getString("id");
				if (mainObj.has("error")) {
					return;
				} else {
					doFbShare();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (FacebookError e) {
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onIOException(IOException e, Object state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onFileNotFoundException(FileNotFoundException e,
				Object state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onMalformedURLException(MalformedURLException e,
				Object state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onFacebookError(FacebookError e, Object state) {

		}

		@Override
		public void onComplete(Bundle values) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onFacebookError(FacebookError e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onError(DialogError e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onCancel() {
			// TODO Auto-generated method stub

		}

	}

	class FacebookLoginDialogListener implements DialogListener {
		public void onComplete(Bundle values) {
			SessionEvents.onLoginSuccess();
			mAsyncFacebookRunner
					.request("me", new UserDetailsRequestListener());
			SessionStore.save(mFacebook, getApplicationContext());
			doFbShare();
		}

		public void onFacebookError(FacebookError error) {
			SessionEvents.onLoginError(error.getMessage());
		}

		public void onError(DialogError error) {
			SessionEvents.onLoginError(error.getMessage());
		}

		public void onCancel() {
			SessionEvents.onLoginError("Action Canceled");
		}
	}

	private void doFbShare() {
		mFacebook = new Facebook(DataEngine.FB_APPID);// new
		mAsyncFacebookRunner = new AsyncFacebookRunner(mFacebook);
		SessionStore.restore(mFacebook, getApplicationContext());
		SessionEvents.addAuthListener(new FacebookAuthListener());
		SessionEvents.addLogoutListener(new FacebookLogoutListener());
		if (mFacebook.isSessionValid()) {

			Bundle params = new Bundle();
			params.putString("message", restaurant.getTitle().replace("#", ""));
			params.putString("link", shareToFbTwitter);
			String tempData = "";
			try {
				if (body.length() > 0) {
					tempData = body.substring(0, 250) + "...";
				}
			} catch (Exception e) {
				tempData = body;
			}
			params.putString("description", tempData);
			params.putString("name", restaurant.getTitle().replace("#", ""));
			params.putString(
					"picture",
					"http://www.whiteguide.se/sites/default/themes/wgtheme/gfx/static-logo-large.png");
			params.putString("to", fbId);
			mFacebook.dialog(this, "feed", params, new DialogListener() {

				@Override
				public void onFacebookError(FacebookError ee) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onError(DialogError ee) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onComplete(Bundle values) {
					// TODO Auto-generated method stub
					if (!values.isEmpty())
						showFbDeliverySuccess();
				}

				@Override
				public void onCancel() {
					// TODO Auto-generated method stub
				}
			});

		} else {
			mFacebook.authorize(DetailedRestaurantPushView.this,
					Util.FACEBOOK_PERMISSIONS, Facebook.FORCE_DIALOG_AUTH,
					new FacebookLoginDialogListener());
		}

	}

	private void showFbDeliverySuccess() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(getString(R.string.facebook_succes));
		builder.setTitle(getString(R.string.facebook_alert));

		builder.setNegativeButton(getString(R.string.ok),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub

						dialog.dismiss();
						try {
							if (progress.isShowing())
								progress.dismiss();
						} catch (Exception e) {
							// TODO: handle exception
						}
					}
				});

		builder.show();

	}

	/**
	 * twitter sharing
	 */
	public void doTweetShare() {
		Twitt_Sharing twitt = new Twitt_Sharing(DetailedRestaurantPushView.this,
				DataEngine.consumer_key, DataEngine.secret_key);
		string_msg = restaurant.getTitle().replace("#", "") + "\n"
				+ shareToFbTwitter;
		String_to_File();
		twitt.shareToTwitter(string_msg, casted_image);

	}

	public void doShare() {
		final Dialog view = new Dialog(this, android.R.style.Theme);
		// view.requestWindowFeature(Window.FEATURE_NO_TITLE);
		view.setContentView(R.layout.mail_twitter_facebook);
		view.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		view.findViewById(R.id.shar_lay).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						view.dismiss();
					}
				});
		view.findViewById(R.id.twitter).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						doTweetShare();
						view.dismiss();
					}
				});
		view.findViewById(R.id.facebook).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						doFbShare();
						view.dismiss();

					}
				});
		/*
		 * view.findViewById(R.id.close_button).setOnClickListener(new
		 * OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { view.dismiss(); } });
		 */
		/**
		 * Mail listener,sends mail
		 */
		view.findViewById(R.id.emailTxt).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						String tempData = "";
						try {
							if (body.length() > 0) {
								tempData = body.substring(0, 250);
							}
						} catch (Exception e) {
							tempData = body;
						}
						string_msg = restaurant.getTitle().replace("#", "")
								+ "\n\n" + address + "\n\n" + tempData + "\n\n"
								+ "<br><br>" + shareToFbTwitter + "<br><br>"
								+ Email_str;
						;
						Intent intent = new Intent(Intent.ACTION_SEND);
						intent.setType("plain/text");
						intent.putExtra(Intent.EXTRA_SUBJECT,
								getString(R.string.email_header)
										+ " "
										+ restaurant.getTitle()
												.replace("#", ""));
						intent.putExtra(Intent.EXTRA_TEXT,
								Html.fromHtml(string_msg));
						startActivity(Intent.createChooser(intent, ""));
						view.dismiss();

					}
				});
		view.show();
	}

	TextView header_text;

	/**
	 * adapter is used to display small symbols
	 */
	public class SymbolsAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return gettigList.size();
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			ImageView image = new ImageView(DetailedRestaurantPushView.this);
			if (metrics.densityDpi == DisplayMetrics.DENSITY_HIGH
					|| metrics.densityDpi == DisplayMetrics.DENSITY_MEDIUM
					|| metrics.densityDpi == DisplayMetrics.DENSITY_LOW) {
				image.setLayoutParams(new GridView.LayoutParams(50, 50));

			} else {
				if (metrics.densityDpi == DisplayMetrics.DENSITY_XXHIGH
						|| metrics.densityDpi == DisplayMetrics.DENSITY_XXXHIGH) {
					image.setLayoutParams(new GridView.LayoutParams(120, 120));
				} else {
					image.setLayoutParams(new GridView.LayoutParams(80, 80));
				}
			}
			image.setScaleType(ScaleType.CENTER_CROP);
			try {

				image.setImageResource(symbol_value.get(gettigList
						.get(position)));
			} catch (Exception e) {

			}
			return image;
		}

	}

	// Typeface typeFace;
	Background sc;
	boolean isPush = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		init();
		imgList = new Vector<ImageView>();
		payInfo = new PaymentInformation(getApplicationContext());
		isPush = getIntent().getBooleanExtra("PUSH", false);
		Log.e("ACTIVITY", "On Create of the " + isPush);
		beanObj = (BasicInfoBean) getIntent().getSerializableExtra("GUIDE");
		setContentView(R.layout.main);
		grid = (GridView) findViewById(R.id.symbols);
		EdgeEffectScrollView gridView = (EdgeEffectScrollView) findViewById(R.id.scrollView);
		gridView.setEdgeEffectColor(Color.parseColor("#71D3FF"));
		favourites = new Vector<Restaurant>();
		favHandler = new FavouritesHandler(getApplicationContext());
		try {
			favourites = favHandler.readFavorite();
		} catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}
		imagecount = (TextView) findViewById(R.id.imagecount);
		restaurant = (Restaurant) getIntent().getSerializableExtra("RES");
		header_text = (TextView) findViewById(R.id.header_text);

		adView = (AdView) findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder().build();
		if (DataEngine.showAds)
			adView.loadAd(adRequest);
		else
			adView.setVisibility(View.GONE);
		String headerText = restaurant.getTitle().replace("#", "");
		if (headerText.length() > 0) {
			headerText = headerText.substring(0, 1).toUpperCase()
					+ headerText.substring(1);
		}
		header_text.setText(headerText);
		score_layout = (LinearLayout) findViewById(R.id.scores_layout);
		mad_txt = (TextView) findViewById(R.id.mad);
		service_txt = (TextView) findViewById(R.id.service);

		// Display Metrics object to get the screen Resolution.
		metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);

		handler = new Handler();
		finalBitmaps = new HashMap<String, Bitmap>();
		urlList = new Vector<String>();
		kr_layout = (LinearLayout) findViewById(R.id.kr_layout);
		clasificationImage = (ImageView) findViewById(R.id.classificationImg);
		totalPoint = (TextView) findViewById(R.id.count);
		openingtime = (TextView) findViewById(R.id.time);
		price = (TextView) findViewById(R.id.price);
		phone_id = (TextView) findViewById(R.id.phone);
		address_id = (TextView) findViewById(R.id.address);
		mail_id = (TextView) findViewById(R.id.mail);
		descriptionHeading = (TextView) findViewById(R.id.textView1);
		detaildescription = (TextView) findViewById(R.id.textView2);
		descriptionfulltext = (TextView) findViewById(R.id.fulltext);
		viewPager = (EdgeEffectViewPager) findViewById(R.id.view_pager);
		viewPager.setEdgeEffectColor(Color.parseColor("#71D3FF"));
		mIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
		treatHandler = new FavouritesHandler(this);
		FavBtnClicked = (ImageView) findViewById(R.id.FavBtn);
		gallery = (Button) findViewById(R.id.gallery);
		twitter = (ImageView) findViewById(R.id.button2);
		gallu = (GridView) findViewById(R.id.gallery1);

		progress = new ProgressDialog(this);
		progress.setCancelable(true);
		progress.setMessage(getString(R.string.loading));
		if (!NetworkChecker.isNetworkOnline(this)) {
			showErrorDialog();
			return;
		}
		sc = new Background();
		sc.execute();

		setListeners();

		lat = restaurant.getLat();
		lan = restaurant.getLon();

		initilizeMap();
		findViewById(R.id.scrollView).setFadingEdgeLength(4);

	}

	/**
	 * Hash map uesd to store key and value of symbols in restaurant/cafe
	 */
	private void init() {

		symbol_value.put("1", R.drawable.handikappvanligt_1);
		symbol_value.put("2", R.drawable.bar_2);
		symbol_value.put("3", R.drawable.enklare_mat_3);
		symbol_value.put("4", R.drawable.brunch_4);
		symbol_value.put("5", R.drawable.parkering_5);
		symbol_value.put("6", R.drawable.vegetariskt_6);
		symbol_value.put("7", R.drawable.overnattningsmojligheter_7);
		symbol_value.put("8", R.drawable.festvaning_8);
		symbol_value.put("9", R.drawable.chambre_separee_9);
		symbol_value.put("10", R.drawable.uteservering_10);
		symbol_value.put("11", R.drawable.utmarkt_dryckesutbud_11);
		symbol_value.put("12", R.drawable.hoga_eko_ambitioner_12);
		symbol_value.put("14", R.drawable.medlem_visita_14);
		symbol_value.put("15", R.drawable.american_express_15);
		symbol_value.put("54", R.drawable.hjartekrog_81);
		symbol_value.put("72", R.drawable.gratis_wifi_72);
		symbol_value.put("74", R.drawable.krav);
		symbol_value.put("75", R.drawable.krav);
		symbol_value.put("81", R.drawable.hjartekrog_81);
		symbol_value.put("82", R.drawable.ny_82);
		symbol_value.put("83", R.drawable.comeback_83);
		symbol_value.put("54", R.drawable.hjartekrog_81);
		symbol_value.put("80", R.drawable.whiteguide_android_55);
		symbol_value.put("79", R.drawable.whiteguide_android_60);
		symbol_value.put("40", R.drawable.whiteguide_android_61);
		symbol_value.put("16", R.drawable.whiteguide_android_62);

	}

	/**
	 * Alert is shown when there is no network
	 */
	private void showErrorDialog() {
		// TODO Auto-generated method stub
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(getString(R.string.networkerror));
		builder.setTitle(getString(R.string.warning));
		builder.setPositiveButton(getString(R.string.ok),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						finish();
					}
				});
		builder.show();
	}

	/**
	 * Listeners for all on click event
	 */
	private void setListeners() {
		// TODO Auto-generated method stub
		descriptionfulltext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showAlertBox();
			}
		});
		grid.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub

				Intent nextActivity = new Intent(DetailedRestaurantPushView.this,
						SwipeView.class);
				startActivity(nextActivity);
				// overridePendingTransition(R.anim.slide_in_right,
				// R.anim.slide_out_left);
				// overridePendingTransition(R.anim.push_up_in,
				// R.anim.push_up_out);

			}
		});
		FavBtnClicked.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				boolean isFavourite = false;
				try {
					favourites = favHandler.readFavorite();
					for (Restaurant dummy : favourites) {
						if (restaurant.getId().equalsIgnoreCase(dummy.getId())) {
							isFavourite = true;
							break;
						}
					}

					if (isFavourite) {
						((ImageView) findViewById(R.id.FavBtn))
								.setImageResource(R.drawable.fav_open);
						treatHandler.deleteFavorite(restaurant.getId());
					} else {
						((ImageView) findViewById(R.id.FavBtn))
								.setImageResource(R.drawable.fav_selected);
						treatHandler.writeFavorite(restaurant);
					}
				} catch (Exception e) {
					MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
				}
			}
		});

		descriptionfulltext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (beanObj.isGuideisBought()) {
					descriptionfulltext.setVisibility(View.GONE);
				} else {
					showAlertBox();
				}
			}
		});
		findViewById(R.id.classificationImg).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						if (beanObj.isGuideisBought()
								|| beanObj
										.getGuideName()
										.equalsIgnoreCase(
												DetailedRestaurantPushView.this
														.getString(R.string.Nytestat_name))) {
							Intent nextActivity = new Intent(
									DetailedRestaurantPushView.this,
									CategoryDetails.class);
							startActivity(nextActivity);
						} else {
							showAlertBox();
						}
					}
				});
		findViewById(R.id.back).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// DetailedRestaurantview.this.overridePendingTransition(R.anim.slide_out_left,
				// R.anim.slide_in_right);
				if (isPush) {

					Intent notificationIntent = new Intent(
							DetailedRestaurantPushView.this, Home.class);
					// set intent so it does not start a new activity
					notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_SINGLE_TOP);
					notificationIntent.putExtras(getIntent().getExtras());
					notificationIntent.putExtra("PUSH_NOTIFICATION", false);
					startActivity(notificationIntent);
				}
				finish();
			}

		});
		gallery.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				Intent nextActivity = new Intent(DetailedRestaurantPushView.this,
						GalleryPage.class);
				nextActivity.putExtra("IMAGES", urlList);
				nextActivity.putExtra("POSITION", 0);
				startActivity(nextActivity);
			}
		});

		twitter.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				doShare();
			}
		});
	}

	/**
	 * Alert forces user to buy guide
	 */
	protected void showAlertBox() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(getString(R.string.guide_buy1) + " "
				+ beanObj.getGuideName() + " " + beanObj.getGuideYear() + " "
				+ getString(R.string.guide_buy2));
		builder.setPositiveButton(getString(R.string.buy),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
						Home.setTab(4);
					}
				});
		builder.setNegativeButton(getString(R.string.cancel),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		builder.show();

	}

	/**
	 * decides the content according to user either free user or guide owner
	 */
	public void decideDataVisibility(boolean purchased) {
		try {
			boolean isFavourite = false;
			for (Restaurant dummy : favourites) {
				if (restaurant.getId().equalsIgnoreCase(dummy.getId())) {
					isFavourite = true;
					break;
				}
			}
			if (isFavourite)
				((ImageView) findViewById(R.id.FavBtn))
						.setImageResource(R.drawable.fav_selected);
			else
				((ImageView) findViewById(R.id.FavBtn))
						.setImageResource(R.drawable.fav_open);
		} catch (Exception e) {

		}
		if (purchased
				|| beanObj.getGuideName().equalsIgnoreCase(
						DetailedRestaurantPushView.this
								.getString(R.string.Nytestat_name))) {
			detaildescription.setPadding(0, 0, 0, 40);
			detaildescription.setText(body);
			descriptionfulltext.setVisibility(View.GONE);
			if (restaurant.getRestaurantClass().contains("1"))
				clasificationImage.setImageResource(R.drawable.intklass);
			else if (restaurant.getRestaurantClass().contains("2"))
				clasificationImage.setImageResource(R.drawable.masterklass);
			else if (restaurant.getRestaurantClass().contains("3"))
				clasificationImage.setImageResource(R.drawable.myketgodklass);
			else if (restaurant.getRestaurantClass().contains("4"))
				clasificationImage.setImageResource(R.drawable.godklass);
			else if (restaurant.getRestaurantClass().contains("7"))
				clasificationImage.setImageResource(R.drawable.brastalle);
			if (restaurant.getScore() > 0
					&& !restaurant.getRestaurantClass().contains("7")) {
				totalPoint.setVisibility(View.VISIBLE);
				totalPoint.setText("" + restaurant.getScore());
			}
		} else {
			String tempData = "";
			try {
				if (body.length() > 0) {
					tempData = body.substring(0, 250) + "...";
				}
			} catch (Exception e) {
				tempData = body;
			}
			detaildescription.setText(tempData);
			descriptionfulltext.setVisibility(View.VISIBLE);
		}
	}

	/**
	 * calls another class when symbol is clicked.
	 */
	public void callswipe(View v) {
		Intent nextActivity = new Intent(this, SwipeView.class);
		nextActivity.putExtra("RES", restaurant);
		ListGroup parentActivity = (ListGroup) getParent();
		parentActivity.startChildActivity("swipe view", nextActivity);
		// overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
	}

	/**
	 * function to load map If map is not created it will create it for you
	 * */
	@SuppressLint("NewApi")
	private void initilizeMap() {
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

		// Height & width of the Screen.
		height = displaymetrics.heightPixels;
		width = displaymetrics.widthPixels;
		if (googleMap == null) {
			googleMap = ((MapFragment) getFragmentManager().findFragmentById(
					R.id.map)).getMap();
		}
		// check if map is created successfully or not
		if (googleMap == null) {
			Toast.makeText(this, "Sorry! unable to create maps",
					Toast.LENGTH_SHORT).show();
		} else {
			googleMap.setOnMapClickListener(new OnMapClickListener() {

				@Override
				public void onMapClick(LatLng arg0) {
					Intent intent = new Intent(DetailedRestaurantPushView.this,
							DetailedMap.class);
					intent.putExtra("RES", restaurant);
					startActivity(intent);
				}
			});
			googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
			// Enable / Disable zooming controls
			googleMap.getUiSettings().setZoomControlsEnabled(false);
			// Enable / Disable Compass icon
			googleMap.getUiSettings().setCompassEnabled(true);
			// Enable / Disable Rotate gesture
			googleMap.getUiSettings().setRotateGesturesEnabled(true);
			// Enable / Disable zooming functionality
			googleMap.getUiSettings().setZoomGesturesEnabled(true);
			// Adding a marker
			MarkerOptions marker;

			Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
					R.drawable.marker);
			Bitmap coffee_bitmap = BitmapFactory.decodeResource(getResources(),
					R.drawable.marker2);
			Bitmap Nytest_bit = BitmapFactory.decodeResource(getResources(),
					R.drawable.newres_marker);
			marker_bitmap = Bitmap.createScaledBitmap(bitmap, 57, 70, true);
			cof_bitmap = Bitmap.createScaledBitmap(coffee_bitmap, 57, 70, true);
			Nytestat_bitmap = Bitmap.createScaledBitmap(Nytest_bit, 57, 70,
					true);
			// Adding a marker
			if (beanObj.getGuideId().matches("58")) {
				marker = new MarkerOptions()
						.position(new LatLng(lat, lan))
						.icon(BitmapDescriptorFactory.fromBitmap(marker_bitmap))
						.title(restaurant.getTitle().replace("#", ""));
				googleMap.addMarker(marker);
			} else if (beanObj.getGuideId().matches("60")) {
				marker = new MarkerOptions().position(new LatLng(lat, lan))
						.icon(BitmapDescriptorFactory.fromBitmap(cof_bitmap))
						.title(restaurant.getTitle().replace("#", ""));
				googleMap.addMarker(marker);
			} else if (beanObj.getGuideId().matches("90")) {
				marker = new MarkerOptions()
						.position(new LatLng(lat, lan))
						.icon(BitmapDescriptorFactory
								.fromBitmap(Nytestat_bitmap))
						.title(restaurant.getTitle().replace("#", ""));
				googleMap.addMarker(marker);
			} else if (beanObj.getGuideId().matches("59")) {
				marker = new MarkerOptions()
						.position(new LatLng(lat, lan))
						.icon(BitmapDescriptorFactory
								.fromResource(R.drawable.marker_bar))
						.title(restaurant.getTitle().replace("#", ""));
				googleMap.addMarker(marker);
			}
			LatLng latLng = new LatLng(lat, lan);
			CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
					latLng, 15);
			googleMap.animateCamera(cameraUpdate);

		}

	}

	public void call_back(View v) {
		// DetailedRestaurantview.this.overridePendingTransition(R.anim.slide_out_left,
		// R.anim.slide_in_right);
		Log.e(TAG, "-----------call--------" + isPush);
		if (isPush) {

			Intent notificationIntent = new Intent(this, Home.class);
			// set intent so it does not start a new activity
			notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_SINGLE_TOP);
			notificationIntent.putExtras(getIntent().getExtras());
			notificationIntent.putExtra("PUSH_NOTIFICATION", false);
			startActivity(notificationIntent);
		}
		finish();
	}

	/**
	 * adapter is used to display image in viewpager
	 */
	private class ImagePagerAdapter extends PagerAdapter {

		@Override
		public int getCount() {
			if (urlList.size() > 0)
				return urlList.size();
			else
				return 1;
		}

		@Override
		public void notifyDataSetChanged() {
			try {
				for (int i = 0; i < imgList.size(); i++) {
					if (urlList.size() > 0 && urlList.size() > i) {
						if (null != finalBitmaps.get(urlList.get(i))) {
							imgList.get(i).setImageBitmap(
									finalBitmaps.get(urlList.get(i)));
						} else
							imgList.get(i).setImageResource(
									R.drawable.wg_noimage);
					} else {
						imgList.get(i).setImageResource(R.drawable.wg_noimage);
					}
				}
				super.notifyDataSetChanged();
			} catch (OutOfMemoryError e) {

			}

		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == ((ImageView) object);
		}

		@Override
		public Object instantiateItem(ViewGroup container, final int position) {
			Context context = DetailedRestaurantPushView.this;
			imageView = new ImageView(context);
			imageView.setLayoutParams(new LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			imageView.setScaleType(ScaleType.CENTER_CROP);
			try {
				if (urlList.size() > 0) {
					if (null != finalBitmaps.get(urlList.get(position)))
						imageView.setImageBitmap(finalBitmaps.get(urlList
								.get(position)));
					else
						imageView.setImageResource(R.drawable.wg_noimage);

					imagecount.setText("" + urlList.size());
				} else {
					imageView.setImageResource(R.drawable.wg_noimage);
				}
			} catch (OutOfMemoryError ome) {
				ome.printStackTrace();
			}
			imageView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (urlList.size() > 0) {
						Intent nextActivity = new Intent(
								DetailedRestaurantPushView.this, GalleryPage.class);
						nextActivity.putExtra("IMAGES", urlList);
						nextActivity.putExtra("RES", restaurant);
						nextActivity.putExtra("POSITION", position);
						startActivity(nextActivity);
					}
				}
			});
			imgList.add(imageView);
			((ViewPager) container).addView(imageView);
			return imageView;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			((ViewPager) container).removeView((ImageView) object);
		}
	}

	public File String_to_File() {

		try {
			File rootSdDirectory = Environment.getExternalStorageDirectory();

			casted_image = new File(rootSdDirectory, "attachment.jpg");
			if (casted_image.exists()) {
				casted_image.delete();
			}
			casted_image.createNewFile();

			FileOutputStream fos = new FileOutputStream(casted_image);
			try {

				BitmapFactory.Options options = new BitmapFactory.Options();

				options.inSampleSize = 2;

				BufferedOutputStream bos = new BufferedOutputStream(fos);
				Bitmap mBitmap = BitmapFactory.decodeResource(getResources(),
						R.drawable.appicon);
				mBitmap.compress(CompressFormat.JPEG, 100, bos);

				bos.flush();

				bos.close();

			} catch (OutOfMemoryError ome) {
				return null;
			} catch (Exception e) {
				return null;
			}

			return casted_image;

		} catch (Exception e) {
		}
		return casted_image;

	}

	/**
	 * background task does json parsing and displaying content accordingly
	 */

	String response1 = "";

	public class Background extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			if (null != progress) {
				if (!progress.isShowing())
					progress.show();
			}
		}

		@Override
		protected void onPostExecute(String result) {
			JSONObject jobjdata = null;
			if (finalBitmaps.size() >= 0) {
				adapter = new ImagePagerAdapter();
				viewPager.setAdapter(adapter);
				mIndicator.setViewPager(viewPager);
				updateImages = new UpdateImages();
				updateImages.start();
			} else {
				gallery.setVisibility(View.GONE);
				imagecount.setVisibility(View.GONE);
				mIndicator.setVisibility(View.GONE);
			}
			if (finalBitmaps.size() <= 4) {
				gallery.setVisibility(View.GONE);
				imagecount.setVisibility(View.GONE);
			}
			if (urlList.size() <= 1) {
				mIndicator.setVisibility(View.INVISIBLE);
			}
			try {
				jobjdata = new JSONObject(result);
				JSONArray jarray = jobjdata.getJSONArray("symbols");

				for (int i = 0; i < jarray.length(); i++) {
					JSONObject info = jarray.getJSONObject(i);
					String id = info.getString("id");
					if (symbol_value.containsKey(id))
						gettigList.add(id);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				tagline = jobjdata.optString("tagline");
				address = jobjdata.optString("address");
				email = jobjdata.optString("email");
				phone = jobjdata.optString("phone");
				website = jobjdata.optString("website");
				body = jobjdata.optString("body");
				opentime = jobjdata.optString("open");
				zipcode = jobjdata.optString("zip");
				city = jobjdata.optString("city");
				shareToFbTwitter = jobjdata.optString("wgurl");
				service = jobjdata.getJSONObject("scores").optString("service");
				food = jobjdata.getJSONObject("scores").optString("food");
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				cheapstPrice = jobjdata.optString("cheapest_entre");
				expensive_entre = jobjdata.optString("expensive_entre");
				cheapest_main_dish = jobjdata.optString("cheapest_main_dish");
				expensive_main_dish = jobjdata.optString("expensive_main_dish");
				cheapest_dessert = jobjdata.optString("cheapest_dessert");
				expensive_dessert = jobjdata.optString("expensive_dessert");
				cheapest_menu = jobjdata.optString("cheapest_menu");
				expensive_menu = jobjdata.optString("expensive_menu");
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				StringBuilder builder = new StringBuilder();
				if (null != cheapstPrice && "null" != cheapstPrice
						&& !"".equalsIgnoreCase(cheapstPrice)
						&& null != expensive_entre && "null" != expensive_entre
						&& !"".equalsIgnoreCase(expensive_entre)) {
					builder.append("F�rr�tt " + cheapstPrice + "-"
							+ expensive_entre + " kr, ");
				}

				if (null != cheapest_main_dish && "null" != cheapest_main_dish
						&& !"".equalsIgnoreCase(cheapest_main_dish)
						&& null != expensive_main_dish
						&& "null" != expensive_main_dish
						&& !"".equalsIgnoreCase(expensive_main_dish)) {
					builder.append("Varmr�tt " + cheapest_main_dish + "-"
							+ expensive_main_dish + " kr, ");

				}

				if (null != cheapest_dessert && "null" != cheapest_dessert
						&& !"".equalsIgnoreCase(cheapest_dessert)
						&& null != expensive_dessert
						&& "null" != expensive_dessert
						&& !"".equalsIgnoreCase(expensive_dessert)) {
					builder.append("dessert " + cheapest_dessert + "-"
							+ expensive_dessert + " kr, ");

				}

				if (null != cheapest_menu && "null" != cheapest_menu
						&& !"".equalsIgnoreCase(cheapest_menu)
						&& null != expensive_menu && "null" != expensive_menu
						&& !"".equalsIgnoreCase(expensive_menu)) {
					builder.append("Meny " + cheapest_menu + "-"
							+ expensive_menu + " kr.");

				}
				String price_txt = "";
				try {
					price_txt = builder.toString();
					if (price_txt.endsWith(" ")) {
						price_txt = price_txt.substring(0,
								price_txt.length() - 2) + ".";

					}

				} catch (Exception e) {
					Log.i("TEST", "NOT THERE" + e.getLocalizedMessage());
				}
				if (builder.length() == 0) {
					builder.append("Ingen uppgift");
				}
				if (beanObj.getGuideId().matches("60")) {
					kr_layout.setVisibility(View.GONE);
				} else {
					price.setText(price_txt.trim());
				}
			} catch (Exception e) {
			}

			try {
				if (beanObj.isGuideisBought() && restaurant.getScore() > 0) {
					if (restaurant.getRestaurantClass().contains("7")) {
						score_layout.setVisibility(View.GONE);
					} else {
						score_layout.setVisibility(View.VISIBLE);
						if (null != food && "null" != food
								&& !"".equalsIgnoreCase(food)) {
							mad_txt.setText("\nMat " + food + "/40");
						}
						if (null != service && "null" != service
								&& !"".equalsIgnoreCase(service)) {
							service_txt.setText("\nService " + service + "/25");
						}
					}

				}
			} catch (Exception e) {
			}

			try {
				if (opentime.equals("null")) {
					opentime = "Ingen uppgift";
				}

				if (email.equals("null")) {
					email = "Ingen uppgift";
				}
				if (address.equals("null")) {
					address = "Ingen uppgift";
				}
				if (phone.equals("null")) {
					phone = "Ingen uppgift";
				}
				if (website.equals("null")) {
					website = "Ingen uppgift";
				}
				if (zipcode.equals("null")) {
					zipcode = "Ingen uppgift";
				}
				if (city.equals("null")) {
					city = "Ingen uppgift";
				}
				openingtime.setText(opentime.trim());
				address_id.setText(address.trim() + "," + "\n" + zipcode.trim()
						+ " " + city.trim() + ".");
				phone_id.setText(phone);
				if (website.startsWith("www.")) {
					website = website.substring(4, website.length());
				}
				mail_id.setText(website);
				descriptionHeading.setText(tagline);
				detaildescription.setText(body);
			} catch (Exception e) {

			}
			try {
				SymbolsAdapter symbolsAdapter = new SymbolsAdapter();
				grid.setAdapter(symbolsAdapter);
				findViewById(R.id.scrollView).setVisibility(View.VISIBLE);
				findViewById(R.id.button2).setVisibility(View.VISIBLE);
				decideDataVisibility(beanObj.isGuideisBought()
						|| payInfo.isThisGuidebought(beanObj.getGuideId(),
								beanObj.getGuideYear()));
				phone_id.setLinkTextColor(Color.parseColor("#000000"));
				mail_id.setLinkTextColor(Color.parseColor("#000000"));
				address_id.setLinkTextColor(Color.parseColor("#000000"));

			} catch (Exception e) {
				e.printStackTrace();
			}
			if (null != progress) {
				if (progress.isShowing())
					progress.dismiss();
			}
		}

		@Override
		protected String doInBackground(String... params) {

			StringBuilder builder = new StringBuilder();
			HttpClient httpClient = new DefaultHttpClient();
			String parse_url = null;
			if (beanObj.getGuideName().equalsIgnoreCase(
					DetailedRestaurantPushView.this
							.getString(R.string.Nytestat_name))) {
				parse_url = DataEngine.BASE_URL + "getnytestatrestaurant/"
						+ restaurant.getId() + "?access=harmoniska";
			} else {
				parse_url = DataEngine.BASE_URL + "restaurants/"
						+ restaurant.getId() + "?access=harmoniska";
			}
			HttpGet httpGet = new HttpGet(parse_url);
			try {
				HttpResponse response = httpClient.execute(httpGet);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
				} else {
				}
				response1 = builder.toString();
			} catch (Exception e) {
				e.printStackTrace();
			}
			JSONObject jobjdata = null;
			try {
				jobjdata = new JSONObject(response1);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			JSONArray picturearray = null;
			try {
				picturearray = jobjdata.getJSONArray("pictures");
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (null != picturearray)
				for (int k = 0; k < picturearray.length(); k++) {
					String picture = null;
					try {
						picture = picturearray.getString(k);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					if (null != picture) {
						finalBitmaps.put(picture, null);
						urlList.add(picture);
					}
				}
			return response1;
		}

	}

	@Override
	public void onBackPressed() {
		// DetailedRestaurantview.this.overridePendingTransition(R.anim.slide_out_left,
		// R.anim.slide_in_right);
		if (isPush) {

			Intent notificationIntent = new Intent(this, Home.class);
			// set intent so it does not start a new activity
			notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_SINGLE_TOP);
			notificationIntent.putExtras(getIntent().getExtras());
			notificationIntent.putExtra("PUSH_NOTIFICATION", false);
			startActivity(notificationIntent);
		}
		finish();
	}

	@Override
	protected void onDestroy() {
		if (urlList.size() > 0) {
			urlList.clear();
		}
		super.onDestroy();

	}

}