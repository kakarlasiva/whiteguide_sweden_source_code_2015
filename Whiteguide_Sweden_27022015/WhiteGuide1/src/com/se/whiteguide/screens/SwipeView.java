package com.se.whiteguide.screens;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.GridView;

import com.live.se.whiteguide.R;
import com.se.whiteguide.helpers.CustomGridViewAdapter;
import com.se.whiteguide.helpers.Item;

/**
 *  Represents all symbols available in restaurant/cafe
 *  @author Conevo
 */

public class SwipeView extends AnimActivity implements OnClickListener {
	GridView gridView;
	ArrayList<Item> gridArray = new ArrayList<Item>();
	CustomGridViewAdapter customGridAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.swipe_activity);
		

		findViewById(R.id.header_tab).setOnClickListener(this);
		findViewById(R.id.back).setOnClickListener(this);
		 Bitmap homeIcon = BitmapFactory.decodeResource(this.getResources(),R.drawable.antal_platser);
		Bitmap userIcon = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.uteservering_10);
		Bitmap homeIcon1 = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.handikappvanligt_1);
		Bitmap userIcon1 = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.utmarkt_dryckesutbud_11);
		Bitmap homeIcon2 = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.bar_2);
		Bitmap userIcon2 = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.hoga_eko_ambitioner_12);
		Bitmap homeIcon3 = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.enklare_mat_3);
		Bitmap userIcon3 = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.hjartekrog_81);
		Bitmap homeIcon4 = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.brunch_4);
		Bitmap userIcon4 = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.ny_82);
		Bitmap homeIcon5 = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.parkering_5);
		Bitmap userIcon5 = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.medlem_visita_14);
		Bitmap homeIcon6 = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.vegetariskt_6);
		Bitmap userIcon6 = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.overnattningsmojligheter_7);
		Bitmap homeIcon7 = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.american_express_15);
		Bitmap userIcon7 = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.festvaning_8);
		Bitmap userIcon8 = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.chambre_separee_9);
		Bitmap wifi = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.gratis_wifi_72);
		Bitmap krav = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.krav);
		Bitmap guide80 = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.whiteguideandroid_56);
		Bitmap guide79 = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.whiteguideandroid_57);
		Bitmap guide40 = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.whiteguideandroid_58);
		Bitmap guide16 = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.whiteguideandroid_59);
		 gridArray.add(new Item(homeIcon, "Antal platser"));
		gridArray.add(new Item(userIcon, "Uteservering"));
		gridArray.add(new Item(homeIcon1, "Handikappv�nligt"));
		gridArray.add(new Item(userIcon1,
				"Utm�rkt och/eller speciellt dryckesutbud"));
		gridArray.add(new Item(homeIcon2, "Bar"));
		gridArray.add(new Item(userIcon2, "H�ga ekologiska ambitioner"));
		gridArray.add(new Item(homeIcon3,
				"Enklare mat i bar eller special- avdelning"));
		gridArray.add(new Item(userIcon3, "Hj�rtekrog"));
		gridArray.add(new Item(homeIcon4, "Brunch"));
		gridArray.add(new Item(userIcon4, "Ny restaurang i White Guide"));
		gridArray.add(new Item(homeIcon5, "Parkering"));
		gridArray.add(new Item(userIcon5, "Medlem i Visita-Svensk bes�ksn�ring"));
		gridArray.add(new Item(homeIcon6, "Vegetariska r�tter"));
		gridArray.add(new Item(userIcon6, "�vernattnings-m�jligheter"));
		gridArray.add(new Item(homeIcon7, "Ansluten till American Express"));
		gridArray.add(new Item(userIcon7, "Festv�ning"));
		gridArray.add(new Item(userIcon8, "Chambre separ�e"));
		
		gridArray.add(new Item(wifi, "Gratis WiFi"));
		gridArray.add(new Item(krav, "krav"));
		gridArray.add(new Item(guide79, "Utm�rkt utbud av bakverk "));
		gridArray.add(new Item(guide80, "Utm�rkt kaffeutbud"));
		gridArray.add(new Item(guide40, "Vintercaf�"));
		gridArray.add(new Item(guide16, "Sommarcaf�"));
		

		gridView = (GridView) findViewById(R.id.gridView1);
		customGridAdapter = new CustomGridViewAdapter(this, R.layout.row_grid,
				gridArray);
		gridView.setAdapter(customGridAdapter);
		
	}

	@Override
	public void onClick(View v) {
		//overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
		finish();

	}

	
}
