package com.se.whiteguide.screens;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.live.se.whiteguide.R;
import com.se.whiteguide.dataengine.DataEngine;
import com.se.whiteguide.dataengine.Restaurant;

/**
 * Displays map along with specific restaurant latitude and longitude.
 * 
 * @author Conevo
 */
public class DetailedMap extends AnimActivity {
	GoogleMap googleMap;
	Restaurant restaurant;
	double lat = 0.0, lan = 0.0;
	AdView adView;
	Bitmap marker_bitmap, cof_bitmap,newres_bitmap;
	int height, width;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.karta1);
		adView = (AdView) findViewById(R.id.adView);
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

		// Height & width of the Screen.
		height = displaymetrics.heightPixels;
		width = displaymetrics.widthPixels;
		restaurant = (Restaurant) getIntent().getSerializableExtra("RES");
		lat = restaurant.getLat();
		lan = restaurant.getLon();
		initilizeMap();
		findViewById(R.id.back).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
				//overridePendingTransition(R.anim.slide_out_left,R.anim.slide_in_right);
			}
		});
	}

	/**
	 * function to load map If map is not created it will create it for you
	 * */
	@SuppressLint("NewApi")
	private void initilizeMap() {
		if (googleMap == null) {
			googleMap = ((MapFragment) getFragmentManager().findFragmentById(
					R.id.map)).getMap();
			AdRequest adRequest = new AdRequest.Builder().build();
			// Start loading the ad in the background.
			if (DataEngine.showAds) {
				adView.loadAd(adRequest);
			} else {
				adView.setVisibility(View.GONE);
			}
		}
		// check if map is created successfully or not
		if (googleMap == null) {
			Toast.makeText(this.getParent(), "Sorry! unable to create maps",
					Toast.LENGTH_SHORT).show();
		} else {
			googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
			// Showing / hiding your current location

			googleMap.setMyLocationEnabled(true);

			// Enable / Disable zooming controls
			googleMap.getUiSettings().setZoomControlsEnabled(false);

			// Enable / Disable my location button
			googleMap.getUiSettings().setMyLocationButtonEnabled(true);

			// Enable / Disable Compass icon
			googleMap.getUiSettings().setCompassEnabled(true);

			// Enable / Disable Rotate gesture
			googleMap.getUiSettings().setRotateGesturesEnabled(true);

			// Enable / Disable zooming functionality
			googleMap.getUiSettings().setZoomGesturesEnabled(true);
			// Adding a marker
			MarkerOptions marker;
			Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
					R.drawable.marker);
			Bitmap coffee_bitmap = BitmapFactory.decodeResource(getResources(),
					R.drawable.marker2);
			Bitmap newr_bitmap = BitmapFactory.decodeResource(getResources(),
					R.drawable.newres_marker);
			marker_bitmap = Bitmap.createScaledBitmap(bitmap, 57, 70, true);
			cof_bitmap = Bitmap.createScaledBitmap(coffee_bitmap, 57, 70, true);
			newres_bitmap = Bitmap.createScaledBitmap(newr_bitmap, 57, 70, true);
			// Adding a marker
			if (restaurant.getGuideId().matches("58")) {
				marker = new MarkerOptions()
						.position(new LatLng(lat, lan))
						// .snippet(restaurant.getCity())
						.icon(BitmapDescriptorFactory.fromBitmap(marker_bitmap))
						.title(restaurant.getTitle().replace("#", ""));
				googleMap.addMarker(marker);
			}

			else if (restaurant.getGuideId().matches("60")) {
				marker = new MarkerOptions().position(new LatLng(lat, lan))
						// .snippet(restaurant.getCity())
						.icon(BitmapDescriptorFactory.fromBitmap(cof_bitmap))
						.title(restaurant.getTitle().replace("#", ""));
				googleMap.addMarker(marker);
			} else if (restaurant.getGuideId().matches("90")) {
				marker = new MarkerOptions()
						.position(new LatLng(lat, lan))
						// .snippet(restaurant.getCity())
						.icon(BitmapDescriptorFactory.fromBitmap(newres_bitmap))
						.title(restaurant.getTitle().replace("#", ""));
				googleMap.addMarker(marker);
			}

			else if (restaurant.getGuideId().matches("59")) {
				marker = new MarkerOptions()
						.position(new LatLng(lat, lan))
						// .snippet(restaurant.getCity())
						.icon(BitmapDescriptorFactory
								.fromResource(R.drawable.marker_bar))
						.title(restaurant.getTitle().replace("#", ""));
				googleMap.addMarker(marker);
			}
			LatLng latLng = new LatLng(lat, lan);
			CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
					latLng, 15);
			googleMap.animateCamera(cameraUpdate);

		}

	}

	public void calling_map(View v) {
		finish();
		//overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
	}

}