package com.se.whiteguide.screens;

import java.io.InputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.live.se.whiteguide.R;
import com.se.whiteguide.AppController;
import com.se.whiteguide.dataengine.DataEngine;
import com.se.whiteguide.log.MyDebug;
import com.se.whiteguide.parsers.NewsListParser;
import com.se.whiteguide.pulltorefresh.PullAndLoadListView;
import com.se.whiteguide.pulltorefresh.PullAndLoadListView.OnLoadMoreListener;
import com.se.whiteguide.pulltorefresh.PullToRefreshListView.OnRefreshListener;
import com.se.whiteguide.tabgroup.Home;
import com.se.whiteguide.tabgroup.NewsFeedGroup;

/**
 * Activity shows you the all the "Krognytt" list of items with having the
 * functionalities of pull to refresh and loading more items at the bottom of
 * listview .
 * 
 */

@SuppressWarnings("serial")
public class NewsFeedView extends AnimActivity implements Serializable {
	// private ListView newsfeed_list;
	PullAndLoadListView newsfeed_list;
	NewsListParser parser_obj;
	ProgressDialog progress;
	// String url;
	NewsAsyn asyn_obj;
	Vector<String> urlList;
	public static HashMap<String, Bitmap> finalBitmaps;
	URL url;
	HttpURLConnection connection;
	InputStream input;
	Bitmap srcBmp;
	BitmapFactory.Options o;
	Handler handler;
	NewsAdapter adapter;
	boolean flag_loading = false;
	int count = 0;
	public static String DATE_FORMAT = "dd MMM yyyy";
	private static String DATAOBJ = "DATAOBJ";
	int pages;
	int pos = 0;
	public static String TAG="NewsFeedView";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.newsfeed_view);
		progress = new ProgressDialog(this.getParent());
		progress.setCancelable(false);
		progress.setMessage(getString(R.string.loading));
		handler = new Handler();
		finalBitmaps = new HashMap<String, Bitmap>();
		urlList = new Vector<String>();
		parser_obj = new NewsListParser(NewsFeedView.this);
		newsfeed_list = (PullAndLoadListView) findViewById(R.id.news_feedlist);
		// url="file:///android_asset/";
		asyn_obj = new NewsAsyn();
		asyn_obj.execute();
		newsfeed_list.setOnRefreshListener(new OnRefreshListener() {
			@Override
			public void onRefresh() {
				count=0;
				new PullToRefreshDataTask().execute();
			}
		});
		newsfeed_list.setOnLoadMoreListener(new OnLoadMoreListener() {
			@Override
			public void onLoadMore() {
				
				new LoadMoreDataTask().execute();
				count++;
			}
		});
		newsfeed_list.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				int c = position - 1;
				if (c <= parser_obj.newslist_data.size()) {
					Intent intent = new Intent(NewsFeedView.this,
							DetailedNewsList.class);
					intent.putExtra(DATAOBJ, parser_obj.full_data.get(pos)
							.getNews_data().get(position - 1));
					intent.putExtra("position", position);
					NewsFeedGroup parentactivity = (NewsFeedGroup) getParent();
					parentactivity.startActivity(intent
							.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
									| Intent.FLAG_ACTIVITY_NEW_TASK));
				}

			}
		});
	}

	/**
	 * Represents the process of the loading when the user pulls down the
	 * listview for refreshing the page
	 */
	private class PullToRefreshDataTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {

			if (isCancelled()) {
				return null;
			}
			handler.post(new Runnable() {
				@Override
				public void run() {
					show_dialog();
				}
			});
			// Simulates a background task
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}
			parser_obj = new NewsListParser(NewsFeedView.this);
			String url = DataEngine.BASE_URL + DataEngine.NEWS_URL_LINK;
			try {
				parser_obj.getNewsList(url);
			} catch (Exception e) {
				MyDebug.log_info(TAG, "Parse Exception",e.getMessage());
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// We need notify the adapter that the data have been changed
			adapter.notifyDataSetChanged();
			handler.post(new Runnable() {
				@Override
				public void run() {
					dismiss_dialog();
				}
			});
			// Call onLoadMoreComplete when the LoadMore task, has finished
			newsfeed_list.onRefreshComplete();
			super.onPostExecute(result);
		}

		@Override
		protected void onCancelled() {
			// Notify the loading more operation has finished
			newsfeed_list.onLoadMoreComplete();
		}
	}

	private void show_dialog() {
		if (null != progress) {
			if (!progress.isShowing())
				progress.show();
		}
	}

	private void dismiss_dialog() {
		if (null != progress) {
			if (progress.isShowing())
				progress.dismiss();
		}
	}

	/**
	 * Represents loading more items to the existing listview
	 */
	private class LoadMoreDataTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {

			if (isCancelled()) {
				return null;
			}
			// Simulates a background task
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}

			if (count <= pages) {
				
				try {
					String page_url = DataEngine.BASE_URL
							+ DataEngine.NEWS_URL_PAGE + count;
					parser_obj.getNewsList(page_url);
				} catch (Exception e) {
					MyDebug.log_info(TAG, "Parse Exception",e.getMessage());
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// We need notify the adapter that the data have been changed
			if (count <= pages)
				adapter.notifyDataSetChanged();
			// Call onLoadMoreComplete when the LoadMore task, has finished
			newsfeed_list.onLoadMoreComplete();
			super.onPostExecute(result);
		}

		@Override
		protected void onCancelled() {
			// Notify the loading more operation has finished
			newsfeed_list.onLoadMoreComplete();
		}
	}

	/**
	 * Adapter class for the listview items.
	 */
	class NewsAdapter extends BaseAdapter {
		Context mtx;
		LayoutInflater inflate;
		ImageLoader imageLoader;
		AdRequest adRequest;

		public NewsAdapter(Context ctx) {
			this.mtx = ctx;
			inflate = LayoutInflater.from(ctx);
			imageLoader = AppController.getInstance().getImageLoader();
			adRequest = new AdRequest.Builder().build();

		}

		@Override
		public int getCount() {
			return parser_obj.newslist_data.size();
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@SuppressLint("InflateParams")
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				convertView = inflate.inflate(R.layout.news_adapter, null);
				holder = new ViewHolder();
				holder.text_lay = (LinearLayout) convertView
						.findViewById(R.id.description_listlay);
				holder.img_lay = (LinearLayout) convertView
						.findViewById(R.id.description_imagelistlay);
				holder.title_txt = (TextView) convertView
						.findViewById(R.id.title_txt);
				holder.adView = (AdView) convertView.findViewById(R.id.adView);
				// holder.title_txt.setTypeface(DataEngine.gettypeface(NewsFeedView.this));
				holder.description_Txt = (TextView) convertView
						.findViewById(R.id.text_desc);
				holder.publish_txt = (TextView) convertView
						.findViewById(R.id.update_date);
				// holder.description_Txt.setTypeface(DataEngine.gettypeface(NewsFeedView.this));
				holder.network_imageview = (NetworkImageView) convertView
						.findViewById(R.id.imgNetwork);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			int ads_pos = position + 1;
			if (ads_pos % 4 == 0) {

					holder.adView.setVisibility(View.VISIBLE);
					holder.text_lay.setVisibility(View.GONE);
					holder.img_lay.setVisibility(View.GONE);
					
					holder.adView.setAdListener(new AdListener() {
					});
					holder.adView.loadAd(adRequest);
					
			
			} else {
				holder.text_lay.setVisibility(View.VISIBLE);
				holder.img_lay.setVisibility(View.VISIBLE);
				holder.adView.setVisibility(View.GONE);
				holder.title_txt.setText(parser_obj.full_data.get(pos)
						.getNews_data().get(position).getTitle());
				String short_desc;
				if (parser_obj.full_data.get(pos).getNews_data().get(position)
						.getSmalldesc().length() > 150) {
					short_desc = parser_obj.full_data.get(pos).getNews_data()
							.get(position).getSmalldesc().substring(0, 149)
							+ "...";
					short_desc = short_desc.replaceAll("\\<.*?\\>", "")
							.replaceAll("H�R", "").replaceAll("Tillbaka", "")
							.replaceAll("Fler", "");

				} else {
					short_desc = parser_obj.full_data.get(pos).getNews_data()
							.get(position).getSmalldesc();
					short_desc = short_desc.replaceAll("\\<.*?\\>", "")
							.replaceAll("H�R", "").replaceAll("Tillbaka", "")
							.replaceAll("Fler", "");
				}
				holder.description_Txt.setText(Html.fromHtml(short_desc));
				holder.publish_txt.setText(getString(R.string.updated)
						+ " "
						+ convertDate(parser_obj.full_data.get(pos)
								.getNews_data().get(position).getDate(),
								DATE_FORMAT));
				holder.network_imageview.setDefaultImageResId(R.drawable.list_noimage);
				holder.network_imageview.setImageUrl(
						parser_obj.full_data.get(pos).getNews_data()
								.get(position).getImagelink(), imageLoader);
				

			}
			return convertView;

		}

		class ViewHolder {
			TextView title_txt, description_Txt, publish_txt;
			NetworkImageView network_imageview;
			LinearLayout text_lay, img_lay;
			private AdView adView;
		}
		
		

	}

	public String convertDate(String dateInMilliseconds, String dateFormat) {
		Locale lithuanian = new Locale("sv", "SWE");
		SimpleDateFormat formatter = new SimpleDateFormat(dateFormat,
				lithuanian);
		// Create a calendar object that will convert the date and time value in
		// milliseconds to date.
		Calendar calendar = Calendar.getInstance();
		if(dateInMilliseconds!=null)
		calendar.setTimeInMillis(Long.parseLong(dateInMilliseconds) * 1000);
		return formatter.format(calendar.getTime());
	}

	/**
	 * Represents the background process for getting the data and loading in
	 * listview.
	 */
	class NewsAsyn extends AsyncTask<Void, String, String> {

		@Override
		protected void onPreExecute() {
			if (null != progress) {
				if (!progress.isShowing())
					progress.show();
			}

		}

		@Override
		protected String doInBackground(Void... params) {
			String exception=null;
			try{
			String url = DataEngine.BASE_URL + DataEngine.NEWS_URL_LINK;
			parser_obj.getNewsList(url);
			}catch(Exception e){
				exception=e.getMessage();
			}
			return exception;
		}

		@Override
		protected void onPostExecute(String result) {
			if (null != progress) {
				if (progress.isShowing())
					progress.dismiss();
			}
			if (parser_obj.full_data.size() > 0) {
				adapter = new NewsAdapter(NewsFeedView.this);
				newsfeed_list.setAdapter(adapter);
				int totalrows = Integer.parseInt(parser_obj.full_data.get(pos)
						.getTotalrowss());
				pages = totalrows / 20;
			}
			super.onPostExecute(result);
		}

	}

	/**
	 * Displays slide menu items
	 */
	public void calling_view(View v) {
		Home.setDrawer();
	}

}
