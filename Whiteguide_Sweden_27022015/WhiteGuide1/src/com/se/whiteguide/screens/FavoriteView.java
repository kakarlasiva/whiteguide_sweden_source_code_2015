package com.se.whiteguide.screens;

import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.live.se.whiteguide.R;
import com.se.whiteguide.dataengine.DataEngine;
import com.se.whiteguide.dataengine.FavouritesHandler;
import com.se.whiteguide.dataengine.InitialInformation;
import com.se.whiteguide.dataengine.PaymentInformation;
import com.se.whiteguide.dataengine.Restaurant;
import com.se.whiteguide.helpers.BasicInfoBean;
import com.se.whiteguide.log.MyDebug;
import com.se.whiteguide.tabgroup.FavouritesGroup;
import com.se.whiteguide.tabgroup.Home;

/**
 * Displays restaruant/cafe which is favorite by user
 * 
 * @author conevo
 */
public class FavoriteView extends Activity {
	SwipeGestureListener gestureListener;
	ListView cartList;
	CartAdapter adapter;
	List<Restaurant> restaurants;
	FavouritesHandler treatHandler;
	InitialInformation infoObj;
	PaymentInformation payInfo;
	Vector<BasicInfoBean> payInfoList;
	Vector<BasicInfoBean> initialInforList;

	BasicInfoBean beanObj;

	AdView adView;
	public static String TAG = "FavoriteView";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.favorite);
		adView = (AdView) findViewById(R.id.adView);
		infoObj = new InitialInformation(getApplicationContext());
		payInfo = new PaymentInformation(getApplicationContext());
		gestureListener = new SwipeGestureListener(this.getParent());
		cartList = (ListView) findViewById(R.id.cart_list);
		treatHandler = new FavouritesHandler(this.getParent());
		try {
			restaurants = treatHandler.readFavorite();
		} catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}
		setListeners();
	}

	/**
	 * Listener for all on click event
	 */
	private void setListeners() {
		cartList.setOnTouchListener(gestureListener);
		gestureListener.setSwipeListner(new onItemSwipeListener() {

			@Override
			public void onItemSwiped(int position) {
				adapter.setDelVisibility(position, false);
			}
		});
		cartList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Home.closeDrawer();
				Restaurant temp = restaurants.get(arg2);
				try {
					for (BasicInfoBean dummy : payInfo.readData()) {

						/*
						 * if
						 * (dummy.getGuideId().equalsIgnoreCase(temp.getGuideId
						 * ()) &&
						 * dummy.getGuideYear().equalsIgnoreCase(temp.getGuideYear
						 * ())) {
						 */
						if (dummy.getGuideId().equalsIgnoreCase(
								temp.getGuideId())) {
							beanObj = dummy;
							break;
						}
					}

					if (null == beanObj) {

						for (BasicInfoBean dummy : infoObj.readData()) {

							/*
							 * if
							 * (dummy.getGuideId().equalsIgnoreCase(temp.getGuideId
							 * ()) &&
							 * dummy.getGuideYear().equalsIgnoreCase(temp.
							 * getGuideYear())) {
							 */
							if (dummy.getGuideId().equalsIgnoreCase(
									temp.getGuideId())) {
								beanObj = dummy;
								break;
							}
						}

					}
				} catch (Exception e) {
					MyDebug.log_info(TAG, "Parse Exception",
							e.getMessage());
				}

				if (null == beanObj) {
					beanObj = new BasicInfoBean();
					beanObj.setGuideId(temp.getGuideId());
					beanObj.setGuideYear(temp.getGuideYear());
					beanObj.setGuideisBought(false);
				}
				Intent nextActivity = new Intent(FavoriteView.this,
						DetailedRestaurantview.class);
				nextActivity.putExtra("RES", restaurants.get(arg2));
				nextActivity.putExtra("GUIDE", beanObj);
				nextActivity.putExtra("PUSH",false);

				FavouritesGroup parentActivity = (FavouritesGroup) getParent();
				parentActivity.startActivity(nextActivity
						.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
								| Intent.FLAG_ACTIVITY_NEW_TASK));

			}
		});

	}

	/**
	 * Displays slide menu items
	 */
	public void calling(View v) {
		Home.setDrawer();
	}

	/**
	 * when we swipe listview delete will be displayed. swipe gestture
	 * implementation
	 */
	public class SwipeGestureListener extends SimpleOnGestureListener implements
			OnTouchListener {
		Context context;
		GestureDetector gDetector;
		static final int SWIPE_MIN_DISTANCE = 120;
		static final int SWIPE_MAX_OFF_PATH = 250;
		static final int SWIPE_THRESHOLD_VELOCITY = 200;
		onItemSwipeListener swipeListener;

		public SwipeGestureListener() {
			super();
		}

		public SwipeGestureListener(Context context) {
			this(context, null);
		}

		public SwipeGestureListener(Context context, GestureDetector gDetector) {
			if (gDetector == null)
				gDetector = new GestureDetector(context, this);
			this.context = context;
			this.gDetector = gDetector;
		}

		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			final int position = cartList.pointToPosition(
					Math.round(e1.getX()), Math.round(e1.getY()));
			if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH) {
				if (Math.abs(e1.getX() - e2.getX()) > SWIPE_MAX_OFF_PATH
						|| Math.abs(velocityY) < SWIPE_THRESHOLD_VELOCITY) {
					return false;
				}
				if (e1.getY() - e2.getY() > SWIPE_MIN_DISTANCE) {

				} else if (e2.getY() - e1.getY() > SWIPE_MIN_DISTANCE) {

				}
			} else {
				if (Math.abs(velocityX) < SWIPE_THRESHOLD_VELOCITY) {
					return false;
				}
				if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE) {
					swipeListener.onItemSwiped(position);
				} else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE) {
					swipeListener.onItemSwiped(position);
				}
			}
			return super.onFling(e1, e2, velocityX, velocityY);
		}

		public void setSwipeListner(onItemSwipeListener swipe) {
			if (null != swipe) {
				swipeListener = swipe;
			}
		}

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			return gDetector.onTouchEvent(event);
		}

		public GestureDetector getDetector() {
			return gDetector;
		}

	}

	@Override
	protected void onResume() {
		beanObj = null;
		try {
			restaurants = treatHandler.readFavorite();
		} catch (Exception e1) {
			MyDebug.log_info(TAG, "Parse Exception", e1.getMessage());
		}
		adapter = new CartAdapter(this.getParent(), cartList);
		cartList.setAdapter(adapter);
		try {
			initialInforList = infoObj.readData();
			payInfoList = payInfo.readData();
		} catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}

		AdRequest adRequest = new AdRequest.Builder().build();
		if (DataEngine.showAds)
			adView.loadAd(adRequest);
		else
			adView.setVisibility(View.GONE);
		if (restaurants.size() > 0) {
			cartList.setVisibility(View.VISIBLE);
			findViewById(R.id.emptyCartTxt).setVisibility(View.GONE);
		} else {
			cartList.setVisibility(View.GONE);
			findViewById(R.id.emptyCartTxt).setVisibility(View.VISIBLE);
		}

		super.onResume();
	}

	public interface onItemSwipeListener {
		public void onItemSwiped(int position);
	}

	/**
	 * Adapter is used to display favorite item in list
	 */
	class CartAdapter extends BaseAdapter {
		Context context;
		int delItemPos = -1;
		boolean delAll = false;
		LayoutInflater inflater;
		ListView cartList;
		HashMap<LinearLayout, Button> deleteBtns;

		public CartAdapter(Context c, ListView cartList2) {
			this.context = c;
			inflater = LayoutInflater.from(c);
			cartList = cartList2;
			deleteBtns = new HashMap<LinearLayout, Button>();
		}

		@Override
		public int getCount() {
			return restaurants.size();
		}

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			return 0;
		}

		public boolean isNytestat() {
			if (beanObj.getGuideName().equalsIgnoreCase(
					context.getString(R.string.Nytestat_name))) {
				return true;
			} else {
				return false;
			}
		}

		@SuppressLint("ViewHolder")
		@Override
		public View getView(final int pos, View convertView, ViewGroup parent) {
			ViewHolder holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.favorite_item, parent,
					false);
			holder.cartLayout = (FrameLayout) convertView
					.findViewById(R.id.cartLayout);
			holder.Favimage = (ImageView) convertView
					.findViewById(R.id.cart_item_image);
			holder.cityName = (TextView) convertView
					.findViewById(R.id.fav_city_name);
			holder.totalPoints = (TextView) convertView
					.findViewById(R.id.total);
			holder.itemName = (TextView) convertView
					.findViewById(R.id.product_name);
			holder.delConfirmation = (LinearLayout) convertView
					.findViewById(R.id.del_confirmation);
			holder.delete = (Button) convertView.findViewById(R.id.delete);
			deleteBtns.put(holder.delConfirmation, holder.delete);

			holder.delete.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					removeItemFromList(pos);
					setDelVisibility(-1, delAll);
				}
			});
			holder.delConfirmation.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					setDelVisibility(pos, delAll);
				}
			});
			if (delAll) {
				holder.delConfirmation.setVisibility(View.VISIBLE);
				holder.Favimage.setVisibility(View.GONE);

			}
			if (pos == delItemPos) {
				holder.delete.setVisibility(View.VISIBLE);
				holder.Favimage.setVisibility(View.GONE);
				holder.totalPoints.setVisibility(View.GONE);
				holder.cartLayout.setVisibility(View.GONE);
			} else {
				holder.delete.setVisibility(View.GONE);
				holder.Favimage.setVisibility(View.VISIBLE);
				holder.cartLayout.setVisibility(View.VISIBLE);
			}
			boolean isbought = false;
			Restaurant dummy = restaurants.get(pos);
			for (BasicInfoBean temp : initialInforList) {
				/*
				 * if (temp.getGuideId().equalsIgnoreCase(dummy.getGuideId()) &&
				 * temp.getGuideYear().equalsIgnoreCase(dummy.getGuideYear())) {
				 */
				if (temp.getGuideId().equalsIgnoreCase(dummy.getGuideId())) {
					if (temp.isGuideisBought()) {
						isbought = true;
						break;
					}
				}
			}
			if (!isbought) {
				for (BasicInfoBean temp : payInfoList) {
					/*
					 * if
					 * (temp.getGuideId().equalsIgnoreCase(dummy.getGuideId())
					 * &&
					 * temp.getGuideYear().equalsIgnoreCase(dummy.getGuideYear
					 * ())) {
					 */
					if (temp.getGuideId().equalsIgnoreCase(dummy.getGuideId())) {
						if (temp.isGuideisBought()) {
							isbought = true;
							break;
						}
					}
				}
			}
			if (!isbought) {
				holder.Favimage.setImageResource(R.drawable.noclasification);
			} else {
				if (dummy.getRestaurantClass().contains("1")) {
					holder.Favimage.setImageResource(R.drawable.intklass);
				} else if (dummy.getRestaurantClass().contains("2")) {
					holder.Favimage.setImageResource(R.drawable.masterklass);
				} else if (dummy.getRestaurantClass().contains("3")) {
					holder.Favimage.setImageResource(R.drawable.myketgodklass);
				} else if (dummy.getRestaurantClass().contains("4")) {
					holder.Favimage.setImageResource(R.drawable.godklass);
				} else if (dummy.getRestaurantClass().contains("7")) {
					holder.Favimage.setImageResource(R.drawable.brastalle);
				}
				if (dummy.getScore() > 0
						&& !dummy.getRestaurantClass().contains("7")) {
					holder.totalPoints.setVisibility(View.VISIBLE);
					holder.totalPoints.setText("" + dummy.getScore());
				}
			}
			holder.itemName.setText(restaurants.get(pos).getTitle()
					.replace("#", ""));
			holder.cityName.setText(restaurants.get(pos).getCity());

			holder.Favimage.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					boolean isbought = false;
					String guidename = null, guideyear = null;
					Restaurant dummy = restaurants.get(pos);
					if (dummy.getGuideId().equalsIgnoreCase("58")) {
						guidename = "Restauranger";
						guideyear = dummy.getGuideYear();
					} else if (dummy.getGuideId().equalsIgnoreCase("60")) {
						guidename = "Caf�er";
						guideyear = dummy.getGuideYear();
					} else if (dummy.getGuideId().equalsIgnoreCase("59")) {
						guidename = "bar";
						guideyear = dummy.getGuideYear();
					}
					for (BasicInfoBean temp : initialInforList) {
						/*
						 * if
						 * (temp.getGuideId().equalsIgnoreCase(dummy.getGuideId
						 * ()) &&
						 * temp.getGuideYear().equalsIgnoreCase(dummy.getGuideYear
						 * ())) {
						 */
						if (temp.getGuideId().equalsIgnoreCase(
								dummy.getGuideId())) {
							if (temp.isGuideisBought()) {
								isbought = true;
								break;
							}
						}
					}
					if (!isbought) {
						for (BasicInfoBean temp : payInfoList) {
							/*
							 * if
							 * (temp.getGuideId().equalsIgnoreCase(dummy.getGuideId
							 * ()) &&
							 * temp.getGuideYear().equalsIgnoreCase(dummy.
							 * getGuideYear())) {
							 */
							if (temp.getGuideId().equalsIgnoreCase(
									dummy.getGuideId())) {
								if (temp.isGuideisBought()) {
									isbought = true;
									break;
								}
							}
						}
					}

					if (!isbought) {
						AlertDialog.Builder builder = new AlertDialog.Builder(
								context);
						builder.setMessage(getString(R.string.guide_buy1) + " "
								+ guidename + " " + guideyear + " "
								+ getString(R.string.guide_buy2));

						builder.setPositiveButton(getString(R.string.buy),
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
										Home.setTab(4);
									}
								});
						builder.setNegativeButton(getString(R.string.cancel),
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
										dialog.cancel();
									}
								});
						builder.show();
					}

					// close

				}
			});

			convertView.setTag(holder);

			return convertView;
		}

		/**
		 * when delete is clicked that particular restaurant/cafe will be
		 * removed my favorite list
		 */
		protected void removeItemFromList(int position) {
			// TOD O Auto-generated method stub
			// main code on after clicking yes
			try {
				treatHandler.deleteFavorite(restaurants.get(position).getId());
			} catch (Exception e) {
				MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
			}
			restaurants.remove(position);
			adapter.notifyDataSetChanged();
			adapter.notifyDataSetInvalidated();
			if (restaurants.size() > 0) {
				cartList.setVisibility(View.VISIBLE);
				findViewById(R.id.emptyCartTxt).setVisibility(View.GONE);
			} else {
				cartList.setVisibility(View.GONE);
				findViewById(R.id.emptyCartTxt).setVisibility(View.VISIBLE);
			}

		}

		public void setDelVisibility(int pos, boolean delete_all) {
			if (pos == delItemPos && delItemPos != -1) {
				delItemPos = -1;
				notifyDataSetChanged();
				return;
			}
			delItemPos = pos;
			delAll = delete_all;
			notifyDataSetChanged();
		}

		public class ViewHolder {
			ImageView Favimage;
			LinearLayout delConfirmation;
			Button delete;
			TextView itemName, cityName, totalPoints;
			FrameLayout cartLayout;
		}
	}

}
