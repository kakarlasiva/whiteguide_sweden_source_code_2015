package com.se.whiteguide.screens;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Vector;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.live.se.whiteguide.R;
import com.se.whiteguide.dataengine.Restaurant;
import com.se.whiteguide.tabgroup.Home;

/**
 *  Represents images of restaurant in gallery view
 */

public class GalleryPage extends AnimActivity {
	GridView gallery;
	EdgeEffectViewPager image;	
	ObjectAnimator forwardAnimator, backwardAnimator;

	// To update images in a new thread.
	UpdateImages updateImages;
	Handler handler;
	ArrayList<String> urlList;
	Vector<View> imageList;
	GridViewAdapter gridAdapter;
	ImagePagerAdapter viewPageadapter;
	int position;
	private Restaurant restaurant;
    ProgressBar frameProgress;
	TextView header_text;
	
	URL url;
	HttpURLConnection connection;
	InputStream input;
	Bitmap srcBmp;
	BitmapFactory.Options o;


	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.gallery_view);
		imageList = new Vector<View>();
		frameProgress = (ProgressBar) findViewById(R.id.frameProgress);
		frameProgress.setVisibility(View.VISIBLE);
		header_text = (TextView) findViewById(R.id.header_text);
		restaurant = (Restaurant) getIntent().getSerializableExtra("RES");
		try {
			header_text.setText(restaurant.getTitle().replace("#", ""));
		} catch (Exception e) {
			header_text.setText("WhiteGuide");
		}
		handler = new Handler();
		urlList = new ArrayList<String>();
		urlList = (ArrayList<String>) getIntent()
				.getSerializableExtra("IMAGES");
		position = getIntent().getIntExtra("POSITION", 0);
		updateImages = new UpdateImages();
		updateImages.start();
		gridAdapter = new GridViewAdapter(this);
		viewPageadapter = new ImagePagerAdapter(this);

		gallery = (GridView) findViewById(R.id.gallery1);
		image = (EdgeEffectViewPager) findViewById(R.id.full_image_view);
		image.setEdgeEffectColor(Color.parseColor("#71D3FF"));

		gallery.setAdapter(gridAdapter);
		image.setAdapter(viewPageadapter);

		findViewById(R.id.back).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
				finish();
			}
			
		});
		if (urlList.size() <= 4) {
			gallery.setVisibility(View.GONE);
			image.setVisibility(View.GONE);			
		} else {
			gallery.setVisibility(View.VISIBLE);
			image.setVisibility(View.GONE);			
		}
		gallery.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {				
				gallery.setVisibility(View.GONE);
				image.setVisibility(View.VISIBLE);
				image.setCurrentItem(position);
			}

		});
		
	}
/**
 * Displays slide menu with items
 */
	public void calling(View v) {
		Home.setDrawer();
	}
/**
 *  Displays image in gallery view 
 */
	public class GridViewAdapter extends BaseAdapter {

		public GridViewAdapter(GalleryPage galleryView) {
			// TODO Auto-generated constructor stub
		}

		public GridViewAdapter(OnItemClickListener onItemClickListener) {
			// TODO Auto-generated constructor stub
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return urlList.size();
		}

		@Override
		public Object getItem(int pos) {
			// TODO Auto-generated method stub
			return pos;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup arg2) {
			// TODO Auto-generated method stub
			ImageView imageView = new ImageView(getBaseContext());
			imageView.setLayoutParams(new GridView.LayoutParams(170, 170));
			imageView.setImageBitmap(DetailedRestaurantview.finalBitmaps
					.get(urlList.get(position)));
			imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
			imageView.setBackgroundColor(Color.WHITE);
			return imageView;
		}

	}
/**
 *  View pager adapter to swipe image in gallery
 */
	private class ImagePagerAdapter extends PagerAdapter {
		LayoutInflater inflater;

		public ImagePagerAdapter(Context cntext) {
			// TODO Auto-generated constructor stub
			inflater = LayoutInflater.from(cntext);
		}

		@Override
		public int getCount() {
			return urlList.size();
		}

		@Override
		public void notifyDataSetChanged() {
			// TODO Auto-generated method stub
			super.notifyDataSetChanged();
			for (int i = 0; i < imageList.size(); i++) {
				if (urlList.size() > 0 && urlList.size() > i) {
					if (null != DetailedRestaurantview.finalBitmaps.get(urlList
							.get(i))) {
						((ImageView) imageList.get(i).findViewById(
								R.id.galleryImage))
								.setImageBitmap(DetailedRestaurantview.finalBitmaps
										.get(urlList.get(i))); 
						(imageList.get(i).findViewById(R.id.progressBar1))
								.setVisibility(View.GONE);
					}
				}
			}
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == ((FrameLayout) object);
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			View layout = inflater.inflate(R.layout.image_gallery, container,false);
			ImageView imageView = (ImageView) layout
					.findViewById(R.id.galleryImage);
			ProgressBar progress = (ProgressBar) layout
					.findViewById(R.id.progressBar1);

			if (null != DetailedRestaurantview.finalBitmaps.get(urlList
					.get(position))) {
				imageView.setImageBitmap(DetailedRestaurantview.finalBitmaps
						.get(urlList.get(position)));
				progress.setVisibility(View.GONE);
			} else {
				progress.setVisibility(View.VISIBLE);
			}
			imageList.add(layout);
			((ViewPager) container).addView(layout);

			return layout;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			((ViewPager) container).removeView((FrameLayout) object);
		}
	}

	/***
	 * Runnable to download the images...update the adapter with each image
	 * download completion.
	 */
	class UpdateImages extends Thread {
		public int calculateInSampleSize(BitmapFactory.Options options,
				int reqWidth, int reqHeight) {
			// Raw height and width of image
			final int height = options.outHeight;
			final int width = options.outWidth;
			int inSampleSize = 1;

			Log.i("BITMAP", "height " + height + "width : " + width);
			if (height > reqHeight || width > reqWidth) {

				final int halfHeight = height / 2;
				final int halfWidth = width / 2;

				// Calculate the largest inSampleSize value that is a power of 2
				// and keeps both
				// height and width larger than the requested height and width.
				while ((halfHeight / inSampleSize) > reqHeight
						&& (halfWidth / inSampleSize) > reqWidth) {
					inSampleSize *= 2;
				}
			}
			Log.i("BITMAP", "Sample Size : " + inSampleSize);

			return inSampleSize;
		}
       @Override
		public void run() {
			Runnable runnable = new Runnable() {

				@Override
				public void run() {
					try {
						for (String treat : urlList) {
							boolean isThere = false;
							try {
								if (null != DetailedRestaurantview.finalBitmaps) {
									if (null != DetailedRestaurantview.finalBitmaps
											.get(treat)) {
										isThere = true;
									}
								}
								if (!isThere) {
									url = new URL(treat);
									connection = (HttpURLConnection) url
											.openConnection();
									connection.setDoInput(true);
									connection.connect();
									input = connection.getInputStream();
									o = new BitmapFactory.Options();
									o.inJustDecodeBounds = false;
									// Calculate inSampleSize
									o.inSampleSize = 2;
									srcBmp = BitmapFactory.decodeStream(input,
											null, o);
									DetailedRestaurantview.finalBitmaps.put(
											treat, srcBmp);
								}
							} catch (Exception e) {
								DetailedRestaurantview.finalBitmaps.put(treat,
										null);
							}
							handler.post(new Runnable() {

								@Override
								public void run() {
									try {
										gridAdapter.notifyDataSetChanged();
										viewPageadapter.notifyDataSetChanged();
									} catch (Exception e) {
										e.printStackTrace();
									}

								}
							});
						}
					} catch (Exception e) {
					    e.printStackTrace();
					}
					handler.post(new Runnable() {
						
						@Override
						public void run() {	
							
							
							if (urlList.size() <= 4) {
								gallery.setVisibility(View.GONE);
								image.setCurrentItem(position);
								image.setVisibility(View.VISIBLE);									
							} else {
								gallery.setVisibility(View.VISIBLE);
								image.setVisibility(View.GONE);
							}
							if(null != frameProgress)
								frameProgress.setVisibility(View.GONE);
							
						}
					});
					
				}
			};
			new Thread(runnable).start();
		};
	}

}
