package com.se.whiteguide.screens;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.live.se.whiteguide.R;
import com.se.whiteguide.dataengine.DataEngine;
import com.se.whiteguide.dataengine.InitialInformation;
import com.se.whiteguide.dataengine.PaymentInformation;
import com.se.whiteguide.dataengine.TransferToServer;
import com.se.whiteguide.helpers.BasicInfoBean;
import com.se.whiteguide.helpers.UserEmailFetcher;
import com.se.whiteguide.log.MyDebug;
import com.se.whiteguide.parsers.BasicDataInfo;
import com.se.whiteguide.tabgroup.Home;
import com.se.whiteguide.util.IabHelper;
import com.se.whiteguide.util.IabResult;
import com.se.whiteguide.util.Inventory;
import com.se.whiteguide.util.Purchase;

/**
 * Represents in app purchase of restaurant/cafe guide.
 * 
 * @author Conevo
 */
public class StoreActivity extends Activity {
	private Vector<BasicInfoBean> beanList;
	private BasicDataInfo dataInfo;
	InitialInformation infoObj;
	PaymentInformation payentObj;
	TransferToServer transerObj;
	// Debug tag, for logging
	static final String TAG = "Store-Page";
	Handler uiHandler;

	// Does the user have the premium upgrade?

	boolean mIsPremium = false;
	boolean click = false;
	boolean isthere = false;
	// SKUs for our two products: the premium upgrade (non-consumable) and gas
	// (consumable)
	static final String SKU_PREMIUM = "premium";
	static final String SKU_GAS = "58";
	static final String SKU_GUIDE = "guide";

	String id = "";
	String name = "", year = "", purchaseData = "";
	long timestamp = 0;
	boolean success = false;
	
	

	// (arbitrary) request code for the purchase flow
	static final int RC_REQUEST = 10001;

	// Graphics for the gas gauge
	static int[] TANK_RES_IDS = { R.drawable.arrowbutton,
			R.drawable.arrowbutton, R.drawable.arrowbutton,
			R.drawable.arrowbutton, R.drawable.arrowbutton };
	SharedPreferences settings;

	// The helper object
	IabHelper mHelper;

	ListView store_list;
	MyAdapter adapter;
	private Typeface typeFace;
	private AdView adView;

	@Override
	protected void onResume() {
		MyDebug.appendLog("currently it is in on resume");
		AdRequest adRequest = new AdRequest.Builder().build();
		if (DataEngine.showAds)
			adView.loadAd(adRequest);
		else
			adView.setVisibility(View.GONE);

		super.onResume();
	}

	/**
	 * Adapter is used to list the available guides
	 */
	class MyAdapter extends BaseAdapter {

		LayoutInflater inflater;

		public MyAdapter() {
			inflater = LayoutInflater.from(StoreActivity.this.getParent());
		}

		@Override
		public int getCount() {
			MyDebug.appendLog("Purchase successful List Size."
					+ beanList.size());
			return beanList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return arg0;
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			Holder holder;
			if (convertView == null) {
				holder = new Holder();
				convertView = inflater.inflate(R.layout.storeitem_lay, parent,
						false);
				holder.cartItem = (TextView) convertView
						.findViewById(R.id.guideName);
				holder.cartItem.setTypeface(typeFace);
				holder.store_image = (ImageView) convertView
						.findViewById(R.id.store_image);
				holder.buyItem = (Button) convertView
						.findViewById(R.id.buyGuideItem);
				holder.cartAvailability = (TextView) convertView
						.findViewById(R.id.guideAvailability);
				holder.cartPrice = (TextView) convertView
						.findViewById(R.id.guidePrice);
				convertView.setTag(holder);
			} else {
				holder = (Holder) convertView.getTag();
			}

			if (beanList.get(position).getGuideId().matches("58")) {
				holder.store_image.setImageResource(R.drawable.storeicon);
			} else if (beanList.get(position).getGuideId().matches("60")) {
				holder.store_image.setImageResource(R.drawable.cafe_storeicon);
			}

			holder.cartItem.setText(beanList.get(position)
					.getDisplayguidename());
			holder.cartPrice.setText("Pris: "
					+ beanList.get(position).getGuidePrice() + ".");
			holder.cartAvailability.setText("Aktiv: "
					+ beanList.get(position).getGuideActiveDates());

			String new_name = beanList.get(position).getGuideName();

			if (beanList.get(position).isGuideisBought()
					&& !(new_name.equalsIgnoreCase(StoreActivity.this
							.getString(R.string.Nytestat_name)))) {
				holder.buyItem.setBackgroundResource(R.drawable.brought_button);
				holder.buyItem.setTextColor(Color.BLACK);
				holder.buyItem.setText(getString(R.string.bought));
			}

			holder.buyItem.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						if (!beanList.get(position).isGuideisBought()
								&& !payentObj.isThisGuidebought(
										beanList.get(position).getGuideId(),
										beanList.get(position).getGuideYear())) {
							if (!click) {
								click = true;
								setWaitScreen(true);
								id = beanList.get(position).getGuideId();
								year = beanList.get(position).getGuideYear();
								name = beanList.get(position).getGuideName();
								MyDebug.appendLog("id " + id + "year " + year
										+ "name " + name);
								/*
								 * mHelper.launchPurchaseFlow(StoreActivity.this
								 * .getParent(),
								 * beanList.get(position).getGuideId(), RC_REQUEST,
								 * mPurchaseFinishedListener);
								 */
								if (success) {
									mHelper.launchPurchaseFlow(StoreActivity.this
											.getParent(), beanList.get(position)
											.getAndroid_inapp_d(), RC_REQUEST,
											mPurchaseFinishedListener);
								} else {
									mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
										public void onIabSetupFinished(
												IabResult result) {
											MyDebug.appendLog("Setup finished.");
											if (!result.isSuccess()) {
												// Oh noes, there was a problem.
												complain("Problem setting up in-app billing: "
														+ result);
												MyDebug.appendLog("Problem setting up in-app billing: "
														+ result);
												click = false;
												return;
											}

											// Hooray, IAB is fully set up. Now,
											// let's get an inventory of
											// stuff we own.
											Log.d(TAG,
													"Setup successful. Querying inventory.");
											MyDebug.appendLog("Setup successful. Querying inventory.");
											success = true;
										}
									});
								}
							}
						} else {
							alert(getString(R.string.already_purchased));
						}
					} catch (Exception e) {
						MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
					}
				}
			});
			return convertView;
		}

		class Holder {
			TextView cartItem, cartYear, cartPrice, cartAvailability;
			Button buyItem;
			ImageView store_image;
		}

	}

	/**
	 * Postdata to palystore
	 */
	public void postData() {
		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(DataEngine.BASE_URL
				+ DataEngine.APP_USERS);

		try {
			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("content-type",
					"application/json"));
			nameValuePairs.add(new BasicNameValuePair("email", UserEmailFetcher
					.getEmail(StoreActivity.this)));
			nameValuePairs.add(new BasicNameValuePair("deviceId",
					UserEmailFetcher.getDeviceId(StoreActivity.this)));
			nameValuePairs.add(new BasicNameValuePair("guideId", id));
			nameValuePairs.add(new BasicNameValuePair("year", year));
			nameValuePairs.add(new BasicNameValuePair("timeStamp", String
					.valueOf(timestamp)));
			nameValuePairs.add(new BasicNameValuePair("paymentToken",
					purchaseData));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			MyDebug.appendLog("post purchase data: " + purchaseData);
			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);
			try {
				String finalReponse = EntityUtils
						.toString(response.getEntity());
				MyDebug.appendLog("Final response :" + finalReponse);
				if (finalReponse.contains("saved")) {
					uiHandler.post(set_adapnotify);
				}

			} catch (Exception e) {
				MyDebug.appendLog("Final error in saving data to server: "
						+ e.getMessage());
			}

		} catch (ClientProtocolException e) {
			MyDebug.appendLog("Final error in saving data to server: "
					+ e.getMessage());
		} catch (IOException e) {
			MyDebug.appendLog("Final error in saving data to server: "
					+ e.getMessage());
		} catch (Exception e) {
			MyDebug.appendLog("Final error in saving data to server: "
					+ e.getMessage());
		}

	}

	/**
	 * Display slide menu with items
	 */
	public void calling(View v) {
		Home.setDrawer();
	}

	Runnable set_adapnotify = new Runnable() {

		@Override
		public void run() {
			set_notify();
		}
	};

	public void set_notify() {
		transerObj.deleteTreat(id, year);
		beanList.clear();
		try {
			for (BasicInfoBean temp : infoObj.readData()) {
				if (!(temp.getGuideName().equalsIgnoreCase(StoreActivity.this
						.getString(R.string.Nytestat_name))))
					beanList.add(temp);
			}
			adapter.notifyDataSetChanged();
		} catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.store);
		adView = (AdView) findViewById(R.id.adView);
		infoObj = new InitialInformation(getApplicationContext());
		payentObj = new PaymentInformation(getApplicationContext());
		transerObj = new TransferToServer(getApplicationContext());
		settings = getSharedPreferences(DataEngine.SETTINGS_NAME, MODE_PRIVATE);
		beanList = new Vector<BasicInfoBean>();
		uiHandler = new Handler();
		typeFace = Typeface.createFromAsset(getAssets(), "HelveticaNeue.ttf");
		TextView tv = (TextView) findViewById(R.id.wview);

		tv.setText(R.string.storeactivity_text);

		store_list = (ListView) findViewById(R.id.storeList);
		new Background().execute();

		// Create the helper, passing it our context and the public key to
		// verify signatures with
		MyDebug.appendLog("Creating IAB helper.");
		mHelper = new IabHelper(this.getParent(),
				DataEngine.base64EncodedPublicKey);

		// enable debug logging (for a production application, you should set
		// this.getParent() to false).
		mHelper.enableDebugLogging(true);

		// Start setup. this.getParent() is asynchronous and the specified
		// listener
		// will be called once setup completes.
		Log.d(TAG, "Starting setup.");
		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
			public void onIabSetupFinished(IabResult result) {
				MyDebug.appendLog("Setup finished.");
				if (!result.isSuccess()) {
					// Oh noes, there was a problem.
					complain("Problem setting up in-app billing: " + result);
					MyDebug.appendLog("Problem setting up in-app billing: "
							+ result);
					return;
				}

				// Hooray, IAB is fully set up. Now, let's get an inventory of
				// stuff we own.
				Log.d(TAG, "Setup successful. Querying inventory.");
				MyDebug.appendLog("Setup successful. Querying inventory.");
				success = true;
			}
		});
	}

	/**
	 * Background task to get info about user
	 */
	class Background extends AsyncTask<Void, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(Void... params) {
			String exception = null;
			try {
				Vector<BasicInfoBean> infoObjData = infoObj.readData();
				if (infoObjData.size() > 0) {
					for (BasicInfoBean temp : infoObjData) {
						if (!(temp.getGuideName()
								.equalsIgnoreCase(StoreActivity.this
										.getString(R.string.Nytestat_name))))
							beanList.add(temp);
					}
				} else {
					dataInfo = new BasicDataInfo(getApplicationContext());

					dataInfo.doParsing();
					for (BasicInfoBean temp : infoObjData) {
						if (!temp.isGuideisBought()) {
							if (!(temp.getGuideName()
									.equalsIgnoreCase(StoreActivity.this
											.getString(R.string.Nytestat_name))))
								beanList.add(temp);
						}
					}

				}
			} catch (Exception e) {
				exception = e.getMessage();
			}

			return exception;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			setWaitScreen(false);
			adapter = new MyAdapter();
			store_list.setAdapter(adapter);
		}
	}

	// User clicked the "Buy Gas" button
	public void onBuyGasButtonClicked(View arg0) {
		// launch the gas purchase UI flow.
		// We will be notified of completion via mPurchaseFinishedListener
		setWaitScreen(true);
		mHelper.launchPurchaseFlow(this.getParent(), SKU_GAS, RC_REQUEST,
				mPurchaseFinishedListener);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		MyDebug.log_info(TAG, "Activitycode ", "onActivityResult("
				+ requestCode + "," + resultCode + "," + data);
		// Pass on the activity result to the helper for handling
		try {
			if (resultCode == RESULT_OK) {
				purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
				MyDebug.appendLog("purchase data  " + purchaseData);
			}
		} catch (Exception e) {
			e.printStackTrace();
			MyDebug.appendLog("error in activity code" + e.getMessage());
		}
		if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
			super.onActivityResult(requestCode, resultCode, data);
		} else {
			MyDebug.log_info(TAG, "error",
					"onActivityResult handled by IABUtil.");
		}
	}

	Purchase fPurchase;
	// Callback for when a purchase is finished
	IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
		public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
			MyDebug.appendLog("Purchase finished:  " + result + ", purchase:  "
					+ purchase);
			click = false;
			fPurchase = purchase;

			if (result.isFailure()) {
				// Oh noes!
				complain(getString(R.string.error_purchase));
				// MyDebug.appendLog("fail:" + result.isFailure());
				// MyDebug.appendLog("infoo" + result.getResponse()
				// + result.getMessage());
				// if (result.getResponse() == 7) {
				//
				// for (BasicInfoBean temp : transerObj.readData()) {
				// if (temp.getGuideId().equalsIgnoreCase(id)) {
				//
				// try {
				// Purchase temp_purchase = temp.getPurchaseInfo();
				// //mHelper.consumeAsync(temp_purchase,
				// mConsumeFinishedListener);
				// id = temp.getGuideId();
				// year = temp.getGuideYear();
				// name = temp.getGuideName();
				// purchaseData = temp_purchase.getOriginalJson();
				// timestamp = temp_purchase.getPurchaseTime();
				// Thread thread = new Thread(new Runnable() {
				//
				// @Override
				// public void run() {
				// postData();
				// }
				// });
				// thread.start();
				// } catch (Exception e) {
				// e.printStackTrace();
				// }
				//
				// break;
				// }
				// }
				// }
				// setWaitScreen(false);
				return;
			} else {
				Editor editor = settings.edit();

				if (settings.getString(DataEngine.SETTINGS_NYTEST, "NOTEXIST")
						.equalsIgnoreCase("NOTEXIST")){
					editor.putString(DataEngine.SETTINGS_NYTEST,"true");
				}
				
				editor.commit();
				MyDebug.appendLog("Purchase successful. Provisioning." + id);

				BasicInfoBean dummy = null, Updateinfoobj = null;
				for (BasicInfoBean temp : beanList) {
					if (temp.getGuideId().equalsIgnoreCase(id)) {
						dummy = temp;
						dummy.setGuideisBought(true);
						MyDebug.appendLog("id true? " + dummy.isGuideisBought());
						break;
					}
				}
				if (null != dummy)
					try {
						infoObj.updateTreat(id, dummy);
					} catch (Exception e) {
						MyDebug.log_info(TAG, "Parse Exception",
								e.getMessage());
					}

				Boolean nytestatisThere = false;
				for (BasicInfoBean temp : beanList) {
					if (temp.getGuideId().equalsIgnoreCase("90")) {
						nytestatisThere = true;
					}
				}
				try {
					if (!nytestatisThere && infoObj.isNytestat_shown()) {
						Updateinfoobj = new BasicInfoBean();
						Updateinfoobj.setGuideId(StoreActivity.this
								.getString(R.string.Nytestat_id));
						Updateinfoobj.setGuideName(StoreActivity.this
								.getString(R.string.Nytestat_name));
						Updateinfoobj.setGuidePrice(StoreActivity.this
								.getString(R.string.Nytestat_price));
						Updateinfoobj.setGuideYear(StoreActivity.this
								.getString(R.string.Nytestat_year));
						Updateinfoobj.setGuideisBought(true);
						Updateinfoobj.setGuideActiveDates(StoreActivity.this
								.getString(R.string.Nytestat_active));
						Updateinfoobj.setDisplayguidename(StoreActivity.this
								.getString(R.string.Nytestat_disguidename));
						Updateinfoobj.setAndroid_inapp_d(StoreActivity.this
								.getString(R.string.inapp_subid));
						Vector<BasicInfoBean> tempBeanList = infoObj.readData();
						tempBeanList.add(Updateinfoobj);
						infoObj.writeAll(tempBeanList);

						Editor mapeditor = settings.edit();
						mapeditor.putString(DataEngine.SETTINGS_MAPRESUME, "SHOW");
						mapeditor.commit();
					}
				} catch (Exception e) {
					MyDebug.log_info(TAG, "Parse Exception",
							e.getMessage());
				}

				timestamp = purchase.getPurchaseTime();
				MyDebug.appendLog("id  " + id + "purchase time :" + timestamp);

				DataEngine.guidePurchased = true;

				dummy.setPurchaseInfo(purchase);
				dummy.setPaymentInfo(purchaseData);
				try {
					payentObj.writeTreat(dummy);
				} catch (Exception e) {
					MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
				}
				transerObj.writeTreat(dummy);
				DataEngine.showAds = false;
				Thread thread = new Thread(new Runnable() {

					@Override
					public void run() {
						postData();

					}
				});
				thread.start();
				alert(getString(R.string.purchase_success));

			}
			MyDebug.appendLog("Purchase successful.");
			// mHelper.consumeAsync(fPurchase, mConsumeFinishedListener);
		}
	};

	Runnable consume = new Runnable() {

		@Override
		public void run() {
			alert(getString(R.string.purchase_success));
		}
	};

	// Called when consumption is complete
	IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
		public void onConsumeFinished(Purchase purchase, IabResult result) {
			MyDebug.appendLog("Consumption finished. Purchase: " + purchase
					+ ", result: " + result);

			if (result.isSuccess()) {
			} else {
				complain("Error while consuming: " + result);
				MyDebug.appendLog("error while buying   " + result);
			}
			uiHandler.post(ad_run);
		}
	};

	Runnable ad_run = new Runnable() {

		@Override
		public void run() {
			try {
				if (null != adView) {
					adView.setVisibility(View.GONE);
				}
			} catch (Exception e) {
				e.printStackTrace();
				MyDebug.appendLog("error in consume:" + e.getMessage());
			}
		}
	};

	// We're being destroyed. It's important to dispose of the helper here!
	@Override
	public void onDestroy() {
		// very important:
		if (mHelper != null) {
			mHelper.dispose();
		}
		mHelper = null;
		super.onDestroy();
	}

	// updates UI to reflect model
	public void updateUi() {
	}

	// Enables or disables the "please wait" screen.
	void setWaitScreen(boolean set) {

	}

	void complain(String message) {
		MyDebug.log_info(TAG, "error", "**** TrivialDrive Error: " + message);
		alert(getString(R.string.error) + " " + message);
	}

	/**
	 * Alert is called when error occurs
	 */
	void alert(String message) {
		AlertDialog.Builder bld = new AlertDialog.Builder(this.getParent());
		bld.setMessage(message);
		bld.setNeutralButton("Okay", null);
		Log.d(TAG, "Showing alert dialog: " + message);
		bld.create().show();
	}

	void saveData() {
	}

	void loadData() {
	}

	// Listener that's called when we finish querying the items we own
	IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
		public void onQueryInventoryFinished(IabResult result,
				Inventory inventory) {
			MyDebug.appendLog("query failed " + result + "inventory"
					+ inventory);
			if (result.isFailure()) {
				complain("Failed to query inventory: " + result);
				return;
			}

			try {
				for (BasicInfoBean temp : infoObj.readData()) {
					mIsPremium = inventory.hasPurchase(temp.getGuideId());
					MyDebug.appendLog("Teh Guide id is " + temp.getGuideId());
					MyDebug.appendLog("Teh Guide bought" + mIsPremium);
					if (mIsPremium) {
						id = temp.getGuideId();
						purchaseData = inventory.getPurchase(temp.getGuideId())
								.getOriginalJson();
						year = temp.getGuideYear();

					}
				}
			} catch (Exception e) {
				MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
			}

		}
	};
}
