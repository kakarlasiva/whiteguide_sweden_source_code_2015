package com.se.whiteguide.screens;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.live.se.whiteguide.R;
import com.se.whiteguide.dataengine.Restaurant;
import com.se.whiteguide.helpers.BasicInfoBean;
import com.se.whiteguide.tabgroup.Home;

/**
 * Represents the view from the notification form server
 * @author Conevo 
 */


public class TempActivity extends Activity{
	String news_id=null;
	String isNyheter=null;
	Restaurant restaurant=null;
	String pushtype=null;
@Override
protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	Bundle extras=getIntent().getExtras();
	if(extras!=null){
		news_id=extras.getString("NEWSID");
		if(null !=extras.get("RES"))
		restaurant=(Restaurant) extras.get("RES");
		if(null !=extras.get("PUSHTYPE"))
		pushtype=extras.getString("PUSHTYPE");
	}
	if(news_id==null){
	Intent notificationIntent = new Intent(this,Home.class);
	// set intent so it does not start a new activity
	notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
			| Intent.FLAG_ACTIVITY_SINGLE_TOP);
	notificationIntent.putExtras(getIntent().getExtras());
	startActivity(notificationIntent);
	finish();
	}else{
		if(pushtype!=null && pushtype.equalsIgnoreCase("nyheter")){
		Intent newsNotificationIntent = new Intent(this,DetailedNewspush.class);
		newsNotificationIntent.putExtras(getIntent().getExtras());
		newsNotificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		newsNotificationIntent.putExtra("newsid",news_id);
		startActivity(newsNotificationIntent);
		finish();
		}else if(pushtype!=null && pushtype.equalsIgnoreCase("nytestat")){
			BasicInfoBean beanObj = new BasicInfoBean();
			beanObj.setGuideId(getApplicationContext().getString(
					R.string.Nytestat_id));
			beanObj.setGuideName(getApplicationContext().getString(
					R.string.Nytestat_name));
			beanObj.setGuidePrice(getApplicationContext()
					.getString(R.string.Nytestat_price));
			beanObj.setGuideYear(getApplicationContext().getString(
					R.string.Nytestat_year));
			beanObj.setGuideisBought(true);
			beanObj.setGuideActiveDates(getApplicationContext()
					.getString(R.string.Nytestat_active));
			beanObj.setDisplayguidename(getApplicationContext()
					.getString(R.string.Nytestat_disguidename));
			beanObj.setAndroid_inapp_d(getApplicationContext()
					.getString(R.string.inapp_subid));
			
			Intent newsNotificationIntent = new Intent(this,DetailedRestaurantPushView.class);
			newsNotificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_SINGLE_TOP);
			
			newsNotificationIntent.putExtras(getIntent().getExtras());
			newsNotificationIntent.putExtra("RES",restaurant);
			newsNotificationIntent.putExtra("GUIDE",beanObj);
			newsNotificationIntent.putExtra("PUSH",true);

			startActivity(newsNotificationIntent);
			finish();
		}
	}
}
}
