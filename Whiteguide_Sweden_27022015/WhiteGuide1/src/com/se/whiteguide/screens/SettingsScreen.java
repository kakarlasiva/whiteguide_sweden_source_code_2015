package com.se.whiteguide.screens;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.live.se.whiteguide.R;
import com.nostra13.socialsharing.twitter.extpack.winterwell.json.JSONObject;
import com.se.whiteguide.dataengine.DataEngine;
import com.se.whiteguide.dataengine.InitialInformation;
import com.se.whiteguide.helpers.BasicInfoBean;
import com.se.whiteguide.helpers.UserEmailFetcher;
import com.se.whiteguide.tabgroup.Home;

public class SettingsScreen extends Activity implements
		OnCheckedChangeListener, OnClickListener {
	private TextView nytest_txt, Nhyt_txt, done_txt;
	ToggleButton nytest_tog, Nhyt_tog;
	SharedPreferences settings;
	private static String YES = "true";
	private static String NO = "false";
	ProgressDialog progress;
	InitialInformation information;
	long timestamp;
	PushAsyn asyn_obj;
	boolean isshown = false;
	LinearLayout nytest_lay;
	boolean isnytestat, isnyheter;
	String text = "";
	String success="";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings_lay);
		progress = new ProgressDialog(this.getParent());
		progress.setMessage(getString(R.string.loading));
		settings = getSharedPreferences(DataEngine.SETTINGS_NAME, MODE_PRIVATE);
		information = new InitialInformation(getApplicationContext());
		nytest_lay = (LinearLayout) findViewById(R.id.newrespush_lay);
		nytest_txt = (TextView) findViewById(R.id.Nytest_txt);
		Nhyt_txt = (TextView) findViewById(R.id.nhyt_txt);
		done_txt = (TextView) findViewById(R.id.done);
		nytest_tog = (ToggleButton) findViewById(R.id.nytest_toggle);
		Nhyt_tog = (ToggleButton) findViewById(R.id.nhyt_toggle);
		Nhyt_tog.setOnCheckedChangeListener(this);
		done_txt.setOnClickListener(this);
		Nhyt_txt.setText(getString(R.string.nyheter_name));
		nytest_txt.setText(getString(R.string.Nytestat_name));
		nytest_tog.setOnCheckedChangeListener(this);

	}

	@Override
	protected void onResume() {
		for (BasicInfoBean beanTemp : information.readData()) {
			if (beanTemp.isGuideisBought()) {
				isshown = true;
				break;
			}
		}
		if (!isshown) {
			nytest_lay.setVisibility(View.GONE);
		} else {
			nytest_lay.setVisibility(View.VISIBLE);
		}
		if (settings.getString(DataEngine.SETTINGS_NHYTER, "false")
				.equalsIgnoreCase(YES)) {
			Nhyt_tog.setChecked(true);
			isnyheter = true;
		} else {
			Nhyt_tog.setChecked(false);
			isnyheter = false;
		}
		if (settings.getString(DataEngine.SETTINGS_NYTEST, "false")
				.equalsIgnoreCase(YES)) {
			nytest_tog.setChecked(true);
			isnytestat = true;
		} else {
			nytest_tog.setChecked(false);
			isnytestat = false;
		}
		super.onResume();
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		switch (buttonView.getId()) {
		case R.id.nhyt_toggle:
			if (isChecked) {
				isnyheter = true;
			} else {
				isnyheter = false;
			}
			break;
		case R.id.nytest_toggle:
			if (isChecked) {
				isnytestat = true;
			} else {
				isnytestat = false;
			}
			break;
		default:
			break;
		}
	}

	/**
	 * Displays slide menu items
	 */
	public void calling_view(View v) {
		Home.setDrawer();
	}

	private void set_value(String key, String value) {
		// Editor editor = settings.edit();
		// editor.putString(key, value);
		// editor.commit();

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.done:
			asyn_obj = new PushAsyn();
			asyn_obj.execute();
			break;

		default:
			break;
		}

	}

	class PushAsyn extends AsyncTask<Void, String, String> {
		@Override
		protected void onPreExecute() {

			if (progress != null) {
				if (!progress.isShowing()) {
					progress.show();
				}
			}
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(Void... params) {
			String exception = null;
			try {
				String reg_id = settings.getString(DataEngine.SETTINGS_GCM,null);
				postData(reg_id);
			} catch (Exception e) {
				exception = e.getMessage();
			}
			return exception;
		}

		@Override
		protected void onPostExecute(String result) {
			if (progress != null) {
				if (progress.isShowing()) {
					progress.dismiss();
				}
			}
			if (success.equalsIgnoreCase("saved")) {
				showAlertBox();
			}
			super.onPostExecute(result);
		}

	}

	public void postData(String regId) {
		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(DataEngine.BASE_URL
				+ DataEngine.GCM_URL);

		try {
			// Add your data
			timestamp = System.currentTimeMillis();
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("content-type",
					"application/json"));
			nameValuePairs.add(new BasicNameValuePair("email", UserEmailFetcher
					.getEmail(getApplicationContext())));
			nameValuePairs.add(new BasicNameValuePair("deviceId",
					UserEmailFetcher.getDeviceId(getApplicationContext())));
			nameValuePairs.add(new BasicNameValuePair("timeStamp", String
					.valueOf(timestamp)));
			nameValuePairs.add(new BasicNameValuePair("gcmId", regId.trim()));
			nameValuePairs.add(new BasicNameValuePair("deviceType", "Android"));
			nameValuePairs.add(new BasicNameValuePair("nyheter", Integer
					.toString(isnyheter ? 1 : 0)));
			nameValuePairs.add(new BasicNameValuePair("nytestat", Integer
					.toString(isnytestat ? 1 : 0)));
			//Log.e("Settings", "-------------------" + Integer.toString(isnyheter ? 1 : 0)+"-------"+Integer.toString(isnytestat ? 1 : 0));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			// Execute HTTP Post Request
			httpclient.execute(httppost);

			HttpResponse response = httpclient.execute(httppost);
			Log.e("myapp", "response " + response.getEntity());

			try {
				text = GetText(response.getEntity().getContent());
				JSONObject obj = new JSONObject(text);
				success = obj.getString("success");
				//Log.e("Settings", "-------------------" + success);

			} catch (Exception ex) {
			}

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
		} catch (IOException e) {
			// TODO Auto-generated catch block
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	protected void showAlertBox() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this.getParent());
		builder.setMessage(getString(R.string.push_success));
		builder.setPositiveButton(getString(R.string.ok),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Editor editor = settings.edit();
						editor.putString(DataEngine.SETTINGS_NHYTER,
								isnyheter ? "true" : "false");
						editor.putString(DataEngine.SETTINGS_NYTEST,
								isnytestat ? "true" : "false");
						Log.e("ERROR", "HELLOR>>>>>>>>>>>>>>>>>>>>>>>>>>"
								+ Boolean.toString((isnytestat)));
						editor.commit();
					}
				});

		builder.show();

	}

	public static String GetText(InputStream in) {
		String text = "";
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		StringBuilder sb = new StringBuilder();
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			text = sb.toString();
		} catch (Exception ex) {

		} finally {
			try {
				in.close();
			} catch (Exception ex) {
			}
		}
		return text;
	}

}
