package com.se.whiteguide.dataengine;

import android.content.Context;
import android.graphics.Typeface;

/**
 *  Represents all global values which can be used in other classes
 *  @author Conevo
 */
public class DataEngine {
	public static final String BASE_URL = "http://api.whiteguide.se/api/";
	// DEV SITE "http://whiteguide.solidpartner.com/api/  http://api.whiteguide.se/api/";
	public static final String ALL_RESTAURANTS_BASIC_INFO = "restaurants.json?tid=";
	public static final String NYTESTAT_INFO="getnytestatrestaurant.json";
	public static final String NEWS_URL_LINK="getnewsfeed.json";
	public static final String NEWS_URL_PAGE="getnewsfeed.json?page=";
	public static final String DETAILED_PULLURL="getnewsfeeddetail.json?nid=";
	public static final String APP_USERS = "appusers";
	public static final String FB_APPID = "530323050349767";
	public static final String GCM_URL = "gcmpushnotify";
	public static boolean showAds = true;
	public static String consumer_key = "gFUtrooktPrH1nkrvIzGQ";
	public static String secret_key = "g36tP2wnGyDvcCqtI0AF2svwk7tYMd3tCf5LZdKOOw";

	public static final String SETTINGS_NAME = "settings";
	public static final String SETTINGS_MAIL = "mail";
	public static final String SETTINGS_NYTEST="Nytest_not";
	public static final String SETTINGS_NHYTER="Nhyter_not";
	public static final String SETTINGS_MAPRESUME="mapresume";
	public static final String SETTINGS_GCM="reg_id";
    public static final String ERROR_LOG = "error";
    public static final String VERBOSE_LOG = "verbose";
    public static final String INFO_LOG = "info";
	public static final String DEBUG_LOG = "debug";

    // The log enabled/disabled status.
	public static final boolean ERROR_LOG_ENABLED = true;
    public static final boolean VERBOSE_LOG_ENABLED = true;
    public static final boolean INFO_LOG_ENABLED = true;
	public static final boolean DEBUG_LOG_ENABLED = true;
	
	public static String ERROR_MSG="error_issue";
	
	public static final String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgAbQ04jYkGW8RCGR0p7MFrNfAa3pxipfqCfv8T4Ik8kJOZ5VxbGi1ljZbKW0DAH7V3w9e7RR2stko5G/qF6RGOFL/W8LzXMaUS/HBol04Q4RIQ4OPWPE+gwpsvA/dez7J5YVZV8HfyVOOmDlTZBPemlY07ASyuYKLxp7KPFPp00n+ODeHeX5C93lGHpWPPiQPASw+JtYJYqquAzh7ibhcJk1gZmlIWyEhS6zg3COWTH2iwEXzuupxsRo8db5O7T9BmGExqifF4nX8mCRfx8Q0hJftAHKW3J/ukSodVZ64X4DejobAHFGfoKFuxDCLlwN3IfCIcC2F3oWG8VY51k3nwIDAQAB";
	  //solidpartner key
	//public static final String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtXVkqBZnShWLD+s4ivsQvIeUJCsFyecDoEeFsT2Wr0uq6/4Q6EpfeYSEhOtquwCK50KaKtPrI4y/qUMC9zgMlQolvFaJukrrqZktC5HnylPNmdheZWQW2fwqE8vVj153usiTi8henPOoDMuajRoRNC6bIjaj+CIOEkMkDRuq8r6JfHdp8f3NHJVVoQ38RgDWZZlxkqDd2TRJ+taGPkWK61boNDAvOp8SXhJ3fsq/uqWY5TuYK6mVQXiudTf0Ymapnwz+c5jzX3G96RdOHPHOWI0YQH5byHa1/dArFVWEvNYhrQSHbE93DifMMJPnVnMEoIMoGTvnhFFQ24YgvnnU7wIDAQAB";

	public static Typeface gettypeface(Context mtx){
		Typeface typeFace = Typeface.createFromAsset(mtx.getAssets(), "HelveticaNeue.ttf");
		return typeFace;
	}
	
	public static boolean guidePurchased=false;
}
