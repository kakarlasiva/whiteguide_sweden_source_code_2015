package com.se.whiteguide.dataengine;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.util.Vector;

import android.content.Context;

import com.se.whiteguide.helpers.BasicInfoBean;
import com.se.whiteguide.log.MyDebug;
/**
 * File which contains basic payment information
 * @author Conevo
 */
public class PaymentInformation {
	Vector<BasicInfoBean> beanList;
	Context cntxt;
	private String fileName = "Payments.txt";
	public String requestFor;
	private static String TAG="PaymentInformation";

	public PaymentInformation(Context contxt) {
		this.cntxt = contxt;
	}
	
	public void clearAll() {
		beanList = new Vector<BasicInfoBean>();
	try {
		FileOutputStream fos = cntxt.openFileOutput(fileName,
				Context.MODE_PRIVATE);
		ObjectOutputStream os = new ObjectOutputStream(fos);
		os.writeObject(beanList);
		os.close();
	} catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
	}
}

	public boolean isAtleastOneguideBought(){
		boolean isbought = false;
		beanList = readData();
		for (BasicInfoBean temp : beanList) {
			if (temp.isGuideisBought())
				return true;
		}
		return isbought;
	}

	public void writeTreat(BasicInfoBean treat) {
		beanList = readData();
		beanList.add(treat);
		try {
			FileOutputStream fos = cntxt.openFileOutput(fileName,
					Context.MODE_PRIVATE);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(beanList);
			os.close();
		} catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}
	}

	public boolean isThisGuidebought(String guideId, String guideYear){
		boolean isbought = false;
		beanList = readData();
		for (BasicInfoBean temp : beanList) {
			if (temp.getGuideId().equalsIgnoreCase(guideId)) {
				isbought = true;
				break;
			}
		}

		return isbought;
	}

	@SuppressWarnings("unchecked")
	public Vector<BasicInfoBean> readData() {
		try {
			FileInputStream fis = cntxt.openFileInput(fileName);
			ObjectInputStream is = new ObjectInputStream(fis);
			beanList = new Vector<BasicInfoBean>();
			beanList = (Vector<BasicInfoBean>) is.readObject();
			is.close();
		} catch (FileNotFoundException e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		} catch (StreamCorruptedException e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		} catch (IOException e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		} catch (ClassNotFoundException e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		} catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}

		if (null == beanList) {
			beanList = new Vector<BasicInfoBean>();
		}

		return beanList;
	}
}
