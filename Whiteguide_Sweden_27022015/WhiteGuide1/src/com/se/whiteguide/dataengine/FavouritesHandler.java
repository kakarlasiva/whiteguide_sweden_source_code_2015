package com.se.whiteguide.dataengine;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Vector;

import android.content.Context;

import com.se.whiteguide.log.MyDebug;
/**
 * saves all restaurant in a file and update,delete restuarant which is added as favorite.
 * @author Conevo
 */
public class FavouritesHandler {
	FavouritesHandler treatHandler;
	Vector<Restaurant> restaurantList;
	Context cntxt;
	private String fileName = "Favourites.txt";
	private static String TAG="FavouritesHandler";

	public FavouritesHandler(Context context) {
		cntxt = context;
	}
/**
 * Reads all favorite restaurant/cafe
 */
	@SuppressWarnings("unchecked")
	public Vector<Restaurant> readFavorite(){
		try {
			FileInputStream fis = cntxt.openFileInput(fileName);
			ObjectInputStream is = new ObjectInputStream(fis);
			restaurantList = new Vector<Restaurant>();
			restaurantList.addAll((Vector<Restaurant>) is.readObject());
			is.close();
		} catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}

		if (null == restaurantList) {
			restaurantList = new Vector<Restaurant>();
		}
		return restaurantList;
	}
/**
 * Update favorite restaurant/cafe
 */
	public void updateFavorite(String treatId, Restaurant treat) {
		restaurantList = readFavorite();
		Restaurant treatToupdate = null;
		for (Restaurant temp : restaurantList) {
			if (temp.getId().equalsIgnoreCase(treatId)) {
				treatToupdate = temp;
				break;
			}
		}
		restaurantList.remove(treatToupdate);
		restaurantList.add(treat);
		try {
			FileOutputStream fos = cntxt.openFileOutput(fileName,
					Context.MODE_PRIVATE);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(restaurantList);
			os.close();
		} catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}
	}
/**
 * Delete specific restaurant/cafe which is unfavorite
 */
	public void deleteFavorite(String treatIdtoDel) {
		restaurantList = readFavorite();
		Restaurant treatToDel = null;
		for (Restaurant temp : restaurantList) {
			if (temp.getId().equalsIgnoreCase(treatIdtoDel)) {
				treatToDel = temp;
				break;
			}
		}
		restaurantList.remove(treatToDel);
		try {
			FileOutputStream fos = cntxt.openFileOutput(fileName,
					Context.MODE_PRIVATE);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(restaurantList);
			os.close();
		} catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}
	}

	public void writeFavorite(Restaurant treat) {
		restaurantList = readFavorite();
		boolean isThere = false;
		boolean isChanged = false;
		for (Restaurant rest : restaurantList) {
			if (treat.getId().equalsIgnoreCase(rest.getId())) {
				isThere = true;
				if (!treat.getChanged().equalsIgnoreCase(rest.getChanged())) {
					isChanged = true;
				}
				break;
			}
		}
		if (!isThere)
			restaurantList.add(treat);
		else if (isChanged)
			updateFavorite(treat.getId(), treat);
		try {
			FileOutputStream fos = cntxt.openFileOutput(fileName,
					Context.MODE_PRIVATE);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(restaurantList);
			os.close();
		} catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}
	}

	public void deleteAll() {
		restaurantList = readFavorite();
		restaurantList.clear();
		try {
			FileOutputStream fos = cntxt.openFileOutput(fileName,
					Context.MODE_PRIVATE);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(restaurantList);
			os.close();
		} catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}
	}
}
