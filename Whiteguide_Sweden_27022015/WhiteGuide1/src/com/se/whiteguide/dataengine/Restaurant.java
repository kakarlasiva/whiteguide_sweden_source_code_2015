package com.se.whiteguide.dataengine;

import java.io.Serializable;
import java.util.Vector;

@SuppressWarnings("serial")
public class Restaurant implements Serializable {
	String title, id, changed;
	String address, city, phone, zip, website, open, province, box, email,
			directions, comment_count, last_comment, owner, barkeeper, manager,
			chief_chef, sommelier, number_of_seats, cheapest_entre,
			expensive_entre, cheapest_main_dish, expensive_main_dish,
			cheapest_dessert, expensive_dessert, cheapest_menu, expensive_menu,
			index, position;
	double lat, lon;
	int score=0;
	String guideYear,guideId;
	boolean isNytestat;
	

	public boolean isNytestat() {
		return isNytestat;
	}

	public void setNytestat(boolean isNytestat) {
		this.isNytestat = isNytestat;
	}

	public String getGuideYear() {
		return guideYear;
	}

	public void setGuideYear(String guideYear) {
		this.guideYear = guideYear;
	}

	public String getGuideId() {
		return guideId;
	}

	public void setGuideId(String guideId) {
		this.guideId = guideId;
	}

	public int getScore() {
		return score;
	}

	public void setScore(String score) {
		try {
			this.score = Integer.parseInt(score);
		} catch (Exception e) {

		}
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	Vector<String> pictures, symbols;
	private String restaurantclass = "";
	private String description = " ";
	private String classification = "";

	public Restaurant() {
		pictures = new Vector<String>();
		symbols = new Vector<String>();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getChanged() {
		return changed;
	}

	public void setChanged(String changed) {
		this.changed = changed;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getOpen() {
		return open;
	}

	public void setOpen(String open) {
		this.open = open;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getBox() {
		return box;
	}

	public void setBox(String box) {
		this.box = box;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDirections() {
		return directions;
	}

	public void setDirections(String directions) {
		this.directions = directions;
	}

	public String getComment_count() {
		return comment_count;
	}

	public void setComment_count(String comment_count) {
		this.comment_count = comment_count;
	}

	public String getLast_comment() {
		return last_comment;
	}

	public void setLast_comment(String last_comment) {
		this.last_comment = last_comment;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getBarkeeper() {
		return barkeeper;
	}

	public void setBarkeeper(String barkeeper) {
		this.barkeeper = barkeeper;
	}

	public String getManager() {
		return manager;
	}

	public void setManager(String manager) {
		this.manager = manager;
	}

	public String getChief_chef() {
		return chief_chef;
	}

	public void setChief_chef(String chief_chef) {
		this.chief_chef = chief_chef;
	}

	public String getSommelier() {
		return sommelier;
	}

	public void setSommelier(String sommelier) {
		this.sommelier = sommelier;
	}

	public String getNumber_of_seats() {
		return number_of_seats;
	}

	public void setNumber_of_seats(String number_of_seats) {
		this.number_of_seats = number_of_seats;
	}

	public String getCheapest_entre() {
		return cheapest_entre;
	}

	public void setCheapest_entre(String cheapest_entre) {
		this.cheapest_entre = cheapest_entre;
	}

	public String getExpensive_entre() {
		return expensive_entre;
	}

	public void setExpensive_entre(String expensive_entre) {
		this.expensive_entre = expensive_entre;
	}

	public String getCheapest_main_dish() {
		return cheapest_main_dish;
	}

	public void setCheapest_main_dish(String cheapest_main_dish) {
		this.cheapest_main_dish = cheapest_main_dish;
	}

	public String getExpensive_main_dish() {
		return expensive_main_dish;
	}

	public void setExpensive_main_dish(String expensive_main_dish) {
		this.expensive_main_dish = expensive_main_dish;
	}

	public String getCheapest_dessert() {
		return cheapest_dessert;
	}

	public void setCheapest_dessert(String cheapest_dessert) {
		this.cheapest_dessert = cheapest_dessert;
	}

	public String getExpensive_dessert() {
		return expensive_dessert;
	}

	public void setExpensive_dessert(String expensive_dessert) {
		this.expensive_dessert = expensive_dessert;
	}

	public String getCheapest_menu() {
		return cheapest_menu;
	}

	public void setCheapest_menu(String cheapest_menu) {
		this.cheapest_menu = cheapest_menu;
	}

	public String getExpensive_menu() {
		return expensive_menu;
	}

	public void setExpensive_menu(String expensive_menu) {
		this.expensive_menu = expensive_menu;
	}

	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
		try {
			this.setLat(Double.parseDouble(position
					.substring(0, position.indexOf(" "))
					.replaceAll("[a-zA-Z(]", "").trim()));
			this.setLon(Double.parseDouble(position
					.substring(position.indexOf(" ") + 1, position.length())
					.replaceAll("[a-zA-Z)]", "").trim()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Vector<String> getPictures() {
		return pictures;
	}

	public void setPictures(Vector<String> pictures) {
		this.pictures = pictures;
	}

	public Vector<String> getSymbols() {
		return symbols;
	}

	public void setSymbols(Vector<String> symbols) {
		this.symbols = symbols;
	}

	public void setRestaurantClass(String ratingclass) {
	   this.restaurantclass = ratingclass;
	}

	public String getRestaurantClass() {
		return this.restaurantclass;
	}

	public void setDescription(String desc) {
		this.description = desc;
	}

	public String getDescription() {
		return this.description;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

	public String getClassification() {
		return this.classification;
	}
}
