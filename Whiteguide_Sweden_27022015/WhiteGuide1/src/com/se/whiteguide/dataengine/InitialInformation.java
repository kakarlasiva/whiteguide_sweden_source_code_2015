package com.se.whiteguide.dataengine;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.util.Vector;

import android.content.Context;

import com.se.whiteguide.helpers.BasicInfoBean;
import com.se.whiteguide.log.MyDebug;
/** 
 * File which contains all information where guide is bought or not.
 * @author Conevo
 */
public class InitialInformation {
	Vector<BasicInfoBean> beanList;
	Context cntxt;
	private String fileName = "Information.txt";
	public DataChanged dataChanged;
	public String requestFor;
	private static String TAG="InitialInformation";

	public InitialInformation(Context contxt) {
		this.cntxt = contxt;
	}

	public boolean isAtleastOneguideBought()  {
		boolean isbought = false;
		beanList = readData();
		for (BasicInfoBean temp : beanList) {
			if (temp.isGuideisBought())
				return true;
		}
		return isbought;
	}
	
	public boolean isNytestat_shown(){
		boolean isnytestat = false;
		beanList = readData();
		for (BasicInfoBean temp : beanList) {
			if (temp.isNytestat_enabled())
				return true;
		}
		return isnytestat;
	}

	public void setListenerforthis(String id, DataChanged changedListener) {
		dataChanged = changedListener;
		requestFor = id;
	}

	public void updateTreat(String treatId, BasicInfoBean treat) {
		beanList = readData();
		MyDebug.appendLog("id:" + treatId+"list of objects:"+beanList.size());

		BasicInfoBean treatToupdate = null;
		int position = 0;
		for (BasicInfoBean temp : beanList) {
			if (temp.getGuideId().equalsIgnoreCase(treatId)) {
				treatToupdate = temp;
				position=beanList.indexOf(treatToupdate);
				break;
			}
		}

		beanList.set(position, treat);
		MyDebug.appendLog("Position: "+position+" id:" + treatId+" list of objects:"+beanList.size());

		
		try {
			FileOutputStream fos = cntxt.openFileOutput(fileName,
					Context.MODE_PRIVATE);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(beanList);
			os.close();
		} catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}
		try {
			if (null != dataChanged && treatId.equalsIgnoreCase(requestFor)) {
				dataChanged.dataChanged(treat);
			}
		} catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	public Vector<BasicInfoBean> readData() {
		try {
			FileInputStream fis = cntxt.openFileInput(fileName);
			ObjectInputStream is = new ObjectInputStream(fis);
			beanList = new Vector<BasicInfoBean>();
			beanList = (Vector<BasicInfoBean>) is.readObject();
			is.close();
		} catch (FileNotFoundException e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		} catch (StreamCorruptedException e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		} catch (IOException e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		} catch (ClassNotFoundException e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}

		if (null == beanList) {
			beanList = new Vector<BasicInfoBean>();
		}

		return beanList;
	}

	public void writeAll(Vector<BasicInfoBean> _beanList) {
		if (null == beanList)
			beanList = new Vector<BasicInfoBean>();
		beanList = _beanList;
		try {
			FileOutputStream fos = cntxt.openFileOutput(fileName,
					Context.MODE_PRIVATE);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(beanList);
			os.close();
		} catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}
	}

	public interface DataChanged {
		public void dataChanged(BasicInfoBean beanObj);
	}
}
