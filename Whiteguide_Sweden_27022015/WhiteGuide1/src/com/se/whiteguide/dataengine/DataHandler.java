package com.se.whiteguide.dataengine;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Vector;

import android.content.Context;

import com.se.whiteguide.helpers.BasicInfoBean;
import com.se.whiteguide.log.MyDebug;

/**
 * Creating file with restaurant data,updating and deleting the data if neccessary. 
 * @author Conevo
 */
public class DataHandler {
	DataHandler treatHandler;
	Context cntxt;
	private String dataFile;
	private String tempFile;
	private static String TAG="DataHandler";

	public DataHandler(Context context) {
		this.cntxt = context;
	}

	/**
	 * creates a file and writes the data count in it
	 */
	@SuppressWarnings("unchecked")
	public int getDataCount(Vector<BasicInfoBean> beanList) {
		int dataCount = 0;
		for (BasicInfoBean basicInfoBean : beanList) {
			dataFile = basicInfoBean.getGuideName() + ".txt";
			try {
				FileInputStream fis = cntxt.openFileInput(dataFile);
				ObjectInputStream is = new ObjectInputStream(fis);
				dataCount += ((Vector<Restaurant>) is.readObject()).size();
				is.close();
			} catch (Exception e) {
				MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
			}
		}
		return dataCount;
	}

	@SuppressWarnings("unchecked")
	public int getDataCount(BasicInfoBean basicInfoBean){
		int dataCount = 0;
		
			dataFile = basicInfoBean.getGuideName() + ".txt";
			try {
				FileInputStream fis = cntxt.openFileInput(dataFile);
				ObjectInputStream is = new ObjectInputStream(fis);
				dataCount += ((Vector<Restaurant>) is.readObject()).size();
				is.close();
			} catch (Exception e) {
				MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
			}
		
		return dataCount;
	}

	public DataHandler(Context context, String fielName) {
		cntxt = context;
		dataFile = fielName + ".txt";
		tempFile = fielName + "_temp.txt";
	}

	/**
	 * Reads the restaurant in file
	 */
	@SuppressWarnings("unchecked")
	public Vector<Restaurant> readRestaurant() {
		Vector<Restaurant> dummyRestaurants = null;
		try {
			FileInputStream fis = cntxt.openFileInput(dataFile);
			ObjectInputStream is = new ObjectInputStream(fis);
			dummyRestaurants = new Vector<Restaurant>();
			dummyRestaurants.addAll((Vector<Restaurant>) is.readObject());
			is.close();
		} catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}

		if (null == dummyRestaurants) {
			dummyRestaurants = new Vector<Restaurant>();
		}
		return dummyRestaurants;
	}
/**
 * updates the restaurant in file
 */
	public void updateRestaurant(String treatId, Restaurant treat) {
		Vector<Restaurant> dummyRestaurants = null;
		dummyRestaurants = readRestaurant();
		Restaurant treatToupdate = null;
		for (Restaurant temp : dummyRestaurants) {
			if (temp.getId().equalsIgnoreCase(treatId)) {
				treatToupdate = temp;
				break;
			}
		}
		dummyRestaurants.remove(treatToupdate);
		dummyRestaurants.add(treat);
		try {
			FileOutputStream fos = cntxt.openFileOutput(tempFile,
					Context.MODE_PRIVATE);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(dummyRestaurants);
			os.close();
		} catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}
	}
/**
 * Delete specific restaurant in file
 */
	public void deleteRestaurant(String treatIdtoDel) {
		Vector<Restaurant> dummyRestaurants = null;
		dummyRestaurants = readRestaurant();
		Restaurant treatToDel = null;
		for (Restaurant temp : dummyRestaurants) {
			if (temp.getId().equalsIgnoreCase(treatIdtoDel)) {
				treatToDel = temp;
				break;
			}
		}
		dummyRestaurants.remove(treatToDel);
		try {
			FileOutputStream fos = cntxt.openFileOutput(dataFile,
					Context.MODE_PRIVATE);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(dummyRestaurants);
			os.close();
		} catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}
	}
/**
 * gets all restuarnt and writes in a file
 */
	public void writeAll(Vector<Restaurant> restList){
		Vector<Restaurant> dummyRestaurants = new Vector<Restaurant>();
		dummyRestaurants = restList;
		try {
			FileOutputStream fos = cntxt.openFileOutput(dataFile,
					Context.MODE_PRIVATE);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(dummyRestaurants);
			os.close();
		} catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	public Vector<Restaurant> readTempData() {
		Vector<Restaurant> dummyRestaurants = null;
		try {
			FileInputStream fis = cntxt.openFileInput(tempFile);
			ObjectInputStream is = new ObjectInputStream(fis);
			dummyRestaurants = new Vector<Restaurant>();
			dummyRestaurants.addAll((Vector<Restaurant>) is.readObject());
			is.close();
		} catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}

		if (null == dummyRestaurants) {
			dummyRestaurants = new Vector<Restaurant>();
		}
		return dummyRestaurants;
	}

	public void writetoTemp(Vector<Restaurant> restList) {
		Vector<Restaurant> dummyRestaurants = new Vector<Restaurant>();
		dummyRestaurants = restList;
		try {
			FileOutputStream fos = cntxt.openFileOutput(tempFile,
					Context.MODE_PRIVATE);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(dummyRestaurants);
			os.close();
		} catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}

	}

	public void updateAll(Vector<Restaurant> restList) {
		Vector<Restaurant> dummyRestaurants = new Vector<Restaurant>();
		dummyRestaurants = restList;
		try {
			FileOutputStream fos = cntxt.openFileOutput(dataFile,
					Context.MODE_PRIVATE);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(dummyRestaurants);
			os.close();
		} catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}
	}
}
