package com.se.whiteguide.dataengine;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.util.Vector;

import android.content.Context;

import com.se.whiteguide.helpers.BasicInfoBean;
import com.se.whiteguide.log.MyDebug;

public class TransferToServer {
	Vector<BasicInfoBean> beanList;
	Context cntxt;
	private String fileName = "SendToServer.txt";
	public String requestFor;

	public TransferToServer(Context contxt) {
		this.cntxt = contxt;
	}

	public void writeTreat(BasicInfoBean treat) {
		beanList = readData();
		beanList.add(treat);
		try {
			FileOutputStream fos = cntxt.openFileOutput(fileName,
					Context.MODE_PRIVATE);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(beanList);
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteTreat(BasicInfoBean beanObj) {
		beanList = this.readData();
		BasicInfoBean dummy = null;
		for (BasicInfoBean temp : beanList) {
			/*if (temp.getGuideId().equalsIgnoreCase(beanObj.getGuideId())
					&& temp.getGuideYear().equalsIgnoreCase(
							beanObj.getGuideYear())) {*/
			if (temp.getGuideId().equalsIgnoreCase(beanObj.getGuideId())) {
				dummy = temp;
				break;
			}
		}
		if (null != dummy) {
			beanList.remove(dummy);
		}
		try {
			FileOutputStream fos = cntxt.openFileOutput(fileName,
					Context.MODE_PRIVATE);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(beanList);
			os.close();
		} catch (Exception e) {
		     e.printStackTrace();
		}
	}

	public void deleteTreat(String id, String year) {
		beanList = this.readData();
		BasicInfoBean dummy = null;
		for (BasicInfoBean temp : beanList) {
			/*if (temp.getGuideId().equalsIgnoreCase(id)
					&& temp.getGuideYear().equalsIgnoreCase(year)) {*/
			if (temp.getGuideId().equalsIgnoreCase(id)) {
				dummy = temp;
				break;
			}
		}
		if (null != dummy) {
			beanList.remove(dummy);
		}
		try {
			FileOutputStream fos = cntxt.openFileOutput(fileName,
					Context.MODE_PRIVATE);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(beanList);
			os.close();
		} catch (Exception e) {
		     e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public Vector<BasicInfoBean> readData() {
		try {
			FileInputStream fis = cntxt.openFileInput(fileName);
			ObjectInputStream is = new ObjectInputStream(fis);
			beanList = new Vector<BasicInfoBean>();
			beanList = (Vector<BasicInfoBean>) is.readObject();
			is.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			MyDebug.log_info("BASIC", "Read File: " , e.getMessage());

		} catch (StreamCorruptedException e) {
			e.printStackTrace();
			MyDebug.log_info("BASIC", "Read File: " , e.getMessage());

		} catch (IOException e) {
			e.printStackTrace();
			MyDebug.log_info("BASIC", "Read File: " , e.getMessage());

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			MyDebug.log_info("BASIC", "Read File: " , e.getMessage());

		} catch (Exception e) {
			e.printStackTrace();
			MyDebug.log_info("BASIC", "Read File: " , e.getMessage());
		}

		if (null == beanList) {
			beanList = new Vector<BasicInfoBean>();
		}

		return beanList;
	}
}
