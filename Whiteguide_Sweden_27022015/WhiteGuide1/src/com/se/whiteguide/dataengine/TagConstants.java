package com.se.whiteguide.dataengine;

public class TagConstants {

	public static final String ID = "id";
	public static final String NAME = "title";
	public static final String CHANGED = "changed";
	public static final String ITEM = "item";
	public static final String CLASSIFICATION = "class";

	// Constants related to individual Restaurant Item.
	public static final String ADDRESS = "address";
	public static final String CITY = "city";
	public static final String PHONE = "phone";
	public static final String ZIP = "zip";
	public static final String WEBSITE = "website";
	public static final String OPEN = "open";
	public static final String PROVINCE = "province";
	public static final String BOX = "box";
	public static final String EMAIL = "email";
	public static final String DIRECTIONS = "directions";
	public static final String COMMENT_COUNT = "comment_count";
	public static final String LAST_COMMENT = "last_comment";
	public static final String OWNER = "owner";
	public static final String BARKEEPER = "barkeeper";
	public static final String MANAGER = "manager";
	public static final String CHIEF_CHEF = "chief_chef";
	public static final String SOMMELIER = "sommelier";
	public static final String NUMBER_OF_SEATS = "number_of_seats";
	public static final String CHEAPEST_ENTRE = "cheapest_entre";
	public static final String EXPENSIV_ENTRE = "expensive_entre";
	public static final String CHEAPEST_MAIN_DISH = "cheapest_main_dish";
	public static final String EXPENSIVE_MAIN_DISH = "expensive_main_dish";
	public static final String CHEAPEST_DESERT = "cheapest_dessert";
	public static final String EXPENSIVE_DESERT = "expensive_dessert";
	public static final String CHEAPEST_MENU = "cheapest_menu";
	public static final String EXPENSIVE_MENU = "expensive_menu";
	public static final String INDEX = "index";
	public static final String POSITION = "position";
	public static final String PICTURES = "pictures";
	public static final String SYMBOLS = "symbols";
	public static final String COORDINATES = "coordinates";

	public static final String GUIDE_NAME = "guidename";
	public static final String GUIDE_YEAR = "year";
	public static final String GUIDE_ID = "id";
	public static final String GUIDE_PURCHASE_INFO = "purchase";
	public static final String GUIDES = "guides";
	public static final String GUIDE = "Guide";
	public static final String GUIDE_SUBID="android_inappId";
	public static final String MINIMUM_VERSION = "minimumandroidversion";
	public static final String MAXIMUM_VERSION = "latestandroidversion";
	public static final String NYTESTAT_ENABLE="nytestat_enabled";

	public static final String SCORE = "total";
	public static final String GUIDE_PRICE = "price";
	public static final String GUIDE_ACTIVEDATES = "active";
	public static final String DISPLAY_GUIDE_NAME="displayguidename";
}
