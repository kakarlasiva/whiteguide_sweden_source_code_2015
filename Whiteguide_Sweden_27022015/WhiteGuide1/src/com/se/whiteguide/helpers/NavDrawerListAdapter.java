package com.se.whiteguide.helpers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.live.se.whiteguide.R;


public class NavDrawerListAdapter extends BaseExpandableListAdapter {

	private Context context;
	private ArrayList<NavDrawerItem> navDrawerItems;
	private Map<String, List<String>> MenuCollections;
	String[] subitems, tempchild;
	LayoutInflater mlayout;
	public Activity activity;
	public ArrayList<String> tempChild, itemName;

	public NavDrawerListAdapter(Context context, ArrayList<String> items,
			Map<String, List<String>> menuCollection,
			ArrayList<NavDrawerItem> navDrawerItems2) {
		this.context = context;
		this.itemName = items;
		this.MenuCollections = menuCollection;
		this.navDrawerItems = navDrawerItems2;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		
		return MenuCollections.get(itemName.get(groupPosition)).get(
				childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		
		return childPosition;
	}

	public void setInflater(LayoutInflater mInflater, Activity act) {
		this.mlayout = mInflater;
		activity = act;
	}

	@Override
	public View getChildView(int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		
		final String menusubitem = (String) getChild(groupPosition,
				childPosition);
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater) context
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.drawer_child_item,parent,false);
		}
        TextView txtTitle = (TextView) convertView
				.findViewById(R.id.SUbmenutitle);
		txtTitle.setTextColor(Color.BLACK);
			txtTitle.setText(menusubitem);
        return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return MenuCollections.get(itemName.get(groupPosition)).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
	     return itemName.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return itemName.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		
		return groupPosition;
	}

	@Override
	public void onGroupCollapsed(int groupPosition) {
		super.onGroupCollapsed(groupPosition);
	}

	@Override
	public void onGroupExpanded(int groupPosition) {
		super.onGroupExpanded(groupPosition);
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		String itemName = (String) getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater) context
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.drawer_list_item, parent,false);
		}
		if (groupPosition == 2 && !isExpanded)
			convertView.setBackgroundResource(R.drawable.actionbar);

		ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
		TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
	    imgIcon.setImageResource(navDrawerItems.get(groupPosition).getIcon());
		txtTitle.setText(itemName);
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int arg0, int arg1) {
		return true;
	}

}
