package com.se.whiteguide.helpers;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.live.se.whiteguide.R;



/**
 *  Represents adapter to list all restaurant/cafe according to city
 *  @author Conevo
 */
public class CityWiseRestaurantsAdapter extends ArrayAdapter<String> implements
		SectionIndexer {

	private List<String> stringArray;
	private Context context;
	protected List<String> list;
	protected ArrayList<String> sortedList;
	LayoutInflater inflate;

	public CityWiseRestaurantsAdapter(Context cntxt, int resource,
			Vector<String> citiesList) {
		super(cntxt, resource, citiesList);
		stringArray = citiesList;
		this.context = cntxt;
		list = citiesList;
		inflate = ((Activity) context).getLayoutInflater();
	}

	public int getCount() {
		return stringArray.size();
	}

	public String getItem(int arg0) {
		return stringArray.get(arg0);
	}

	public long getItemId(int arg0) {
		return 0;
	}

	@SuppressLint("ViewHolder")
	public View getView(int position, View v, ViewGroup parent) {
		View view = (View) inflate.inflate(R.layout.layout_city,parent,false);
		LinearLayout header = (LinearLayout) view.findViewById(R.id.section);
		String label = stringArray.get(position);

		char firstChar = label.toUpperCase().charAt(0);
		if (position == 0) {
			setSection(header, label);
		} else {
			String preLabel = stringArray.get(position - 1);
			char preFirstChar = preLabel.toUpperCase().charAt(0);
			if (firstChar != preFirstChar) {
				setSection(header, label);
			} else {
				header.setVisibility(View.GONE);
			}
		}
		TextView textView2 = (TextView) view.findViewById(R.id.city_name);
		textView2.setText(stringArray.get(position));
		textView2.setTextColor(Color.BLACK);
		return view;
	}

	private void setSection(LinearLayout header, String label) {
		TextView text = new TextView(context);
		header.setBackgroundColor(Color.BLACK);
		text.setTextColor(Color.WHITE);
		text.setText(label.substring(0, 1).toUpperCase());
		text.setTextSize(13);
		text.setPadding(5, 0, 0, 0);
		text.setGravity(Gravity.CENTER_VERTICAL);
	    header.addView(text);
	}

	public int getPositionForSection(int section) {
		if (section == 35) {
			return 0;
		}
		for (int i = 0; i < stringArray.size(); i++) {
			String l = stringArray.get(i);
			char firstChar = l.toUpperCase().charAt(0);
			if (firstChar == section) {
				return i;
			}
		}
		return -1;
	}

	public int getSectionForPosition(int arg0) {
		return 0;
	}

	public Object[] getSections() {
		return null;
	}

	@Override
	public Filter getFilter() {
		Filter filter = new Filter() {

			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				stringArray = (List<String>) results.values;
				notifyDataSetChanged();
			}

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults results = new FilterResults();
				List<String> FilteredArrList = new ArrayList<String>();
				sortedList = new ArrayList<String>(list);

				if (constraint == null || constraint.length() == 0) {
					// set the Original result to return
					results.count = sortedList.size();
					results.values = sortedList;
				} else {
					constraint = constraint.toString().toLowerCase();
					for (int i = 0; i < sortedList.size(); i++) {
						String data = sortedList.get(i);
						if (data.toLowerCase().contains(constraint.toString())) {
							FilteredArrList.add(data);
						}
					}
					// set the Filtered result to return
					results.count = FilteredArrList.size();
					results.values = FilteredArrList;
				}
				return results;
			}
		};
		return filter;
	}

}