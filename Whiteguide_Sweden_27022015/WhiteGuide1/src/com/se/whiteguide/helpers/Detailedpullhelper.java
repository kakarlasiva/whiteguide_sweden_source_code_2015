package com.se.whiteguide.helpers;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Detailedpullhelper implements Serializable{
	
	private String id,title,full_des,desc,itemurl,imageurl,pubdate;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFull_des() {
		return full_des;
	}

	public void setFull_des(String full_des) {
		this.full_des = full_des;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getItemurl() {
		return itemurl;
	}

	public void setItemurl(String itemurl) {
		this.itemurl = itemurl;
	}

	public String getImageurl() {
		return imageurl;
	}

	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}

	public String getPubdate() {
		return pubdate;
	}

	public void setPubdate(String pubdate) {
		this.pubdate = pubdate;
	}

}
