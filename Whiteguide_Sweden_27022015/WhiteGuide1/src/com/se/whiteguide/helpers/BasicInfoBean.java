package com.se.whiteguide.helpers;

import java.io.Serializable;

import com.se.whiteguide.util.Purchase;

@SuppressWarnings("serial")
public class BasicInfoBean implements Serializable{
	String guideName = "", guideYear = "", guideId = "",paymentInfo="",guideActiveDates="",guidePrice="",displayguidename="",android_inapp_d="";
	boolean Nytestat_enabled;
	public boolean isNytestat_enabled() {
		return Nytestat_enabled;
	}

	public void setNytestat_enabled(boolean nytestat_enabled) {
		Nytestat_enabled = nytestat_enabled;
	}

	public String getDisplayguidename() {
		return displayguidename;
	}

	public void setDisplayguidename(String displayguidename) {
		this.displayguidename = displayguidename;
	}

	public String getAndroid_inapp_d() {
		return android_inapp_d;
	}

	public void setAndroid_inapp_d(String android_inapp_d) {
		this.android_inapp_d = android_inapp_d;
	}

	int minimumversion=0,maximumversion=0;
	
	Purchase purchaseInfo;
	
	public Purchase getPurchaseInfo() {
		return purchaseInfo;
	}

	public void setPurchaseInfo(Purchase purchaseInfo) {
		this.purchaseInfo = purchaseInfo;
	}

	public int getMinimumversion() {
		return minimumversion;
	}

	public void setMinimumversion(int minimumversion) {
		this.minimumversion = minimumversion;
	}

	public int getMaximumversion() {
		return maximumversion;
	}

	public void setMaximumversion(int maximumversion) {
		this.maximumversion = maximumversion;
	}

	public String getGuideActiveDates() {
		return guideActiveDates;
	}

	public void setGuideActiveDates(String guideActiveDates) {
		this.guideActiveDates = guideActiveDates;
	}

	public String getGuidePrice() {
		return guidePrice;
	}

	public void setGuidePrice(String guidePrice) {
		this.guidePrice = guidePrice;
	}

	public String getPaymentInfo() {
		return paymentInfo;
	}

	public void setPaymentInfo(String paymentInfo) {
		this.paymentInfo = paymentInfo;
	}

	boolean guideisBought = false;

	public String getGuideName() {
		return guideName;
	}

	public void setGuideName(String guideName) {
		this.guideName = guideName;
	}

	public String getGuideYear() {
		return guideYear;
	}

	public void setGuideYear(String guideYear) {
		this.guideYear = guideYear;
	}

	public String getGuideId() {
		return guideId;
	}

	public void setGuideId(String guideId) {
		this.guideId = guideId;
	}

	public boolean isGuideisBought() {
		return guideisBought;
	}

	public void setGuideisBought(boolean guideisBought) {
		this.guideisBought = guideisBought;
	}

	
}
