package com.se.whiteguide.helpers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.live.se.whiteguide.R;
import com.se.whiteguide.dataengine.Restaurant;
import com.se.whiteguide.tabgroup.Home;

/**
 * Represents adapter to display restaurant/cafe in a list.
 * 
 * @author Conevo
 */
public class AZRestaurantsAdapter extends ArrayAdapter<Restaurant> implements
		SectionIndexer {

	private List<Restaurant> stringArray;
	private Context context;
	protected List<Restaurant> list;
	protected ArrayList<Restaurant> sortedList;
	LayoutInflater inflate;
	boolean isbought = false;
	BasicInfoBean beanObj;
	public static String DATE_FORMAT = "dd MMM yyyy";

	public AZRestaurantsAdapter(Context cntxt, int resource,
			List<Restaurant> data, boolean buyinfo, BasicInfoBean bean) {
		super(cntxt, resource, data);
		stringArray = data;
		this.context = cntxt;
		list = data;
		this.isbought = buyinfo;
		this.beanObj = bean;
		inflate = ((Activity) context).getLayoutInflater();
	}

	public int getCount() {
		return stringArray.size();
	}

	public Restaurant getItem(int arg0) {
		return stringArray.get(arg0);
	}

	public long getItemId(int arg0) {
		return 0;
	}

	public boolean isNytestat() {
		if (beanObj.getGuideName().equalsIgnoreCase(
				context.getString(R.string.Nytestat_name))) {
			return true;
		} else {
			return false;
		}
	}

	@SuppressLint("ViewHolder")
	public View getView(int position, View v, ViewGroup parent) {
		View view = (View) inflate.inflate(R.layout.layout_row,parent,false);
		LinearLayout header = (LinearLayout) view.findViewById(R.id.section);
		String label, preLabel, nlabel = null;
		label = stringArray.get(position).getTitle().trim();
		if (!isNytestat()) {

			char firstChar = label.toUpperCase().charAt(0);
			if (position == 0) {
				setSection(header, label);
			} else {
				preLabel = stringArray.get(position - 1).getTitle();
				char preFirstChar = preLabel.toUpperCase().charAt(0);
				if (firstChar != preFirstChar) {
					setSection(header, label);
				} else {
					header.setVisibility(View.GONE);
				}
			}
		} else {
			nlabel = convertDate(stringArray.get(position).getChanged(), DATE_FORMAT);
			if (position == 0) {
				setSection(header, nlabel);
			} else {
				preLabel = convertDate(stringArray.get(position-1).getChanged(), DATE_FORMAT);
				if (!nlabel.equalsIgnoreCase(preLabel)) {
					setSection(header, nlabel);
				} else {
					header.setVisibility(View.GONE);
				}
			}

		}
		TextView textView = (TextView) view.findViewById(R.id.restaurant_name);
		try {
			if (label.startsWith("#")) {
				label = label.substring(1, label.length());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (label.length() > 0) {
			label = label.substring(0, 1).toUpperCase() + label.substring(1);
		}
		textView.setText(label);
		TextView textView2 = (TextView) view.findViewById(R.id.city_name);
		textView2.setText(stringArray.get(position).getCity());
		LinearLayout layout = (LinearLayout) view
				.findViewById(R.id.class_layout);
		layout.setVisibility(View.VISIBLE);
		ImageView clasificationImage = (ImageView) view
				.findViewById(R.id.cart_item_image);
		TextView totalPoint = (TextView) view.findViewById(R.id.total);
		if (isbought || isNytestat()) {
			if (stringArray.get(position).getRestaurantClass().contains("1")) {
				clasificationImage.setImageResource(R.drawable.intklass);
			} else if (stringArray.get(position).getRestaurantClass()
					.contains("2")) {
				clasificationImage.setImageResource(R.drawable.masterklass);
			} else if (stringArray.get(position).getRestaurantClass()
					.contains("3")) {
				clasificationImage.setImageResource(R.drawable.myketgodklass);
			} else if (stringArray.get(position).getRestaurantClass()
					.contains("4")) {
				clasificationImage.setImageResource(R.drawable.godklass);
			} else if (stringArray.get(position).getRestaurantClass()
					.contains("7")) {
				clasificationImage.setImageResource(R.drawable.brastalle);
			}
			if (stringArray.get(position).getScore() > 0
					&& !stringArray.get(position).getRestaurantClass()
							.contains("7")) {
				totalPoint.setVisibility(View.VISIBLE);
				totalPoint.setText("" + stringArray.get(position).getScore());
			}
		}
		layout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!isbought
						&& !(beanObj.getGuideName().equalsIgnoreCase(context
								.getString(R.string.Nytestat_name)))) {
					showAlertBox();
				}
			}
		});
		return view;
	}

	TextView sectionText;

	protected void showAlertBox() {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(this.context.getString(R.string.guide_buy1) + " "
				+ beanObj.getGuideName() + " " + beanObj.getGuideYear() + " "
				+ this.context.getString(R.string.guide_buy2));
		builder.setPositiveButton(this.context.getString(R.string.buy),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Home.setTab(4);
					}
				});
		builder.setNegativeButton(context.getString(R.string.cancel),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		builder.show();

	}

	private void setSection(LinearLayout header, String label) {
		sectionText = new TextView(context);
		header.setBackgroundColor(Color.BLACK);
		sectionText.setTextColor(Color.WHITE);
		if (isNytestat()) {
			sectionText.setText(label);
		} else {
			sectionText.setText(label.substring(0, 1).toUpperCase());
		}
		sectionText.setTextSize(13);
		sectionText.setPadding(5, 0, 0, 0);
		sectionText.setGravity(Gravity.CENTER_VERTICAL);
		header.addView(sectionText);
		header.setPadding(0, 0, 30, 0);
	}

	public int getPositionForSection(int section) {
		if (section == 35) {
			return 0;
		}
		for (int i = 0; i < stringArray.size(); i++) {
			String l = stringArray.get(i).getTitle();
			char firstChar = l.toUpperCase().charAt(0);
			if (firstChar == section) {
				return i;
			}
		}
		return -1;
	}

	public int getSectionForPosition(int arg0) {
		return 0;
	}

	public Object[] getSections() {
		return null;
	}

	public String convertDate(String dateInMilliseconds, String dateFormat) {
		SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

		// Create a calendar object that will convert the date and time value in
		// milliseconds to date.
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(Long.parseLong(dateInMilliseconds) * 1000);
		return formatter.format(calendar.getTime());
	}

	@Override
	public Filter getFilter() {
		Filter filter = new Filter() {

			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				stringArray = (List<Restaurant>) results.values;
				notifyDataSetChanged();
			}

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults results = new FilterResults();
				List<Restaurant> FilteredArrList = new ArrayList<Restaurant>();
				sortedList = new ArrayList<Restaurant>(list);

				if (constraint == null || constraint.length() == 0) {
					// set the Original result to return
					results.count = sortedList.size();
					results.values = sortedList;
				} else {
					constraint = constraint.toString().toLowerCase();
					for (int i = 0; i < sortedList.size(); i++) {
						Restaurant data = sortedList.get(i);
						if (data.getTitle().toLowerCase()
								.contains(constraint.toString())) {
							FilteredArrList.add(data);
						}
					}
					// set the Filtered result to return
					results.count = FilteredArrList.size();
					results.values = FilteredArrList;
				}
				return results;
			}
		};
		return filter;
	}

}