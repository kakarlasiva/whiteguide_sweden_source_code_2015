package com.se.whiteguide.tabgroup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.PendingIntent;
import android.app.TabActivity;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

import com.android.vending.billing.IInAppBillingService;
import com.live.se.whiteguide.R;
import com.se.whiteguide.NetworkChecker;
import com.se.whiteguide.dataengine.DataEngine;
import com.se.whiteguide.dataengine.InitialInformation;
import com.se.whiteguide.dataengine.PaymentInformation;
import com.se.whiteguide.dataengine.TransferToServer;
import com.se.whiteguide.helpers.BasicInfoBean;
import com.se.whiteguide.helpers.NavDrawerItem;
import com.se.whiteguide.helpers.NavDrawerListAdapter;
import com.se.whiteguide.helpers.UserEmailFetcher;
import com.se.whiteguide.log.MyDebug;
import com.se.whiteguide.util.IabHelper;
import com.se.whiteguide.util.IabResult;
import com.se.whiteguide.util.Purchase;

/**
 * Represents the view for slide menu
 * 
 * @author Santosh
 */

@SuppressWarnings("deprecation")
public class Home extends TabActivity implements OnTabChangeListener,
		OnChildClickListener, OnGroupClickListener {
	public static TabHost mHost;
	TabSpec Krognytt, treat, scanner, vouchers, settings, notifications;
	private ScheduledExecutorService stpe;

	public PendingIntent pendingIntent;
	int i = 1;
	SharedPreferences sharedsettings;
	String b_count;

	private static DrawerLayout mDrawerLayout;
	public static ExpandableListView mDrawerList;
	private static ActionBarDrawerToggle mDrawerToggle;
	public static BasicInfoBean beanObj;
	ImageView toggle;

	// slide menu items
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;

	private ArrayList<NavDrawerItem> navDrawerItems;
	private ArrayList<String> items;
	Map<String, List<String>> MenuCollection;
	List<String> childList;
	InitialInformation info_obj;
	PaymentInformation payInfoObj;
	private NavDrawerListAdapter adapter;
	private static String TAG = "Home";

	@Override
	protected void onNewIntent(Intent intent) {
		try {
			mHost.clearAllTabs();
			mHost.addTab(Krognytt);
			mHost.addTab(treat);
			mHost.addTab(scanner);
			mHost.addTab(vouchers);
			mHost.addTab(settings);
			mHost.addTab(notifications);
			if (intent.getBooleanExtra("PUSH_NOTIFICATION", false))
				Home.setTab(4);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void setTab(int index) {
		if (null != mHost)
			mHost.setCurrentTab(index);
		mHost.setFocusable(true);
	}

	boolean frompushIntent = false;

	private View getIndicator(String labelId, int drawableId, int background) {
		View tabIndicator = LayoutInflater.from(this).inflate(
				R.layout.tab_indicator, getTabWidget(), false);
		TextView title = (TextView) tabIndicator.findViewById(R.id.title);
		title.setText(labelId.toUpperCase());
		ImageView icon = (ImageView) tabIndicator.findViewById(R.id.icon);
		icon.setImageResource(drawableId);
		return tabIndicator;
	}

	public static void setDrawer() {
		mDrawerLayout.openDrawer(Gravity.START);
	}

	/**
	 * Consuming In app purchase details of user
	 */
	IInAppBillingService mService;

	ServiceConnection mServiceConn = new ServiceConnection() {
		@Override
		public void onServiceDisconnected(ComponentName name) {
			mService = null;
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			mService = IInAppBillingService.Stub.asInterface(service);
			try {
				Bundle ownedItems = mService.getPurchases(3, getPackageName(),
						"subs", null);
				int response = ownedItems.getInt("RESPONSE_CODE");
				System.out.println("In Home: ressponse code" + response);
				MyDebug.appendLog("response" + response);
				if (response == 0) {
					ArrayList<String> ownedSkus = ownedItems
							.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
					ArrayList<String> purchaseDataList = ownedItems
							.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
					boolean ispurchased = false, isthere = false;

					if (null != purchaseDataList)
						for (int i = 0; i < purchaseDataList.size(); ++i) {
							String purchaseData = purchaseDataList.get(i);
							MyDebug.appendLog("HOME dataaaa" + purchaseData);
							JSONObject json = new JSONObject(purchaseData);
							MyDebug.appendLog("OWN>>>>>>>>>>dataaaa"
									+ json.has("productId"));
							if (json.has("productId")
									&& json.has("autoRenewing")) {
								String id = json.optString("productId");
								payInfoObj.clearAll();
								for (BasicInfoBean obj : info_obj.readData()) {
									// need to check... autorenew flag.
									if (obj.getAndroid_inapp_d()
											.equalsIgnoreCase(id)
											|| (id.equalsIgnoreCase("580") && obj
													.getGuideId()
													.equalsIgnoreCase("58"))) {
										MyDebug.appendLog("HOME dataaaa  >>>>>>>   "
												+ obj.getAndroid_inapp_d());
										obj.setGuideisBought(true);
										obj.setPaymentInfo(purchaseData);
										DataEngine.showAds = false;
										ispurchased = true;
										info_obj.updateTreat(obj.getGuideId(),
												obj);
										payInfoObj.writeTreat(obj);
										break;
									}
								}
								MyDebug.appendLog("HOME dataaaa  >>>>>>>   "
										+ info_obj.readData().size()
										+ ".... Payinto "
										+ payInfoObj.readData().size());
								/*
								 * if (!isthere) { try { timestamp =
								 * Long.parseLong(json
								 * .getString("purchaseTime")); } catch
								 * (Exception e) { timestamp =
								 * System.currentTimeMillis(); }
								 * MyDebug.appendLog("timestamp: " + timestamp);
								 * 
								 * Calendar cal = Calendar.getInstance();
								 * cal.setTimeInMillis(timestamp);
								 * cal.get(Calendar.YEAR); BasicInfoBean bean =
								 * new BasicInfoBean(); bean.setGuideId(id);
								 * bean.setPaymentInfo(purchaseData);
								 * bean.setGuideYear(String.valueOf(cal
								 * .get(Calendar.YEAR)));
								 * 
								 * if (id.equalsIgnoreCase("58")) {
								 * bean.setGuideName("Restauranger"); } else if
								 * (id.equalsIgnoreCase("60")) {
								 * bean.setGuideName("Caf�er"); } else if
								 * (id.equalsIgnoreCase("59")) {
								 * bean.setGuideName("Bar"); } else {
								 * bean.setGuideName("Bar"); }
								 * 
								 * bean.setGuideisBought(true);
								 * transerObj.writeTreat(bean);
								 * MyDebug.appendLog("Year: " +
								 * bean.getGuideYear() + "...Name " +
								 * bean.getGuideName());
								 * 
								 * }
								 */
								// Purchase purchaseObj = new
								// Purchase(purchaseData, null);
								// mHelper.consumeAsync(purchaseObj,mConsumeFinishedListener);
							}

						}
					for (BasicInfoBean bean_obj : info_obj.readData()) {
						if (bean_obj.getGuideName().equalsIgnoreCase(
								getString(R.string.Nytestat_name))) {
							isthere = true;
							break;
						}
					}
					if (!isthere && ispurchased && info_obj.isNytestat_shown()) {
						BasicInfoBean beanObj = new BasicInfoBean();
						beanObj.setGuideId(getApplicationContext().getString(
								R.string.Nytestat_id));
						beanObj.setGuideName(getApplicationContext().getString(
								R.string.Nytestat_name));
						beanObj.setGuidePrice(getApplicationContext()
								.getString(R.string.Nytestat_price));
						beanObj.setGuideYear(getApplicationContext().getString(
								R.string.Nytestat_year));
						beanObj.setGuideisBought(true);
						beanObj.setGuideActiveDates(getApplicationContext()
								.getString(R.string.Nytestat_active));
						beanObj.setDisplayguidename(getApplicationContext()
								.getString(R.string.Nytestat_disguidename));
						beanObj.setAndroid_inapp_d(getApplicationContext()
								.getString(R.string.inapp_subid));
						Vector<BasicInfoBean> beanList = info_obj.readData();
						beanList.add(beanObj);
						info_obj.writeAll(beanList);
						createChildList();
						adapter = new NavDrawerListAdapter(
								getApplicationContext(), items, MenuCollection,
								navDrawerItems);
						mDrawerList.setAdapter(adapter);
						
						Editor editor = sharedsettings.edit();
						if (sharedsettings.getString(DataEngine.SETTINGS_NYTEST, "NOTEXIST")
								.equalsIgnoreCase("NOTEXIST")){
							editor.putString(DataEngine.SETTINGS_NYTEST,"true");
						}
						
						editor.commit();					}

					if (null != ownedItems)
						for (int i = 0; i < ownedItems.size(); ++i) {
							String sku = ownedSkus.get(i);
							MyDebug.appendLog("OWN>>>>>>>>>>dataaaa" + sku);
						}

					// if continuationToken != null, call getPurchases again
					// and pass in the token to retrieve more items
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	// The helper object
	IabHelper mHelper;
	// Called when consumption is complete
	IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
		public void onConsumeFinished(Purchase purchase, IabResult result) {
			MyDebug.appendLog("HOME: THE DATACOSUMED " + result.isSuccess());
		}
	};

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// Hide the Title
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home);
		sharedsettings=getSharedPreferences(DataEngine.SETTINGS_NAME, MODE_PRIVATE);
		payInfoObj = new PaymentInformation(this);

		String base64EncodedPublicKey = DataEngine.base64EncodedPublicKey;

		// Create the helper, passing it our context and the public key to
		// verify signatures with
		MyDebug.appendLog("Creating IAB helper.");
		mHelper = new IabHelper(this, base64EncodedPublicKey);

		// enable debug logging (for a production application, you should set
		// this.getParent() to false).
		mHelper.enableDebugLogging(true);

		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
			public void onIabSetupFinished(IabResult result) {
				MyDebug.appendLog("Home : Setup finished.");
			}
		});
		transerObj = new TransferToServer(getApplicationContext());
		bindService(new Intent(
				"com.android.vending.billing.InAppBillingService.BIND"),
				mServiceConn, Context.BIND_AUTO_CREATE);
		mHost = getTabHost();
		mHost.setOnTabChangedListener(this);

		// News Feed Activity
		Krognytt = mHost.newTabSpec("NewsFeed");
		Krognytt.setIndicator(getIndicator("Map",
				android.R.drawable.ic_dialog_map,
				Integer.parseInt("2E2E2E", 16)));
		Intent newsIntent = new Intent(this, NewsFeedGroup.class);
		Krognytt.setContent(newsIntent);

		// Map Activity
		treat = mHost.newTabSpec("Map");
		treat.setIndicator(getIndicator("Map",
				android.R.drawable.ic_dialog_map,
				Integer.parseInt("2E2E2E", 16)));
		Intent treatIntent = new Intent(this, MapGroup.class);
		treat.setContent(treatIntent);

		// Restaurantlist Activity
		scanner = mHost.newTabSpec("List");
		scanner.setIndicator(getIndicator("List",
				android.R.drawable.ic_menu_agenda,
				Integer.parseInt("2E2E2E", 16)));
		Intent scannerIntent = new Intent(this, ListGroup.class);
		scanner.setContent(scannerIntent);

		// Favorite Activity
		vouchers = mHost.newTabSpec("Favourites");
		vouchers.setIndicator(getIndicator("Favourites",
				android.R.drawable.btn_star, Integer.parseInt("2E2E2E", 16)));
		Intent voucherIntent = new Intent(this, FavouritesGroup.class);
		vouchers.setContent(voucherIntent);

		// Store Activity
		settings = mHost.newTabSpec("Store");
		settings.setIndicator(getIndicator("Store",
				android.R.drawable.ic_menu_send, Integer.parseInt("000000", 16)));
		Intent settingsIntent = new Intent(this, SettingsGroup.class);
		settings.setContent(settingsIntent);

		// Settings Activity
		notifications = mHost.newTabSpec("notification");
		notifications.setIndicator(getIndicator("notification",
				android.R.drawable.ic_menu_send, Integer.parseInt("000000", 16)));
		Intent notificationIntent = new Intent(this, NotificationGroup.class);
		notifications.setContent(notificationIntent);

		mHost.getTabWidget().setStripEnabled(false);

		// Add Required Stuff
		mHost.addTab(Krognytt);
		mHost.addTab(treat);
		mHost.addTab(scanner);
		// mHost.addTab(cart);
		mHost.addTab(vouchers);
		mHost.addTab(settings);
		mHost.addTab(notifications);
		mHost.getTabWidget().setVisibility(View.GONE);

		// load slide menu items
		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
		// navMenuSubTitles =
		// getResources().getStringArray(R.array.nav_drawer_sub_items);

		// nav drawer icons from resources
		navMenuIcons = getResources()
				.obtainTypedArray(R.array.nav_drawer_icons);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ExpandableListView) findViewById(R.id.list_slidermenu);

		navDrawerItems = new ArrayList<NavDrawerItem>();

		// adding nav drawer items to array
		// NewsFeed
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons
				.getResourceId(0, -1)));
		// Home
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons
				.getResourceId(1, -1)));
		// Find People
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons
				.getResourceId(2, -1)));
		// Photos
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons
				.getResourceId(3, -1)));
		// Communities, Will add a counter here
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons
				.getResourceId(4, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons
				.getResourceId(5, -1)));
		createGroupList();
		createChildList();
		// Recycle the typed array
		navMenuIcons.recycle();
		// setting the nav drawer list adapter
		adapter = new NavDrawerListAdapter(getApplicationContext(), items,
				MenuCollection, navDrawerItems);
		mDrawerList.setAdapter(adapter);
		mDrawerList.setOnChildClickListener(this);
		mDrawerList.setOnGroupClickListener(this);
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_launcher, // nav menu toggle icon
				R.string.app_name, // nav drawer open - description for
									// accessibility
				R.string.app_name // nav drawer close - description for
									// accessibility
		) {
			public void onDrawerClosed(View view) {
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				invalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		setDrawer();
		if (savedInstanceState == null) {
			// on first time display view for first nav item
			displayView(0, 0);
		}
		stpe = Executors.newScheduledThreadPool(5);
		stpe.scheduleAtFixedRate(new Runnable() {

			@Override
			public void run() {
				if (NetworkChecker.isNetworkOnline(getApplicationContext())) {
					for (BasicInfoBean temp : transerObj.readData()) {
						MyDebug.appendLog("HOME : onstepup exetuce ");
						postData(temp);
					}
				}
				try {
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}, 5, 10, TimeUnit.MINUTES);
		try {
			frompushIntent = getIntent().getBooleanExtra("PUSH_NOTIFICATION",
					false);
		} catch (Exception e) {

		}
		if (frompushIntent)
			Home.setTab(4);
	}

	private long timestamp = 0;
	TransferToServer transerObj;

	/**
	 * To post the data to the server.
	 * 
	 * @param tempObj
	 *            information object
	 */
	public void postData(BasicInfoBean tempObj) {
		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(DataEngine.BASE_URL
				+ DataEngine.APP_USERS);
		try {
			// Add your data
			timestamp = System.currentTimeMillis();
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("content-type",
					"application/json"));
			nameValuePairs.add(new BasicNameValuePair("email", UserEmailFetcher
					.getEmail(Home.this)));
			nameValuePairs.add(new BasicNameValuePair("deviceId",
					UserEmailFetcher.getDeviceId(Home.this)));
			nameValuePairs.add(new BasicNameValuePair("guideId", tempObj
					.getGuideId()));
			nameValuePairs.add(new BasicNameValuePair("year", tempObj
					.getGuideYear()));
			nameValuePairs.add(new BasicNameValuePair("timeStamp", String
					.valueOf(timestamp)));
			nameValuePairs.add(new BasicNameValuePair("paymentToken", tempObj
					.getPaymentInfo()));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);
			try {
				String finalReponse = EntityUtils
						.toString(response.getEntity());
				MyDebug.appendLog(finalReponse);
				if (finalReponse.contains("saved")) {
					transerObj.deleteTreat(tempObj.getGuideId(),
							tempObj.getGuideYear());
				}
			} catch (Exception e) {
				MyDebug.appendLog(e.getLocalizedMessage());
			}

		} catch (ClientProtocolException e) {
			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	/**
	 * Creats list of item to be displayed in menu
	 */
	private void createGroupList() {
		items = new ArrayList<String>();
		items.add(getString(R.string.newsfeed));
		items.add(getString(R.string.map));
		items.add(getString(R.string.list));
		items.add(getString(R.string.favourites));
		items.add(getString(R.string.store));
		items.add(getString(R.string.settings));
	}

	/**
	 * Creats subchild of list of items in menu
	 */
	private void createChildList() {
		info_obj = new InitialInformation(Home.this);
		ArrayList<String> newsitems = new ArrayList<String>();
		ArrayList<String> mapItems = new ArrayList<String>();
		ArrayList<String> listaItems = new ArrayList<String>();
		ArrayList<String> favouriteItems = new ArrayList<String>();
		ArrayList<String> storeItems = new ArrayList<String>();
		ArrayList<String> notificationItems=new ArrayList<String>();

		try {
			for (BasicInfoBean beanObj : info_obj.readData()) {
				listaItems.add(beanObj.getGuideName());
			}
		} catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}

		MenuCollection = new LinkedHashMap<String, List<String>>();
		for (String laptop : items) {
			if (laptop.equals(getString(R.string.list))) {
				loadChild(listaItems);
			} else if (laptop.equals(getString(R.string.newsfeed)))
				loadChild(newsitems);
			else if (laptop.equals(getString(R.string.map)))
				loadChild(mapItems);
			else if (laptop.equals(getString(R.string.favourites)))
				loadChild(favouriteItems);
			else if (laptop.equals(getString(R.string.store)))
				loadChild(storeItems);
			else
				loadChild(notificationItems);

			MenuCollection.put(laptop, childList);
		}
	}

	/**
	 * Loads the subitems into the list.
	 * 
	 * @param lapModels
	 */
	private void loadChild(ArrayList<String> lapModels) {
		childList = new ArrayList<String>();
		for (String model : lapModels)
			childList.add(model);
	}

	@Override
	protected void onDestroy() {
		if (mService != null) {
			unbindService(mServiceConn);
		}
		try {
			stpe.shutdown();
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onDestroy();
	}

	@Override
	public void onTabChanged(String tabId) {
		try {
			for (int i = 0; i < mHost.getTabWidget().getChildCount(); i++) {
				TextView tv = ((TextView) ((RelativeLayout) mHost
						.getTabWidget().getChildAt(i)).getChildAt(1));
				if (tv.getText().toString().equalsIgnoreCase(tabId)) {
					tv.setTextColor(Color.parseColor("#c4af89"));
				} else {
					tv.setTextColor(Color.WHITE);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/* *
	 * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	/**
	 * Diplaying fragment view for selected nav drawer list item
	 * 
	 * @param childPosition
	 * */
	@SuppressWarnings("unused")
	private void displayView(int groupposition, int childPosition) {
		// update the main content by replacing fragments
		Fragment fragment = null;
		switch (groupposition) {
		case 0:
			Home.setTab(0);
			mDrawerLayout.closeDrawer(mDrawerList);
			break;

		case 1:
			Home.setTab(1);
			mDrawerLayout.closeDrawer(mDrawerList);
			break;
		case 2:
			switch (childPosition) {

			case 1:
				Home.setTab(2);
				mDrawerLayout.closeDrawer(mDrawerList);
				break;
			}
		case 3:
			Home.setTab(3);
			mDrawerLayout.closeDrawer(mDrawerList);
			break;
		case 4:
			Home.setTab(4);
			mDrawerLayout.closeDrawer(mDrawerList);
			break;
		case 5:
			Home.setTab(5);
			mDrawerLayout.closeDrawer(mDrawerList);
			break;
		}

		if (fragment != null) {
			// update selected item and title, then close the drawer
			mDrawerList.setItemChecked(groupposition, true);
			mDrawerList.setSelection(groupposition);
			setTitle(navMenuTitles[groupposition]);
			mDrawerLayout.closeDrawer(mDrawerList);
		} else {
			// error in creating fragment
			MyDebug.appendLog("MainActivity Error in creating fragment");
		}
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onChildClick(ExpandableListView parent, View v,
			int groupPosition, int childPosition, long id) {
		// displayView(groupPosition,childPosition);
		// adapter.notifyDataSetChanged();
		int index = parent.getFlatListPosition(ExpandableListView
				.getPackedPositionForChild(groupPosition, childPosition));
		parent.setItemChecked(index, true);
		parent.collapseGroup(groupPosition);
		// parent.getChildAt(childPosition).g
		try {
			Home.setBeanObj(info_obj.readData().get(childPosition));
		} catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}
		Home.setTab(2);
		((ListGroup) getLocalActivityManager().getActivity(scanner.getTag()))
				.doHack();
		Log.i("TEST", ">>>>");
		mDrawerLayout.closeDrawer(mDrawerList);
		return true;
	}

	@Override
	public void onBackPressed() {
		set_alertdialog(Home.this);
	}

	/**
	 * Alert is shown to close the app or not
	 */
	private void set_alertdialog(Context mtx) {
		AlertDialog.Builder builder = new AlertDialog.Builder(mtx);
		builder.setMessage(getString(R.string.doyouwant_close));
		builder.setPositiveButton(getString(R.string.ok),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				});
		builder.setNegativeButton(getString(R.string.cancel),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		builder.show();
	}

	@Override
	public boolean onGroupClick(ExpandableListView arg0, View v,
			int groupPosition, long id) {
		switch (groupPosition) {
		case 0:
			Home.setTab(0);
			mDrawerLayout.closeDrawer(mDrawerList);
			break;
		case 1:
			Home.setTab(1);
			mDrawerLayout.closeDrawer(mDrawerList);
			break;
		case 2:
			if (DataEngine.guidePurchased) {
				MyDebug.appendLog("Purchase successful. Provisioning."
						+ DataEngine.guidePurchased);
				createChildList();
				DataEngine.guidePurchased = false;
				adapter = new NavDrawerListAdapter(getApplicationContext(),
						items, MenuCollection, navDrawerItems);
				mDrawerList.setAdapter(adapter);
			}
			break;
		case 3:
			Home.setTab(3);
			mDrawerLayout.closeDrawer(mDrawerList);
			((FavouritesGroup) getLocalActivityManager().getActivity(
					vouchers.getTag())).doHack();
			break;
		case 4:
			Home.setTab(4);
			mDrawerLayout.closeDrawer(mDrawerList);
			break;
		case 5:
			Home.setTab(5);
			mDrawerLayout.closeDrawer(mDrawerList);
			break;
		}
		return false;
	}

	public static void setBeanObj(BasicInfoBean infoBeanObj) {
		beanObj = infoBeanObj;
	}

	public static void closeDrawer() {
		mDrawerLayout.closeDrawer(Gravity.START);
		mDrawerLayout.setFocusable(false);
	}

}
