package com.se.whiteguide;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.live.se.whiteguide.R;
import com.se.whiteguide.dataengine.DataEngine;
import com.se.whiteguide.dataengine.DataHandler;
import com.se.whiteguide.dataengine.FavouritesHandler;
import com.se.whiteguide.dataengine.InitialInformation;
import com.se.whiteguide.dataengine.PaymentInformation;
import com.se.whiteguide.dataengine.Restaurant;
import com.se.whiteguide.helpers.BasicInfoBean;
import com.se.whiteguide.log.MyDebug;
import com.se.whiteguide.parsers.RestaurantsParser;
import com.se.whiteguide.renders.utl.MultiDrawable;
import com.se.whiteguide.screens.DetailedRestaurantview;
import com.se.whiteguide.tabgroup.Home;
import com.se.whiteguide.tabgroup.MapGroup;
import com.se.whiteguide.util.MyItem;

public class MapFragmentCluster extends FragmentActivity implements
		LocationListener, ClusterManager.OnClusterClickListener<MyItem>,
		ClusterManager.OnClusterItemClickListener<MyItem>,
		ClusterManager.OnClusterInfoWindowClickListener<MyItem>,
		ClusterManager.OnClusterItemInfoWindowClickListener<MyItem> {

	LocationManager locationManager;
	String provider;
	Location location;
	ProgressDialog progress;
	private GoogleMap googleMap;
	private ClusterManager<MyItem> mClusterManager;

	public static final String TAG = "KARTA";
	AdView adView;
	RestaurantsParser parser;
	Vector<Double> lat, lon;
	Vector<Integer> Id;
	Vector<Restaurant> restaurants;
	FavouritesHandler treatHandler;
	DataHandler dataHandler;
	InitialInformation information;
	PaymentInformation payInfo;
	Bitmap marker_bitmap, cof_bitmap, Nytestat_bitmap;
	int height, width;

	double lattitude, longitude;
	SharedPreferences settings;

	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		lat = new Vector<Double>();
		lon = new Vector<Double>();
		Id = new Vector<Integer>();

		progress = new ProgressDialog(this.getParent());
		progress.setCancelable(true);
		progress.setMessage(getString(R.string.loading));
		information = new InitialInformation(getApplicationContext());
		payInfo = new PaymentInformation(getApplicationContext());

		restaurants = new Vector<Restaurant>();
		setContentView(R.layout.karta);
		settings = getSharedPreferences(DataEngine.SETTINGS_NAME, MODE_PRIVATE);
		Editor editor = settings.edit();
		editor.putString(DataEngine.SETTINGS_MAPRESUME, "DONTSHOW");
		editor.commit();
		adView = (AdView) findViewById(R.id.adView);
		String name = "Restaurang";
		try {
			name = new InitialInformation(this).readData().get(0)
					.getGuideName();
		} catch (Exception e) {
			e.printStackTrace();
		}
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

		// Height & width of the Screen.
		height = displaymetrics.heightPixels;
		width = displaymetrics.widthPixels;
		dataHandler = new DataHandler(this.getParent(), name);
		// Get the location manager
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		if (locationManager.getProviders(true).size() > 0) {
			for (int i = 0; i < locationManager.getAllProviders().size(); i++) {
				provider = locationManager.getAllProviders().get(i);
				location = locationManager.getLastKnownLocation(provider);
				if (null == location) {
					locationManager
							.requestLocationUpdates(provider, 0, 0, this);
				} else {
					break;
				}
			}
		} else {
			// showAlert();
			return;
		}

		// Initialize the location fields
		if (location != null) {
			onLocationChanged(location);
		}
		new BackGround().execute();
	}

	@Override
	protected void onResume() {
		String refresh = settings
				.getString(DataEngine.SETTINGS_MAPRESUME, null);
		if (refresh != null && refresh.equalsIgnoreCase("SHOW")) {
			new BackGround().execute();
		}

		super.onResume();
	}

	@Override
	public void onLocationChanged(Location location) {
		this.location = location;
		if (null != location) {
			BackGround backGround = new BackGround();
			backGround.execute();
			try {
				locationManager.removeUpdates(this);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {

	}

	@Override
	public void onProviderEnabled(String provider) {

	}

	@Override
	public void onProviderDisabled(String provider) {

	}

	/**
	 * Intialize map and addidng latitude and longitude of resatuarant/cafe from
	 * server
	 */
	class BackGround extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (null != progress) {
				if (!progress.isShowing())
					progress.show();
			}
		}

		@Override
		protected Void doInBackground(Void... params) {
			dotheRequired();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			initilizeMap();
			if (null != progress) {
				if (progress.isShowing())
					progress.dismiss();
			}
		}
	}

	/**
	 * function to load map If map is not created it will create it for you
	 * */
	@SuppressLint("NewApi")
	private void initilizeMap() {
		if (googleMap == null) {
			AdRequest adRequest = new AdRequest.Builder().build();
			// Start loading the ad in the background.
			if (DataEngine.showAds)
				adView.loadAd(adRequest);
			else
				adView.setVisibility(View.GONE);

			googleMap = ((SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.map)).getMap();
		}
		// check if map is created successfully or not
		if (googleMap == null) {

		} else {
			// googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
			// Showing / hiding your current location
			if (null != location) {
				googleMap.setMyLocationEnabled(true);
			}

			googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
					lattitude, longitude), 10));

			// Initialize the manager with the context and the map.
			// (Activity extends context, so we can pass 'this' in the
			// constructor.)
			mClusterManager = new ClusterManager<MyItem>(this, googleMap);
			mClusterManager.setRenderer(new PersonRenderer());
			googleMap.setOnCameraChangeListener(mClusterManager);
			googleMap.setOnMarkerClickListener(mClusterManager);
			googleMap.setOnInfoWindowClickListener(mClusterManager);
			mClusterManager.setOnClusterClickListener(this);
			mClusterManager.setOnClusterInfoWindowClickListener(this);
			mClusterManager.setOnClusterItemClickListener(this);
			mClusterManager.setOnClusterItemInfoWindowClickListener(this);

			// Point the map's listeners at the listeners implemented by the
			// cluster
			// manager.

			for (int i = 0; i < lat.size(); i++) {
				if (Id.get(i).equals(58)) {
					try {
						if (lat.get(i) != 0.0 && lon.get(i) != 0.0) {
							MyItem offsetItem = new MyItem(new LatLng(
									lat.get(i), lon.get(i)), restaurants.get(i)
									.getTitle().replace("#", ""),
									R.drawable.marker);
							mClusterManager.addItem(offsetItem);
						}
					} catch (OutOfMemoryError e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}

				}

				else if (Id.get(i).equals(60)) {
					try {
						if (lat.get(i) != 0.0 && lon.get(i) != 0.0) {
							MyItem offsetItem = new MyItem(new LatLng(
									lat.get(i), lon.get(i)), restaurants.get(i)
									.getTitle().replace("#", ""),
									R.drawable.marker2);
							mClusterManager.addItem(offsetItem);
						}
					} catch (OutOfMemoryError e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}

				} else if (Id.get(i).equals(90)) {
					try {
						for (BasicInfoBean beanTemp : information.readData()) {
							if (beanTemp.isGuideisBought()
									&& beanTemp.isNytestat_enabled()
									&& lat.get(i) != 0.0 && lon.get(i) != 0.0) {
								MyItem offsetItem = new MyItem(new LatLng(
										lat.get(i), lon.get(i)), restaurants
										.get(i).getTitle().replace("#", ""),
										R.drawable.newres_marker);
								mClusterManager.addItem(offsetItem);
								break;
							}
						}

					} catch (OutOfMemoryError e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}

				}

				else if (Id.get(i).equals(59)) {
					try {
						if (lat.get(i) != 0.0 && lon.get(i) != 0.0) {
							MyItem offsetItem = new MyItem(new LatLng(
									lat.get(i), lon.get(i)), restaurants.get(i)
									.getTitle().replace("#", ""),
									R.drawable.marker_bar);
							mClusterManager.addItem(offsetItem);
						}
					} catch (OutOfMemoryError e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			}

			mClusterManager.cluster();
		}

	}

	/**
	 * Draws profile photos inside markers (using IconGenerator). When there are
	 * multiple people in the cluster, draw multiple photos (using
	 * MultiDrawable).
	 */
	private class PersonRenderer extends DefaultClusterRenderer<MyItem> {
		private final IconGenerator mIconGenerator = new IconGenerator(
				getApplicationContext());
		private final IconGenerator mClusterIconGenerator = new IconGenerator(
				getApplicationContext());
		private final ImageView mImageView;
		private final ImageView mClusterImageView;
		private final int mDimension;
		ShapeDrawable mColoredCircleBackground;
		private float mDensity;

		public PersonRenderer() {
			super(getApplicationContext(), googleMap, mClusterManager);

			View multiProfile = getLayoutInflater().inflate(
					R.layout.multi_profile, null);
			mClusterIconGenerator.setContentView(multiProfile);
			mClusterImageView = (ImageView) multiProfile
					.findViewById(R.id.image);
			mDensity = getResources().getDisplayMetrics().density;
			mImageView = new ImageView(getApplicationContext());
			mDimension = (int) getResources().getDimension(
					R.dimen.custom_profile_image);
			mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension,
					mDimension));
			int padding = (int) getResources().getDimension(
					R.dimen.custom_profile_padding);
			mImageView.setPadding(padding, padding, padding, padding);
			mIconGenerator.setContentView(mImageView);
		}

		@Override
		protected void onBeforeClusterItemRendered(MyItem item,
				MarkerOptions markerOptions) {
			// Draw a single person.
			// Set the info window to show their name.
			mImageView.setImageResource(item.profilePhoto);
			Bitmap icon = mIconGenerator.makeIcon();
			markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title(
					item.name);
		}

		@Override
		protected void onBeforeClusterRendered(Cluster<MyItem> cluster,
				MarkerOptions markerOptions) {
			// Draw multiple people.
			// Note: this method runs on the UI thread. Don't spend too much
			// time in here (like in this example).
			List<Drawable> profilePhotos = new ArrayList<Drawable>(Math.min(1,
					cluster.getSize()));
			int width = mDimension;
			int height = mDimension;

			for (MyItem p : cluster.getItems()) {
				// Draw 4 at most.
				if (profilePhotos.size() == 1)
					break;
				Drawable drawable = getResources().getDrawable(p.profilePhoto);
				drawable.setBounds(0, 0, width, height);
				profilePhotos.add(drawable);
			}
			MultiDrawable multiDrawable = new MultiDrawable(profilePhotos);
			multiDrawable.setBounds(0, 0, width, height);

			// mClusterImageView.setImageDrawable(multiDrawable);
			// mClusterImageView.setBackgroundResource(R.drawable.appicon);
			mClusterImageView.setBackground(makeClusterBackground());
			Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster
					.getSize()));
			markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
		}

		@Override
		protected boolean shouldRenderAsCluster(Cluster cluster) {
			// Always render clusters.
			return cluster.getSize() > 1;
		}

		private LayerDrawable makeClusterBackground() {
			mColoredCircleBackground = new ShapeDrawable(new OvalShape());
			mColoredCircleBackground.getPaint().setColor(Color.WHITE);
			ShapeDrawable outline = new ShapeDrawable(new OvalShape());
			outline.getPaint().setColor(Color.BLACK); // Transparent white.
			LayerDrawable background = new LayerDrawable(new Drawable[] {
					outline, mColoredCircleBackground });
			int strokeWidth = (int) (mDensity * 2);
			background.setLayerInset(1, strokeWidth, strokeWidth, strokeWidth,
					strokeWidth);
			return background;
		}
	}

	public void CallToDisplayMenuItem(View v) {
		Home.setDrawer();
	}

	/**
	 * used to call next class when we click info window
	 */
	private void handleThisCall(final BasicInfoBean dummy,
			final Restaurant tempObj) {
		Home.closeDrawer();

		new Handler().post(new Runnable() {

			@Override
			public void run() {
				Intent nextActivity = new Intent(MapFragmentCluster.this,
						DetailedRestaurantview.class);
				nextActivity.putExtra("RES", tempObj);
				nextActivity.putExtra("GUIDE", dummy);
				MapGroup parentActivity = (MapGroup) getParent();
				parentActivity.startActivity(nextActivity);
			}
		});

	}

	/**
	 * get latitude and longitude
	 */
	public void dotheRequired() {

		// location = null;
		if (location == null) {
			lattitude = 59.32;
			longitude = 18.06;
		} else {
			lattitude = location.getLatitude();
			longitude = location.getLongitude();
		}
		restaurants = new Vector<Restaurant>();
		lat = new Vector<Double>();
		lon = new Vector<Double>();
		Id = new Vector<Integer>();
		String name = "Restaurang";
		try {
			name = new InitialInformation(this).readData().get(0)
					.getGuideName();
		} catch (Exception e) {
			e.printStackTrace();
		}
		int count = 0;
		try {
			count = information.readData().size();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (count > 0) {
			try {
				for (BasicInfoBean beanTemp : information.readData()) {

					dataHandler = new DataHandler(this.getParent(),
							beanTemp.getGuideName());

					for (Restaurant restaurant : dataHandler.readRestaurant()) {
						restaurants.add(restaurant);
						lat.add(restaurant.getLat());
						lon.add(restaurant.getLon());
						Id.add(Integer.parseInt(restaurant.getGuideId()));
					}
				}
			} catch (Exception e) {
				MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
			}

		} else {
			dataHandler = new DataHandler(this.getParent(), name);
			try {
				for (Restaurant restaurant : dataHandler.readRestaurant()) {

					restaurants.add(restaurant);
					lat.add(restaurant.getLat());
					lon.add(restaurant.getLon());
					Id.add(Integer.parseInt(restaurant.getGuideId()));

				}
			} catch (Exception e) {
				MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
			}
		}
	}

	@Override
	public boolean onClusterClick(Cluster<MyItem> cluster) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onClusterItemInfoWindowClick(MyItem item) {
		// TODO Auto-generated method stub
		boolean is_purchased = false;
		String rest_title = "";
		LatLng lal = item.mPosition;

		int position = lat.indexOf(lal.latitude);
		Restaurant tempObj = null;
		if (position >= 0) {
			try {
				tempObj = restaurants.get((lat.indexOf(lal.latitude)));
			} catch (Exception e) {

			}
		}
		if (null == tempObj) {
			try {
				for (Restaurant restObj : restaurants) {
					rest_title = restObj.getTitle().replace("#", "");
					if (item.getname().trim().equalsIgnoreCase(rest_title)) {
						tempObj = restObj;
						break;
					}
				}
			} catch (Exception e) {

			}
		}
		if (null != tempObj) {
			BasicInfoBean dummy = null;
			try {
				for (BasicInfoBean beanobj : information.readData()) {
					if (tempObj.getGuideId().equalsIgnoreCase(
							beanobj.getGuideId())
							&& tempObj.getGuideYear().equalsIgnoreCase(
									beanobj.getGuideYear())) {
						if (beanobj.isGuideisBought()) {
							is_purchased = true;
						}
						dummy = beanobj;
						break;
					}
				}
				if (!is_purchased) {
					is_purchased = payInfo.isThisGuidebought(tempObj.getId(),
							tempObj.getGuideYear());
				}
				handleThisCall(dummy, tempObj);
			} catch (Exception e) {
				MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
			}

		}

	}

	@Override
	public void onClusterInfoWindowClick(Cluster<MyItem> cluster) {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean onClusterItemClick(MyItem item) {
		// TODO Auto-generated method stub
		return false;
	}

}
