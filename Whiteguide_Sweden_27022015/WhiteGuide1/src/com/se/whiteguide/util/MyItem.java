package com.se.whiteguide.util;

import android.graphics.Bitmap;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class MyItem implements ClusterItem {
	public final LatLng mPosition;
	public final String name;
	public final int profilePhoto;

	public MyItem(LatLng pos,String name,int pictureResource) {
		//mPosition = new LatLng(lat, lng);
		this.mPosition=pos;
		this.name=name;
		this.profilePhoto=pictureResource;
	}
	
	/*public MyItem(LatLng position, String name, int pictureResource){
		
	}*/

	@Override
	public LatLng getPosition() {
		// TODO Auto-generated method stub
		return mPosition;
	}
	
	public String getname(){
		return name;
	}

}
