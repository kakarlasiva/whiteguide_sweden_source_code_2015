package com.se.whiteguide.util;

import android.text.TextPaint;
import android.text.style.URLSpan;

public class RemoveUnderline extends URLSpan {

	public RemoveUnderline(String url) {
		super(url);
		// TODO Auto-generated constructor stub
	}
	  public void updateDrawState(TextPaint p_DrawState) {
	        super.updateDrawState(p_DrawState);
	        p_DrawState.setUnderlineText(false);
	    }

}
