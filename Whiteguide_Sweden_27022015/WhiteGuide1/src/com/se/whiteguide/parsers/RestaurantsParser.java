package com.se.whiteguide.parsers;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.se.whiteguide.dataengine.DataHandler;
import com.se.whiteguide.dataengine.Restaurant;
import com.se.whiteguide.dataengine.TagConstants;
import com.se.whiteguide.log.MyDebug;



/**
 *  Represents json parsing of restaurant/cafe
 *  @author Conevo
 */
public class RestaurantsParser {
    Context cntxt;

	// Actual URL gives the data.
	String parse_url;

	// To catch the characters string{event triggered by sax}
	String str;

	// stream corresponding to the given url.
	InputStream in;

	// Restaurant information holding objects
	Restaurant restaurant_obj;

	// To store all informations and write to file..
	Vector<Restaurant> restaurant_list;

	private DataHandler dataHandler;
	String guideYear, guideId;
	private static String TAG="RestaurantsParser";

	public RestaurantsParser(Context context, String url, String guidename,
			String id, String year) {
		this.cntxt = context;
		this.parse_url = url;
		this.guideId = id;
		this.guideYear = year;
		dataHandler = new DataHandler(context, guidename);
		restaurant_list = new Vector<Restaurant>();
	}

	/**
	 *  convert InputStream to String
	 */
	private static String getStringFromInputStream(InputStream is) {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {
			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return sb.toString();

	}

	public Vector<Restaurant> getData() {
		return restaurant_list;
	}

	public void doJsonparse(){
		HttpURLConnection http = null;
		URL url = null;
		Log.e("BASIC", "The url here is" + parse_url);

		try {
			url = new URL(parse_url);
			http = (HttpURLConnection) url.openConnection();
			in = new BufferedInputStream(http.getInputStream());
		} catch (Exception e1) {
			MyDebug.log_info(TAG, "Parse Exception", e1.getMessage());
		}

		
		String data = getStringFromInputStream(in);
		String jsonResponse = "{ \"items\" : " + data + "}";
		if (null != data && !"".equalsIgnoreCase(data)) {
			int count = dataHandler.readRestaurant().size();
			try {
				JSONObject json = new JSONObject(jsonResponse);
				JSONArray restArray = json.getJSONArray("items");
				for (int i = 0; i < restArray.length(); i++) {
					restaurant_obj = new Restaurant();
					JSONObject obj = restArray.getJSONObject(i);
					restaurant_obj.setTitle(obj.optString(TagConstants.NAME)
							.trim());
					if (TextUtils.isDigitsOnly(restaurant_obj.getTitle()
							.subSequence(0, 1))) {
						restaurant_obj.setTitle("#"
								+ obj.optString(TagConstants.NAME).trim());
					}
					restaurant_obj.setGuideId(guideId);
					restaurant_obj.setGuideYear(guideYear);
					
					restaurant_obj.setId(obj.optString(TagConstants.ID).trim());
					restaurant_obj.setChanged(obj.optString(
							TagConstants.CHANGED).trim());
					restaurant_obj.setCity(obj.optString(TagConstants.CITY)
							.trim());
					restaurant_obj.setPosition(obj
							.optString(TagConstants.COORDINATES));
					restaurant_obj.setRestaurantClass(obj
							.optString(TagConstants.CLASSIFICATION));
					restaurant_obj.setScore(obj.optString(TagConstants.SCORE));
					restaurant_list.add(restaurant_obj);
					if (count > 0)
						dataHandler.updateRestaurant(restaurant_obj.getId(),
								restaurant_obj);
				}
				if (count < 0) {
					dataHandler.writeAll(restaurant_list);
					dataHandler.writetoTemp(restaurant_list);
				} else {
					if (restaurant_list.size() > 0)
						dataHandler.updateAll(restaurant_list);
				}
			} catch (Exception e) {
				MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
			}
		}
	}
}
