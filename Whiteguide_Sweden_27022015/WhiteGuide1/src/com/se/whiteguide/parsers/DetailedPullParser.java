package com.se.whiteguide.parsers;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.se.whiteguide.helpers.Detailedpullhelper;
import com.se.whiteguide.log.MyDebug;

@SuppressWarnings("serial")
public class DetailedPullParser implements Serializable {
	Context mtx;
	String parse_url;
	public List<Detailedpullhelper> fullnews_data;
	Detailedpullhelper helper_obj;
	private static String TAG="DetailedPullParser";

	public DetailedPullParser(Context mtx) {
		this.mtx = mtx;
		fullnews_data = new ArrayList<Detailedpullhelper>();
	
	}

	// Method that will parse the JSON file and will return a JSONObject
	public JSONObject parseJSONData() {
		String JSONString = null;
		JSONObject JSONObject = null;
		try {

			// open the inputStream to the file
			InputStream inputStream = mtx.getAssets().open(parse_url);

			int sizeOfJSONFile = inputStream.available();

			// array that will store all the data
			byte[] bytes = new byte[sizeOfJSONFile];

			// reading data into the array from the file
			inputStream.read(bytes);

			// close the input stream
			inputStream.close();

			JSONString = new String(bytes, "UTF-8");
			JSONObject = new JSONObject(JSONString);

		} catch (IOException ex) {
			MyDebug.log_info(TAG, "Parse Exception", ex.getMessage());
		} catch (JSONException x) {
			MyDebug.log_info(TAG, "Parse Exception", x.getMessage());
		}
		return JSONObject;
	}

	InputStream in;
	String json;
	JSONArray jobj, jmenus;

	public JSONArray getjsonfromurl(String menu_url){

		HttpURLConnection http = null;
		URL url = null;
		try {
			url = new URL(menu_url);
		} catch (MalformedURLException e1) {
			MyDebug.log_info(TAG, "Parse Exception", e1.getMessage());
		}
		if (url.getProtocol().toLowerCase().equals("https")) {

			HttpsURLConnection https = null;
			try {
				https = (HttpsURLConnection) url.openConnection();
			} catch (IOException e) {
				MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
			}

			http = https;
		} else {
			try {
				http = (HttpURLConnection) url.openConnection();
			} catch (IOException e) {
				MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
			}
		}
		
		// in=ctx.getResources().openRawResource(R.raw.menuview);
		/*
		 * try { in=mtx.getAssets().open(menu_url); } catch (IOException e1) {
		 * // TODO Auto-generated catch block e1.printStackTrace(); }
		 */
		try {
			in = new BufferedInputStream(http.getInputStream());
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					in, "ISO-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			in.close();
			json = sb.toString();
		} catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}

		// try parse the string to a JSON object
		/*
		 * try { if (null != json) jobj = new JSONArray(json); } catch
		 * (JSONException e) { Log.e("JSON Parser", "Error parsing data " +
		 * e.toString()); }
		 */
		try {
			if (null != json)
				jobj = new JSONArray(json);
		} catch (JSONException e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}

		return jobj;

	}

	public void getNewsList(String url)  {
		
		try {
			jmenus = getjsonfromurl(url);
			for(int i=0;i<jmenus.length();i++){
				helper_obj = new Detailedpullhelper();
		    	JSONObject news_obj = jmenus.getJSONObject(i);
		    	String news_id = news_obj.getString("id");
		    	helper_obj.setId(news_id);
		    	String news_title = news_obj.getString("title");
		    	helper_obj.setTitle(news_title);
		    	String news_smalldesc = news_obj.getString("description");
		    	helper_obj.setDesc(news_smalldesc);
		    	String news_fulldesc = news_obj.getString("fulldescription");
		    	helper_obj.setFull_des(news_fulldesc);
		    	String news_itemurl = news_obj.getString("itemurl");
		    	helper_obj.setItemurl(news_itemurl);
		    	String news_imageurl = news_obj.getString("imageurl");
		    	helper_obj.setImageurl(news_imageurl);
		    	String news_date = news_obj.getString("pubdate");
		    	helper_obj.setPubdate(news_date);
		    	fullnews_data.add(helper_obj);
			}
			
		} catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}
	}

}
