package com.se.whiteguide.parsers;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Vector;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.json.JSONArray;
import org.json.JSONObject;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.content.Context;
import android.util.Log;

import com.live.se.whiteguide.R;
import com.se.whiteguide.dataengine.DataEngine;
import com.se.whiteguide.dataengine.InitialInformation;
import com.se.whiteguide.dataengine.TagConstants;
import com.se.whiteguide.helpers.BasicInfoBean;
import com.se.whiteguide.helpers.UserEmailFetcher;
import com.se.whiteguide.log.MyDebug;

/**
 * Represents json parsing of basic info
 * 
 * @author Conevo
 */
public class BasicDataInfo {
	private static final String TAG = "BASIC";
	BasicInfoBean beanObj;
	Context context;
	// To hold temporary data(in case of data has some special characters
	// \r/\n..etc)
	private StringBuilder builder;

	// helper to the builder.
	private String temp;

	private InitialInformation info_Handler;

	// To Write to file..
	Vector<BasicInfoBean> beanObjList;

	HttpURLConnection http = null;
	URL url = null;

	public BasicDataInfo(Context cntxt) {
		context = cntxt;
		info_Handler = new InitialInformation(context);
		beanObjList = new Vector<BasicInfoBean>();
	}

	public void doParsing() {

		try {
			url = new URL(DataEngine.BASE_URL + DataEngine.APP_USERS
					+ "?email=" + UserEmailFetcher.getEmail(context)
					+ "&deviceId=" + UserEmailFetcher.getDeviceId(context));
			Log.e(TAG, "URL : " + url);
		} catch (Exception e1) {
			MyDebug.log_info(TAG, "Parse Exception", e1.getMessage());
		}
		InputStream in = null;
		try {
			http = (HttpURLConnection) url.openConnection();
			in = new BufferedInputStream(http.getInputStream());
		} catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}

		MyDebug.log_info(TAG, "Inputstream : ", String.valueOf(in));
		String jsonResponse = getStringFromInputStream(in);
		try {
			JSONObject json = new JSONObject(jsonResponse);
			JSONArray restArray = json.getJSONArray("guides");

			for (int i = 0; i < restArray.length(); i++) {
				beanObj = new BasicInfoBean();
				JSONObject obj = restArray.getJSONObject(i);
				beanObj.setGuideId(obj.getString(TagConstants.GUIDE_ID));
				temp = obj.getString(TagConstants.GUIDE_PURCHASE_INFO);
				if (temp.equalsIgnoreCase("true") || temp.equalsIgnoreCase("1")) {
					beanObj.setGuideisBought(true);
					DataEngine.showAds = false;
				} else {
					beanObj.setGuideisBought(false);
				}
				beanObj.setGuideName(obj.getString(TagConstants.GUIDE_NAME));
				beanObj.setGuideYear(obj.getString(TagConstants.GUIDE_YEAR));
				beanObj.setGuidePrice(obj.getString(TagConstants.GUIDE_PRICE));
				beanObj.setGuideActiveDates(obj
						.getString(TagConstants.GUIDE_ACTIVEDATES));
				beanObj.setDisplayguidename(obj
						.getString(TagConstants.DISPLAY_GUIDE_NAME));
				beanObj.setAndroid_inapp_d(obj
						.getString(TagConstants.GUIDE_SUBID));
				JSONObject jobj = json.getJSONObject("versioninfo");
				beanObj.setMinimumversion(Integer.parseInt(jobj
						.getString(TagConstants.MINIMUM_VERSION)));
				beanObj.setMaximumversion(Integer.parseInt(jobj
						.getString(TagConstants.MAXIMUM_VERSION)));
				if (jobj.getString(TagConstants.NYTESTAT_ENABLE)
						.equalsIgnoreCase("true")) {
					beanObj.setNytestat_enabled(true);
				} else {
					beanObj.setNytestat_enabled(false);
				}
				beanObjList.add(beanObj);
			}
			info_Handler.writeAll(beanObjList);

			if (info_Handler.isAtleastOneguideBought()
					&& info_Handler.isNytestat_shown()) {
				beanObj = new BasicInfoBean();
				beanObj.setGuideId(context.getString(R.string.Nytestat_id));
				beanObj.setGuideName(context.getString(R.string.Nytestat_name));
				beanObj.setGuidePrice(context
						.getString(R.string.Nytestat_price));
				beanObj.setGuideYear(context.getString(R.string.Nytestat_year));
				beanObj.setGuideisBought(true);
				beanObj.setGuideActiveDates(context
						.getString(R.string.Nytestat_active));
				beanObj.setDisplayguidename(context
						.getString(R.string.Nytestat_disguidename));
				beanObj.setAndroid_inapp_d(context
						.getString(R.string.inapp_subid));
				beanObjList.add(beanObj);
				
			}
			info_Handler.writeAll(beanObjList);
			Log.e(TAG, "The Size here is ----------"+beanObjList.size());
		} catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}
	}

	// convert InputStream to String
	private static String getStringFromInputStream(InputStream is) {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
		String line;
		try {
			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return sb.toString();

	}

	public void handleInputStream(InputStream in) {
		// get a factory
		SAXParserFactory spf = SAXParserFactory.newInstance();
		try {
			// get a new instance of parser
			SAXParser sp = spf.newSAXParser();
			sp.parse(in, handler);
		} catch (SAXException se) {
			MyDebug.log_info(TAG, "Parse Exception", se.getMessage());
		} catch (ParserConfigurationException pce) {
			MyDebug.log_info(TAG, "Parse Exception", pce.getMessage());
		} catch (IOException ie) {
			MyDebug.log_info(TAG, "Parse Exception", ie.getMessage());
		} catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}

	}

	DefaultHandler handler = new DefaultHandler() {
		@Override
		public void characters(char[] ch, int start, int length)
				throws SAXException {
			// TODO Auto-generated method stub
			super.characters(ch, start, length);
			temp = new String(ch, start, start + length);
			builder.append(temp);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.xml.sax.helpers.DefaultHandler#endElement(java.lang.String,
		 * java.lang.String, java.lang.String)
		 */
		@Override
		public void endElement(String uri, String localName, String qName)
				throws SAXException {
			// TODO Auto-generated method stub
			super.endElement(uri, localName, qName);
			temp = builder.toString();
			if (TagConstants.GUIDE_NAME.equalsIgnoreCase(localName)) {
				beanObj.setGuideName(temp);
				return;
			}
			if (TagConstants.GUIDE_ID.equalsIgnoreCase(localName)) {
				beanObj.setGuideId(temp);
				return;
			}
			if (TagConstants.GUIDE_PURCHASE_INFO.equalsIgnoreCase(localName)) {
				if (temp.equalsIgnoreCase("true") || temp.equalsIgnoreCase("1"))
					beanObj.setGuideisBought(true);
				else
					beanObj.setGuideisBought(false);
				return;
			}
			if (TagConstants.GUIDE.equalsIgnoreCase(localName)) {
				beanObjList.add(beanObj);
				return;
			}
			if (TagConstants.GUIDES.equalsIgnoreCase(localName)) {
				try {
					info_Handler.writeAll(beanObjList);
				} catch (Exception e) {
					
					e.printStackTrace();
				}
				return;
			}

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.xml.sax.helpers.DefaultHandler#startElement(java.lang.String,
		 * java.lang.String, java.lang.String, org.xml.sax.Attributes)
		 */
		@Override
		public void startElement(String uri, String localName, String qName,
				Attributes attributes) throws SAXException {
			// TODO Auto-generated method stub
			super.startElement(uri, localName, qName, attributes);
			builder = new StringBuilder();
			if (TagConstants.GUIDE.equalsIgnoreCase(localName)) {
				beanObj = new BasicInfoBean();
				return;
			}

		}

	};

}
