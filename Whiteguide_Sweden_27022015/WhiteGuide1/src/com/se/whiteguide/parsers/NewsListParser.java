package com.se.whiteguide.parsers;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.se.whiteguide.helpers.NewsListHelper;
import com.se.whiteguide.log.MyDebug;

@SuppressWarnings("serial")
public class NewsListParser implements Serializable {
	Context mtx;
	String parse_url;
	NewsListHelper helper_obj;
	NewsListHelper.NewsHelper news_arrobj;
	public List<NewsListHelper.NewsHelper> newslist_data;
	public List<NewsListHelper> full_data;
	int rows=0;
	private static String TAG="NewsListParser";

	public NewsListParser(Context mtx) {
		this.mtx = mtx;
		full_data = new ArrayList<NewsListHelper>();
		newslist_data = new ArrayList<NewsListHelper.NewsHelper>();
		rows=0;
	}

	// Method that will parse the JSON file and will return a JSONObject
	public JSONObject parseJSONData() {
		String JSONString = null;
		JSONObject JSONObject = null;
		try {

			// open the inputStream to the file
			InputStream inputStream = mtx.getAssets().open(parse_url);

			int sizeOfJSONFile = inputStream.available();

			// array that will store all the data
			byte[] bytes = new byte[sizeOfJSONFile];

			// reading data into the array from the file
			inputStream.read(bytes);

			// close the input stream
			inputStream.close();

			JSONString = new String(bytes, "UTF-8");
			JSONObject = new JSONObject(JSONString);

		} catch (IOException ex) {
			MyDebug.log_info(TAG, "Parse Exception", ex.getMessage());
		} catch (JSONException x) {
			MyDebug.log_info(TAG, "Parse Exception", x.getMessage());
		}
		return JSONObject;
	}

	InputStream in;
	String json;
	JSONObject jobj, jmenus;

	public JSONObject getjsonfromurl(String menu_url){

		HttpURLConnection http = null;
		URL url = null;
		try {
			url = new URL(menu_url);
		} catch (MalformedURLException e1) {
			MyDebug.log_info(TAG, "Parse Exception", e1.getMessage());
		}
		if (url.getProtocol().toLowerCase().equals("https")) {

			HttpsURLConnection https = null;
			try {
				https = (HttpsURLConnection) url.openConnection();
			} catch (IOException e) {
				MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
			}

			http = https;
		} else {
			try {
				http = (HttpURLConnection) url.openConnection();
			} catch (IOException e) {
				MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
			}
		}
		try {
			in = new BufferedInputStream(http.getInputStream());
		} catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}
		// in=ctx.getResources().openRawResource(R.raw.menuview);
		/*
		 * try { in=mtx.getAssets().open(menu_url); } catch (IOException e1) {
		 * // TODO Auto-generated catch block e1.printStackTrace(); }
		 */
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					in, "ISO-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			in.close();
			json = sb.toString();
			//Log.e("HELLO", json);
		} catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}

		// try parse the string to a JSON object
		/*
		 * try { if (null != json) jobj = new JSONArray(json); } catch
		 * (JSONException e) { Log.e("JSON Parser", "Error parsing data " +
		 * e.toString()); }
		 */
		try {
			if (null != json)
				jobj = new JSONObject(json);
		} catch (JSONException e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}

		return jobj;

	}

	public void getNewsList(String url){
		
		try {
			jmenus = getjsonfromurl(url);
			helper_obj = new NewsListHelper();
			String totolrows = jmenus.getString("totalrows");
			helper_obj.setTotalrowss(totolrows);
			JSONArray newsarray = jmenus.getJSONArray("0");
			int ads_length = newsarray.length() / 3;
			for (int i = 0; i < newsarray.length() + ads_length; i++) {
				rows++;
				int k = i + 1;
				if (rows % 4 == 0) {
					news_arrobj = helper_obj.new NewsHelper();
					news_arrobj.setId("99");
					news_arrobj.setTitle("Advertisement");
					news_arrobj
							.setSmalldesc("This is for advertisement purpose");
					news_arrobj.setItemurl("No need url");
					news_arrobj.setImagelink("No need image link");
					news_arrobj.setDate("No specific date");
				} else {

					int y = k / 4;
					int pos = i - y;
					JSONObject news_obj = newsarray.getJSONObject(pos);
					news_arrobj = helper_obj.new NewsHelper();
					String news_id = news_obj.getString("id");
					news_arrobj.setId(news_id);
					String news_title = news_obj.getString("title");
					news_arrobj.setTitle(news_title);
					String news_smalldesc = news_obj.getString("description");
					news_arrobj.setSmalldesc(news_smalldesc);
					String news_itemurl = news_obj.getString("itemurl");
					news_arrobj.setItemurl(news_itemurl);
					String news_imageurl = news_obj.getString("imageurl");
					news_arrobj.setImagelink(news_imageurl);
					String news_date = news_obj.getString("pubdate");
					news_arrobj.setDate(news_date);

				}

				newslist_data.add(news_arrobj);
			}
			helper_obj.setNews_data(newslist_data);
			full_data.add(helper_obj);
		} catch (Exception e) {
			MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
		}
	}

}
