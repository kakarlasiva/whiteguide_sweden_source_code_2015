package com.se.whiteguide;

import java.util.Vector;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.live.se.whiteguide.R;
import com.se.whiteguide.dataengine.DataEngine;
import com.se.whiteguide.dataengine.DataHandler;
import com.se.whiteguide.dataengine.FavouritesHandler;
import com.se.whiteguide.dataengine.InitialInformation;
import com.se.whiteguide.dataengine.PaymentInformation;
import com.se.whiteguide.dataengine.Restaurant;
import com.se.whiteguide.helpers.BasicInfoBean;
import com.se.whiteguide.log.MyDebug;
import com.se.whiteguide.parsers.RestaurantsParser;
import com.se.whiteguide.screens.DetailedRestaurantview;
import com.se.whiteguide.tabgroup.Home;
import com.se.whiteguide.tabgroup.MapGroup;

/**
 * Represents map view with current location and marker in all restaurant and
 * cafe location
 * 
 * @author Conevo
 * 
 */

public class MainActivity extends Activity implements LocationListener {
	public static final String TAG = "KARTA";
	private GoogleMap googleMap;
	AdView adView;
	RestaurantsParser parser;
	Vector<Double> lat, lon;
	Vector<Integer> Id;
	Vector<Restaurant> restaurants;
	FavouritesHandler treatHandler;
	LocationManager locationManager;
	String provider;
	Location location;
	DataHandler dataHandler;
	InitialInformation information;
	PaymentInformation payInfo;
	ProgressDialog progress;
	Bitmap marker_bitmap, cof_bitmap, Nytestat_bitmap;
	int height, width;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		lat = new Vector<Double>();
		lon = new Vector<Double>();
		Id = new Vector<Integer>();

		progress = new ProgressDialog(this.getParent());
		progress.setCancelable(true);
		progress.setMessage(getString(R.string.loading));
		information = new InitialInformation(getApplicationContext());
		payInfo = new PaymentInformation(getApplicationContext());

		restaurants = new Vector<Restaurant>();
		setContentView(R.layout.karta);
		adView = (AdView) findViewById(R.id.adView);
		String name = "Restaurang";
		try {
			name = new InitialInformation(this).readData().get(0)
					.getGuideName();
		} catch (Exception e) {
			e.printStackTrace();
		}
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

		// Height & width of the Screen.
		height = displaymetrics.heightPixels;
		width = displaymetrics.widthPixels;
		dataHandler = new DataHandler(this.getParent(), name);
		// Get the location manager
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		if (locationManager.getProviders(true).size() > 0) {
			for (int i = 0; i < locationManager.getAllProviders().size(); i++) {
				provider = locationManager.getAllProviders().get(i);
				location = locationManager.getLastKnownLocation(provider);
				if (null == location) {
					locationManager
							.requestLocationUpdates(provider, 0, 0, this);
				} else {
					break;
				}
			}
		} else {
			showAlert();
			return;
		}

		// Initialize the location fields
		if (location != null) {
			onLocationChanged(location);
		} else {
			showAlert();
		}
		new BackGround().execute();
	}

	public void CallToDisplayMenuItem(View v) {
		Home.setDrawer();
	}

	/**
	 * Intialize map and addidng latitude and longitude of resatuarant/cafe from
	 * server
	 */
	class BackGround extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (null != progress) {
				if (!progress.isShowing())
					progress.show();
			}
		}

		@Override
		protected Void doInBackground(Void... params) {
			dotheRequired();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			initilizeMap();
			if (null != progress) {
				if (progress.isShowing())
					progress.dismiss();
			}
		}
	}

	/**
	 * alert to turn on location service
	 */
	private void showAlert() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this.getParent());
		builder.setMessage(getString(R.string.location_request));
		builder.setPositiveButton(getString(R.string.ok),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Intent myIntent = new Intent(
								android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						startActivity(myIntent);
					}
				});
		builder.setNegativeButton(getString(R.string.cancel),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		builder.show();
	}

	/**
	 * get latitude and longitude
	 */
	@SuppressWarnings("unused")
	public void dotheRequired() {
		double lattitude, longitude;
		// location = null;
		if (location == null) {
			lattitude = 59.32;
			longitude = 18.06;
		} else {
			lattitude = location.getLatitude();
			longitude = location.getLongitude();
		}
		restaurants = new Vector<Restaurant>();
		lat = new Vector<Double>();
		lon = new Vector<Double>();
		Id = new Vector<Integer>();
		String name = "Restaurang";
		try {
			name = new InitialInformation(this).readData().get(0)
					.getGuideName();
		} catch (Exception e) {
			e.printStackTrace();
		}
		int count = 0;
		try {
			count = information.readData().size();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (count > 0) {
			try {
				for (BasicInfoBean beanTemp : information.readData()) {
					dataHandler = new DataHandler(this.getParent(),
							beanTemp.getGuideName());

					for (Restaurant restaurant : dataHandler.readRestaurant()) {

						restaurants.add(restaurant);
						lat.add(restaurant.getLat());
						lon.add(restaurant.getLon());
						Id.add(Integer.parseInt(restaurant.getGuideId()));
						// }

					}
				}
			} catch (Exception e) {
				MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
			}

		} else {
			dataHandler = new DataHandler(this.getParent(), name);
			try {
				for (Restaurant restaurant : dataHandler.readRestaurant()) {

					restaurants.add(restaurant);
					lat.add(restaurant.getLat());
					lon.add(restaurant.getLon());
					Id.add(Integer.parseInt(restaurant.getGuideId()));

				}
			} catch (Exception e) {
				MyDebug.log_info(TAG, "Parse Exception", e.getMessage());
			}
		}
	}

	/**
	 * function to load map If map is not created it will create it for you
	 * */
	@SuppressLint("NewApi")
	private void initilizeMap() {
		if (googleMap == null) {
			AdRequest adRequest = new AdRequest.Builder().build();
			// Start loading the ad in the background.
			if (DataEngine.showAds)
				adView.loadAd(adRequest);
			else
				adView.setVisibility(View.GONE);
			googleMap = ((MapFragment) getFragmentManager().findFragmentById(
					R.id.map)).getMap();
		}
		// check if map is created successfully or not
		if (googleMap == null) {

		} else {
			googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
			// Showing / hiding your current location
			if (null != location) {
				googleMap.setMyLocationEnabled(true);
			}

			// Enable / Disable zooming controls
			googleMap.getUiSettings().setZoomControlsEnabled(false);

			// Enable / Disable my location button
			googleMap.getUiSettings().setMyLocationButtonEnabled(true);

			// Enable / Disable Compass icon
			googleMap.getUiSettings().setCompassEnabled(true);

			// Enable / Disable Rotate gesture
			googleMap.getUiSettings().setRotateGesturesEnabled(true);

			// Enable / Disable zooming functionality
			googleMap.getUiSettings().setZoomGesturesEnabled(true);
			// lets place some 10 random markers
			MarkerOptions marker;
			Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
					R.drawable.marker);
			Bitmap coffee_bitmap = BitmapFactory.decodeResource(getResources(),
					R.drawable.marker2);
			Bitmap bitmap_nytest = BitmapFactory.decodeResource(getResources(),
					R.drawable.marker);
			marker_bitmap = Bitmap.createScaledBitmap(bitmap, 57, 70, true);
			cof_bitmap = Bitmap.createScaledBitmap(coffee_bitmap, 57, 70, true);
			// Nytestat_bitmap=Bitmap.createScaledBitmap(bitmap_nytest, 57, 70,
			// true);

			for (int i = 0; i < lat.size(); i++) {
				if (Id.get(i).equals(58)) {
					try {
						marker = new MarkerOptions()
								.position(new LatLng(lat.get(i), lon.get(i)))
								.icon(BitmapDescriptorFactory
										.fromBitmap(marker_bitmap))
								.title(restaurants.get(i).getTitle()
										.replace("#", ""));
						googleMap.addMarker(marker);

					} catch (OutOfMemoryError e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}

				}

				else if (Id.get(i).equals(60)) {
					try {
						marker = new MarkerOptions()
								.position(new LatLng(lat.get(i), lon.get(i)))
								.icon(BitmapDescriptorFactory
										.fromBitmap(cof_bitmap))
								.title(restaurants.get(i).getTitle()
										.replace("#", ""));
						googleMap.addMarker(marker);

					} catch (OutOfMemoryError e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}

				}

				else if (Id.get(i).equals(59)) {
					try {
						marker = new MarkerOptions()
								.position(new LatLng(lat.get(i), lon.get(i)))
								.icon(BitmapDescriptorFactory
										.fromResource(R.drawable.marker_bar))
								.title(restaurants.get(i).getTitle()
										.replace("#", ""));
						googleMap.addMarker(marker);
					} catch (OutOfMemoryError e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			}
			googleMap
					.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

						@Override
						public void onInfoWindowClick(Marker arg0) {
							boolean is_purchased = false;
							String rest_title = "";
							int position = lat.indexOf(arg0.getPosition().latitude);
							Restaurant tempObj = null;
							if (position >= 0) {
								try {
									tempObj = restaurants.get((lat.indexOf(arg0
											.getPosition().latitude)));
								} catch (Exception e) {

								}
							}
							if (null == tempObj) {
								try {
									for (Restaurant restObj : restaurants) {
										rest_title = restObj.getTitle()
												.replace("#", "");
										if (arg0.getTitle().trim()
												.equalsIgnoreCase(rest_title)) {
											tempObj = restObj;
											break;
										}
									}
								} catch (Exception e) {

								}
							}
							if (null != tempObj) {
								BasicInfoBean dummy = null;
								try {
									for (BasicInfoBean beanobj : information
											.readData()) {
										if (tempObj.getGuideId().equalsIgnoreCase(
												beanobj.getGuideId())
												&& tempObj
														.getGuideYear()
														.equalsIgnoreCase(
																beanobj.getGuideYear())) {
											if (beanobj.isGuideisBought()) {
												is_purchased = true;
											}
											dummy = beanobj;
											break;
										}
									}
								
								if (!is_purchased) {
									is_purchased = payInfo.isThisGuidebought(
											tempObj.getId(),
											tempObj.getGuideYear());
								}
								handleThisCall(dummy, tempObj);
								} catch (Exception e) {
									MyDebug.log_info(TAG, "Parse Exception",e.getMessage());
								}

							}
						}
					});
			if (null != location) {
				LatLng latLng = new LatLng(location.getLatitude(),
						location.getLongitude());
				CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
						latLng, 15);
				googleMap.animateCamera(cameraUpdate);
			}

		}

	}

	/**
	 * used to call next class when we click info window
	 */
	private void handleThisCall(final BasicInfoBean dummy,
			final Restaurant tempObj) {
		Home.closeDrawer();

		new Handler().post(new Runnable() {

			@Override
			public void run() {
				Intent nextActivity = new Intent(MainActivity.this,
						DetailedRestaurantview.class);
				nextActivity.putExtra("RES", tempObj);
				nextActivity.putExtra("GUIDE", dummy);
				MapGroup parentActivity = (MapGroup) getParent();
				parentActivity.startActivity(nextActivity);
			}
		});

	}

	/**
	 * forces user to buy the guide
	 */
	protected void showAlertBox() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this.getParent());
		builder.setMessage(getString(R.string.unlock_information));
		builder.setPositiveButton(getString(R.string.ok),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Home.setTab(3);
					}
				});
		builder.setNegativeButton(getString(R.string.cancel),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		builder.show();

	}

	@Override
	public void onLocationChanged(Location location) {
		this.location = location;
		if (null != location) {
			BackGround backGround = new BackGround();
			backGround.execute();
			try {
				locationManager.removeUpdates(this);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}
}
