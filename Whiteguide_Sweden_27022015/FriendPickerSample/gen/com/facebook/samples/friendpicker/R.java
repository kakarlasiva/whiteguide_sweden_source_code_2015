/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.facebook.samples.friendpicker;

public final class R {
    public static final class attr {
    }
    public static final class color {
        public static int number_bar=0x7f040001;
        public static int word_bar=0x7f040000;
    }
    public static final class dimen {
        public static int list_item_height=0x7f050004;
        public static int tab_height=0x7f050000;
        public static int tab_padding=0x7f050001;
        public static int text_default=0x7f050003;
        public static int text_small=0x7f050002;
    }
    public static final class drawable {
        public static int arrowbutton=0x7f020000;
        public static int close_buttn=0x7f020001;
        public static int com_facebook_close=0x7f020002;
        public static int divider=0x7f020003;
        public static int fb_tab_selected=0x7f020004;
        public static int fb_tab_unselected=0x7f020005;
        public static int homeicon=0x7f020006;
        public static int homeicon_sel=0x7f020007;
        public static int homeicon_touch=0x7f020008;
        public static int icon=0x7f020009;
        public static int image_bg=0x7f02000a;
        public static int info_text=0x7f02000b;
        public static int loding_album=0x7f02000c;
        public static int logout=0x7f02000d;
        public static int newphoto=0x7f02000e;
        public static int tab_focused=0x7f02000f;
        public static int tab_pressed=0x7f020010;
        public static int tab_selector=0x7f020011;
        public static int tab_text_selector=0x7f020012;
        public static int title_tile_bar=0x7f020013;
    }
    public static final class id {
        public static int albums_list=0x7f070008;
        public static int bar=0x7f070012;
        public static int btn_info=0x7f070003;
        public static int fb_Friends=0x7f070018;
        public static int fb_logout=0x7f070006;
        public static int fbconnect_album_count=0x7f07000c;
        public static int fbconnect_album_image=0x7f07000a;
        public static int fbconnect_album_user_name=0x7f07000b;
        public static int friend_picker_fragment=0x7f070019;
        public static int header=0x7f070002;
        public static int headerhomeicon=0x7f070001;
        public static int imageID=0x7f07000f;
        public static int imv_albums_profile_picture=0x7f070004;
        public static int layout=0x7f070016;
        public static int multiplecouponarrow=0x7f07000d;
        public static int myListView=0x7f070014;
        public static int no_albums=0x7f070007;
        public static int phone_contacts=0x7f070017;
        public static int section=0x7f07000e;
        public static int sideBar=0x7f070015;
        public static int sideBar1=0x7f070009;
        public static int tab_1=0x7f07001a;
        public static int tab_2=0x7f07001b;
        public static int text=0x7f070013;
        public static int textView=0x7f070010;
        public static int textView2=0x7f070011;
        public static int tv_albums_fb_userName=0x7f070005;
        public static int widget38=0x7f070000;
    }
    public static final class layout {
        public static int facebook_connect_activity=0x7f030000;
        public static int facebook_friend_activity=0x7f030001;
        public static int fb_friend_row=0x7f030002;
        public static int fbconnect_album_row=0x7f030003;
        public static int layout_row=0x7f030004;
        public static int list_item=0x7f030005;
        public static int main=0x7f030006;
        public static int main_layout=0x7f030007;
        public static int pick_friends_activity=0x7f030008;
        public static int tab=0x7f030009;
        public static int tabs_fragment=0x7f03000a;
    }
    public static final class string {
        /**  <string name="alert_InternetConnection">We’re very sorry but this feature requires an internet connection.</string> 
         */
        public static int alert_InternetConnection=0x7f060005;
        public static int alert_InternetConnection_title=0x7f060004;
        public static int app_id=0x7f060001;
        public static int app_name=0x7f060000;
        public static int exception=0x7f060002;
        public static int loading=0x7f06000a;
        public static int ok_button=0x7f060003;
        public static int progress_default_title=0x7f060006;
        public static int progress_rx_msg=0x7f060007;
        public static int tab_numbers=0x7f060009;
        public static int tab_words=0x7f060008;
    }
}
