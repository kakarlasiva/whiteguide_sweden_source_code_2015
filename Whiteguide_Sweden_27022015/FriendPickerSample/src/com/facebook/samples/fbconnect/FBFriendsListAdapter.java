package com.facebook.samples.fbconnect;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.samples.friendpicker.R;
import com.facebook.samples.friendpicker.UrlImageLoader;



public class FBFriendsListAdapter extends ArrayAdapter<FbFriend> {
	private List<FbFriend> listFacebookFriendInfoBeanBean;
	public Activity activity;
	private static LayoutInflater inflater = null;
	private UrlImageLoader fBConnectImageLoader;

	public FBFriendsListAdapter(Activity activity, int resourceId,
			List<FbFriend> listFacebookAlbumInfoBeanBean,
			UrlImageLoader fBConnectImageLoader) {
		super(activity, resourceId, listFacebookAlbumInfoBeanBean);
		this.listFacebookFriendInfoBeanBean = listFacebookAlbumInfoBeanBean;
		this.activity = (Activity) activity;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.fBConnectImageLoader = fBConnectImageLoader;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// to findViewById() on each row.
		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.fb_friend_row,parent,false);
			// Creates a ViewHolder and store references to the two children
			// views
			// we want to bind data to.
			viewHolder = new ViewHolder();
			viewHolder.mAlbumName = (TextView) convertView
					.findViewById(R.id.fbconnect_album_user_name);
			
			viewHolder.coverImage = (ImageView) convertView
					.findViewById(R.id.fbconnect_album_image);
			convertView.setTag(viewHolder);
		} else {
			// Get the ViewHolder back to get fast access to the TextView
			// and the ImageView.
			viewHolder = (ViewHolder) convertView.getTag();
		}

		
		/*viewHolder.mAlbumCount.setText(" ("
				+ listFacebookAlbumInfoBeanBean.get(position)
						.getAlbumPicsCount() + ")");*/
		viewHolder.coverImage.setTag(listFacebookFriendInfoBeanBean
				.get(position).getUrl());
		fBConnectImageLoader.DisplayImage(
				listFacebookFriendInfoBeanBean.get(position)
						.getUrl(), activity,
				viewHolder.coverImage);
		viewHolder.mAlbumName.setText(listFacebookFriendInfoBeanBean.get(
				position).getName());
		return convertView;
	}

	static class ViewHolder {
		protected TextView mAlbumName;
		//protected TextView mAlbumCount;
		protected ImageView coverImage;
	}
}
