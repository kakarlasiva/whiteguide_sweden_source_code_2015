package com.facebook.samples.contacts;

import java.util.ArrayList;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import com.facebook.samples.friendpicker.R;


import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class PhoneContactsActivity extends Activity {
	private List<PhoneContact> stringList;
	private HashMap<String, PhoneContact> nameMap;
	  /** Called when the activity is first created. */  
    @Override  
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.main);
        Cursor c = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        String contactName, contactTelNumber = "";
        String contactID;
        stringList = new ArrayList<PhoneContact>();
        nameMap = new HashMap<String, PhoneContact>();
        // You only need to find these indices once
        int idIndex = c.getColumnIndex(ContactsContract.Contacts._ID);
        int hasNumberIndex = c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER);
        int nameIndex = c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
        int photoIndex = c.getColumnIndex(ContactsContract.Contacts.PHOTO_THUMBNAIL_URI);

        // This is simpler than calling getCount() every iteration
        while(c.moveToNext()) {
            contactName = c.getString(nameIndex);
            contactID = c.getString(idIndex);
            // If this is an integer ask for an integer
            if (c.getInt(hasNumberIndex) > 0) {
                Cursor pCur = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[] { contactID },null);
                while (pCur.moveToNext()) {
                    contactTelNumber = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    PhoneContact contact = new PhoneContact();
                    contact.setContactName(contactName);
                    contact.setContactNumber(contactTelNumber);
                    contact.setContactUri(c.getString(photoIndex));
                    if(nameMap.containsKey(contactName)){
                    	PhoneContact dummyContact = nameMap.get(contactName) ;
                    	if(!dummyContact.getExtraNums().contains(contactTelNumber)){
                    		dummyContact.getExtraNums().add(contactTelNumber);
                    	}
                    }else{
                    	contact.getExtraNums().add(contactTelNumber);
                    	nameMap.put(contactName, contact);
                    }
                   // Store the "name: number" string in our list
                   // stringList.add(contact);
                } 
            }
        }
        stringList.addAll(nameMap.values());
        Collections.sort(stringList,new Comparator<PhoneContact>() {

			@Override
			public int compare(PhoneContact p1, PhoneContact p2) {
				return p1.getContactName().compareToIgnoreCase(p2.getContactName());
			}
		});

        // Find the ListView, create the adapter, and bind them
        ListView listView = (ListView) findViewById(R.id.myListView);
        PhoneBookContactsAdapter adapter = new PhoneBookContactsAdapter(PhoneContactsActivity.this, stringList);
        listView.setAdapter(adapter);
        SideBar indexBar = (SideBar) findViewById(R.id.sideBar);  
        indexBar.setListView(listView);  
    } 
    
    
}
