package com.facebook.samples.facebook;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * Used for saving and retrieving Facebook Sessions
 * 
 * @author Kavya.Soni
 */
public class SessionStore {

	private static final String TOKEN = "access_token";
	private static final String EXPIRES = "expires_in";
	private static final String KEY = "facebook-session";
	private static final String USERNAME = "facebook_user_name";

	public static boolean save(Facebook session, Context context) {
		Editor intreatEditor = context.getSharedPreferences("dob",
				Context.MODE_PRIVATE).edit();
		Editor editor = context.getSharedPreferences(KEY, Context.MODE_PRIVATE)
				.edit();
		editor.putString(TOKEN, session.getAccessToken());
		intreatEditor.putString("fbSessionToken", session.getAccessToken());
		editor.putLong(EXPIRES, session.getAccessExpires());
		intreatEditor.commit();
		return editor.commit();
	}

	public static boolean saveUserName(String userName, Context context) {
		Editor editor = context.getSharedPreferences(KEY, Context.MODE_PRIVATE)
				.edit();
		editor.putString(USERNAME, userName);
		return editor.commit();
	}

	public static String getUserName(Context context) {
		SharedPreferences savedSession = context.getSharedPreferences(KEY,
				Context.MODE_PRIVATE);
		return savedSession.getString(USERNAME, null);
	}

	public static boolean restore(Facebook session, Context context) {
		SharedPreferences savedSession = context.getSharedPreferences(KEY,
				Context.MODE_PRIVATE);
		session.setAccessToken(savedSession.getString(TOKEN, null));
		session.setAccessExpires(savedSession.getLong(EXPIRES, 0));
		return session.isSessionValid();
	}

	public static String getAccessTocken(Context context) {
		SharedPreferences savedSession = context.getSharedPreferences(KEY,
				Context.MODE_PRIVATE);
		return savedSession.getString(TOKEN, null);
	}

	public static void clear(Context context) {
		Editor editor = context.getSharedPreferences(KEY, Context.MODE_PRIVATE)
				.edit();
		Editor intreatEditor = context.getSharedPreferences("dob",
				Context.MODE_PRIVATE).edit();
		intreatEditor.putString("fbSessionToken", "0");
		intreatEditor.commit();
		editor.clear();
		editor.commit();
	}

}
