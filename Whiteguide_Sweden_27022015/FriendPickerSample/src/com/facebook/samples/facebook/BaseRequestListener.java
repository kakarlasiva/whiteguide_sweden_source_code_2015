package com.facebook.samples.facebook;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import com.facebook.samples.facebook.AsyncFacebookRunner.RequestListener;


import android.util.Log;


/**
 * Skeleton base class for RequestListeners, providing default error handling.
 * Applications should handle these error conditions.
 * 
 */
public abstract class BaseRequestListener implements RequestListener {

	public void onFacebookError(FacebookError e, final Object state) {
		Log.e("FacebookError" , e.getMessage());
	}

	public void onFileNotFoundException(FileNotFoundException e,
			final Object state) {
		Log.e("FileNotFoundException" , e.getMessage());
	}

	public void onIOException(IOException e, final Object state) {
		Log.e("IOException" , e.getMessage());
	}

	public void onMalformedURLException(MalformedURLException e,
			final Object state) {
		Log.e("MalformedURLException" , e.getMessage());
	}

}
